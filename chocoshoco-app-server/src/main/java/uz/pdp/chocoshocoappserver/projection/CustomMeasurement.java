package uz.pdp.chocoshocoappserver.projection;


import org.springframework.data.rest.core.config.Projection;
import uz.pdp.chocoshocoappserver.entity.Measurement;
import uz.pdp.chocoshocoappserver.entity.Region;

import java.util.UUID;

@Projection(name = "customMeasurement", types = Measurement.class)
public interface CustomMeasurement {
    UUID getId();

    String getName();
}
