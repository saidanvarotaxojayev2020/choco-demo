package uz.pdp.chocoshocoappserver.projection;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.chocoshocoappserver.entity.Address;


@Projection(name = "customAddress", types = Address.class)
public interface CustomAddress {

    Integer getId();

    String getStreet();

    String getHouse();

    Double getLat();

    Double getLon();

    @Value("#{@districtRepository.districtByAddressId(target.id)}")
    CustomDistrict getCustomDistrict();

}
