package uz.pdp.chocoshocoappserver.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.chocoshocoappserver.entity.Color;

import java.util.UUID;

@Projection(name = "customColor",types = Color.class)
public interface CustomColor {

    UUID getId();

    String getName();

    String getCode();
}
