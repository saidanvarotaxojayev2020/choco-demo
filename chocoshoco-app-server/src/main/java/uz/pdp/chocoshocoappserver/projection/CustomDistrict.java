package uz.pdp.chocoshocoappserver.projection;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.chocoshocoappserver.entity.District;
import uz.pdp.chocoshocoappserver.entity.Region;


@Projection(name = "districtCustom",types = District.class)
public interface CustomDistrict {
    Integer getId();



    String getName();


    @Value("#{@regionRepository.byId(target.id)}")
    Region getRegion();



}
