package uz.pdp.chocoshocoappserver.projection;


import org.springframework.data.rest.core.config.Projection;
import uz.pdp.chocoshocoappserver.entity.Region;

@Projection(name = "customRegion", types = Region.class)
public interface CustomRegion {

    Integer getId();

    String getName();



}
