package uz.pdp.chocoshocoappserver.projection;


import org.springframework.data.rest.core.config.Projection;
import uz.pdp.chocoshocoappserver.entity.BoxCategory;

import java.util.UUID;


@Projection(name = "customBoxCategory", types = BoxCategory.class)
public interface CustomerBoxCategory {

    UUID getId();

    String getName();
}
