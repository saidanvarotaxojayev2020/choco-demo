package uz.pdp.chocoshocoappserver.projection;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.chocoshocoappserver.entity.DeliveryPrice;
import uz.pdp.chocoshocoappserver.entity.District;

import java.util.UUID;

@Projection(name = "customDeliveryPrice",types = DeliveryPrice.class)
public interface CustomDeliveryPrice {
    UUID getId();

    @Value("#{target.district.id}")
    Integer getDistrictId();

//    @Value("#{@districtRepository.getDistrictNameById(target.district.id)}")
    @Value("#{target.district.name}")
    String getDistrictName();

    double getPrice();

//    @Value("#{@districtRepository.districtByAddressId(target.id)}")
//    CustomDistrict getCustomDistrict();









}
