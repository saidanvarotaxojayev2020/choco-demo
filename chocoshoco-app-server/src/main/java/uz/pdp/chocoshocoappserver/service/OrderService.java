package uz.pdp.chocoshocoappserver.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.chocoshocoappserver.entity.*;
import uz.pdp.chocoshocoappserver.entity.enums.OrderStatus;
import uz.pdp.chocoshocoappserver.payload.*;
import uz.pdp.chocoshocoappserver.repository.*;
import uz.pdp.chocoshocoappserver.repository.res.AddressRepository;
import uz.pdp.chocoshocoappserver.repository.res.ColorRepository;
import uz.pdp.chocoshocoappserver.repository.res.DeliveryPriceRepository;
import uz.pdp.chocoshocoappserver.repository.res.DistrictRepository;
import uz.pdp.chocoshocoappserver.utills.CommonUtils;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BoxRepository boxRepository;

    @Autowired
    DeliveryPriceRepository deliveryPriceRepository;

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    ProductWithAmountRepository productWithAmountRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    ColorRepository colorRepository;

    public ApiResponse saveOrEditOrder(OrderDto orderDto) {
        try {
            Order order = new Order();
            if (orderDto.getId() != null) {
                order = orderRepository.findById(orderDto.getId()).orElseThrow(() -> new IllegalStateException("Order not found"));
                for (ProductWithAmount productWithAmount : order.getProductWithAmounts()) {
                    productWithAmountRepository.deleteProductWithAmountById(productWithAmount.getId());
                }
            }
            double totalPrice = 0.0;
            if (orderDto.getAddressDto() != null) {
                Address address = new Address();
                address.setDistrict(districtRepository.findById(orderDto.getAddressDto().getDistrictDto().getId()).orElseThrow(() -> new IllegalStateException("District not found")));
                address.setHouse(orderDto.getAddressDto().getHouse());
                address.setStreet(orderDto.getAddressDto().getStreet());
                if (orderDto.getAddressDto().getLat() != null && orderDto.getAddressDto().getLon() != null) {
                    address.setLat(orderDto.getAddressDto().getLat());
                    address.setLon(orderDto.getAddressDto().getLon());
                }
                Address saveAddress = addressRepository.save(address);
                order.setAddress(saveAddress);
                DeliveryPrice deliveryPriceByDistrictId = deliveryPriceRepository.findByDistrictId(saveAddress.getDistrict().getId());
                order.setDeliveryPrice(deliveryPriceByDistrictId);
                totalPrice += deliveryPriceByDistrictId.getPrice();
            }
            order.setClientNumber(orderDto.getClientNumber());
            order.setUser(userRepository.findById(orderDto.getUserDto().getId()).orElseThrow(() -> new IllegalStateException("User not found")));
            order.setBox(boxRepository.findById(orderDto.getBoxDto().getId()).orElseThrow(() -> new IllegalStateException("Box not found")));
            order.setDate(orderDto.getDate());
            order.setOrderStatus(orderDto.getOrderStatus());
            Order saveOrder = orderRepository.save(order);
            for (ProductWithAmountDto productWithAmountDto : orderDto.getProductWithAmountDtos()) {
                ProductWithAmount newProductWithAmount = new ProductWithAmount();
                Product product = productRepository.findById(productWithAmountDto.getProductDto().getId()).orElseThrow(() -> new IllegalStateException("Product not found"));
                newProductWithAmount.setProduct(product);
                newProductWithAmount.setAmount(productWithAmountDto.getAmount());
                newProductWithAmount.setOrder(saveOrder);
                newProductWithAmount.setContent(productWithAmountDto.getContent());
                totalPrice += product.getPrice() * productWithAmountDto.getAmount();
                productWithAmountRepository.save(newProductWithAmount);
            }

            if (orderDto.getOrderDiscountDtos() != null) {
                List<OrderDiscount> orderDiscountList = new ArrayList<>();
                for (OrderDiscountDto orderDiscountDto : orderDto.getOrderDiscountDtos()) {
                    if (orderDiscountDto.getDiscountType().name().equals("PERCENT")) {
                        totalPrice -= (totalPrice / 100) * orderDiscountDto.getDiscountAmount();
                    } else {
                        totalPrice -= totalPrice - orderDiscountDto.getDiscountAmount();
                    }
                    OrderDiscount orderDiscount = new OrderDiscount();
                    orderDiscount.setOrder(saveOrder);
                    orderDiscount.setDiscountType(orderDiscountDto.getDiscountType());
                    orderDiscount.setDiscountAmount(orderDiscountDto.getDiscountAmount());
                    orderDiscountList.add(orderDiscount);
                }
                saveOrder.setOrderDiscounts(orderDiscountList);
                orderRepository.save(saveOrder);
            }
            saveOrder.setTotalPrice(totalPrice);
            orderRepository.save(saveOrder);
            return new ApiResponse("Success", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse getAllOrderByPageable(Integer page, Integer size, OrderStatus orderStatus, String search, UUID userId) {
        Page<Order> orderPage;
        if (search.equals("all")) {
            orderPage = orderRepository.findAll(CommonUtils.getPageableByCreatedAtDesc(page, size));
        } else if (search.equals("client")) {
            if (orderStatus == null) {
                orderPage = orderRepository.findAllByUserId(userId, CommonUtils.getPageableByCreatedAtDesc(page, size));
            } else if (userId != null){
                orderPage = orderRepository.findAllByUserIdAndOrderStatus(userId, orderStatus, CommonUtils.getPageableByCreatedAtDesc(page, size));
            }else {
                orderPage = orderRepository.findAllByOrderStatus(orderStatus, CommonUtils.getPageableByCreatedAtDesc(page, size));
            }
        } else {
            orderPage = orderRepository.findAllByOrderStatus(orderStatus, CommonUtils.getPageableByCreatedAtDesc(page, size));
        }
        return new ApiResponse("Ok", true, orderPage.getContent().stream().map(this::getOrderDto).collect(Collectors.toList()), orderPage.getTotalElements(), page);
    }

    public OrderDto getOrderDto(Order order) {
        OrderDto orderDto = new OrderDto();
        Set<UUID> attachmentIds = new HashSet<>();
        for (Attachment attachment : order.getBox().getAttachments()) {
            attachmentIds.add(attachment.getId());
        }
        orderDto.setId(order.getId());
        orderDto.setUserDto(new UserDto(
                order.getUser().getId(),
                order.getUser().getFirstName(),
                order.getUser().getPhoneNumber()
        ));
        orderDto.setMessage(order.getMessage());
        orderDto.setBoxDto(new BoxDto(
                order.getBox().getId(),
                order.getBox().getName(),
                new ColorDto(
                        order.getBox().getColor().getId(),
                        order.getBox().getColor().getName(),
                        order.getBox().getColor().getCode()
                ),
                new BoxCategoryDto(
                        order.getBox().getBoxCategory().getId(),
                        order.getBox().getBoxCategory().getName()
                ),
                attachmentIds
        ));
        orderDto.setClientNumber(order.getClientNumber());
        orderDto.setOrderStatus(order.getOrderStatus());
        orderDto.setDate(order.getDate());
        if (order.getAddress() != null) {
            orderDto.setAddressDto(new AddressDto(
                    order.getAddress().getId(),
                    order.getAddress().getStreet(),
                    order.getAddress().getHouse(),
                    order.getAddress().getLat(),
                    order.getAddress().getLon(),
                    new DistrictDto(
                            order.getAddress().getDistrict().getId(),
                            order.getAddress().getDistrict().getName()
                    )
            ));
            orderDto.setDeliveryPriceDto(new DeliveryPriceDto(
                    order.getDeliveryPrice().getId(),
                    order.getDeliveryPrice().getPrice()
            ));
        }
        orderDto.setTotalPrice(order.getTotalPrice());
        List<ProductWithAmount> allByOrderId = productWithAmountRepository.findAllByOrderId(order.getId());
        orderDto.setProductWithAmountDtos(allByOrderId.stream().map(this::getProductWithAmountDto).collect(Collectors.toList()));
        return orderDto;
    }

    public ProductWithAmountDto getProductWithAmountDto(ProductWithAmount productWithAmount) {
        List<UUID> attachmentIds = new ArrayList<>();
        for (Attachment photo : productWithAmount.getProduct().getPhotos()) {
            attachmentIds.add(photo.getId());
        }
        return new ProductWithAmountDto(
                productWithAmount.getId(),
                new ProductDto(
                        productWithAmount.getProduct().getId(),
                        productWithAmount.getProduct().getName(),
                        productWithAmount.getProduct().getPrice(),
                        productWithAmount.getProduct().getMeasureValue(),
                        productWithAmount.getProduct().isHasContent(),
                        new ColorDto(
                                productWithAmount.getProduct().getColor().getId(),
                                productWithAmount.getProduct().getColor().getName(),
                                productWithAmount.getProduct().getColor().getCode()
                        ),
                        new CategoryDto(
                                productWithAmount.getProduct().getCategory().getId(),
                                productWithAmount.getProduct().getCategory().getName()
                        ),
                        new MeasurementDto(
                                productWithAmount.getProduct().getMeasurement().getId(),
                                productWithAmount.getProduct().getMeasurement().getName()
                        ),
                        attachmentIds
                ),
                productWithAmount.getAmount(),
                productWithAmount.getContent()
        );
    }

    public ApiResponse deleteOrder(UUID id) {
        try {
            orderRepository.deleteById(id);
            return new ApiResponse("Success", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse editOrderStatus(UUID orderId, OrderStatus orderStatus) {
        try {
            Order order = orderRepository.findById(orderId).orElseThrow(() -> new IllegalStateException("Order topilmadi"));
            order.setOrderStatus(orderStatus);
            orderRepository.save(order);
            return new ApiResponse("Status tahrirlandi", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }
}
