package uz.pdp.chocoshocoappserver.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.chocoshocoappserver.entity.Attachment;
import uz.pdp.chocoshocoappserver.entity.Box;
import uz.pdp.chocoshocoappserver.entity.Product;
import uz.pdp.chocoshocoappserver.payload.ApiResponse;
import uz.pdp.chocoshocoappserver.payload.BoxCategoryDto;
import uz.pdp.chocoshocoappserver.payload.BoxDto;
import uz.pdp.chocoshocoappserver.payload.ColorDto;
import uz.pdp.chocoshocoappserver.repository.AttachmentRepository;
import uz.pdp.chocoshocoappserver.repository.BoxRepository;
import uz.pdp.chocoshocoappserver.repository.res.BoxCategoryRepository;
import uz.pdp.chocoshocoappserver.repository.res.ColorRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class BoxService {

    @Autowired
    BoxRepository boxRepository;

    @Autowired
    ColorRepository colorRepository;

    @Autowired
    BoxCategoryRepository boxCategoryRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    public ApiResponse savOrEditBox(BoxDto boxDto) {

        try {
            Box box = new Box();
            if (boxDto.getId() != null) {
                box = boxRepository.findById(boxDto.getId()).orElseThrow(() -> new IllegalStateException("Box not found"));
            }
            box.setName(boxDto.getName());
            box.setColor(colorRepository.findById(boxDto.getColorDto().getId()).orElseThrow(() -> new IllegalStateException("Color not found")));
            box.setBoxCategory(boxCategoryRepository.findById(boxDto.getBoxCategoryDto().getId()).orElseThrow(() -> new IllegalStateException("Box category not found")));
            if (boxDto.getAttachmentIds() != null){
                Set<Attachment> attachments = new HashSet<>();
                for (UUID attachmentId : boxDto.getAttachmentIds()) {
                    Attachment oneAttachment = attachmentRepository.findById(attachmentId).orElseThrow(() -> new IllegalStateException("Attachment not found"));
                    attachments.add(oneAttachment);
                }
                box.setAttachments(attachments);
            }

            boxRepository.save(box);
            return new ApiResponse("Success", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }

    }

    public ApiResponse getAllBox() {
        try {
            List<Box> allBox = boxRepository.findAll();
            return new ApiResponse("Success", true, allBox.stream().map(this::getBoxDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public BoxDto getBoxDto(Box box) {
        Set<UUID> attachmentId = new HashSet<>();
        for (Attachment photo : box.getAttachments()) {
            attachmentId.add(photo.getId());
        }
        return new BoxDto(
                box.getId(),
                box.getName(),
                new ColorDto(
                        box.getColor().getId(),
                        box.getColor().getName(),
                        box.getColor().getCode()
                ),
                new BoxCategoryDto(
                        box.getBoxCategory().getId(),
                        box.getBoxCategory().getName()
                ),
                attachmentId
        );
    }

    public ApiResponse deleteBox(UUID id) {
        try {
            boxRepository.deleteById(id);
            return new ApiResponse("Success", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse getBoxByCategoryId(UUID id) {
        try {
            List<Box> allBox = boxRepository.findAllByBoxCategoryId(id);
            return new ApiResponse("Success", true, allBox.stream().map(this::getBoxDto).collect(Collectors.toList()));
        }catch (Exception e){
            return new ApiResponse("Error", false);
        }
    }
}
