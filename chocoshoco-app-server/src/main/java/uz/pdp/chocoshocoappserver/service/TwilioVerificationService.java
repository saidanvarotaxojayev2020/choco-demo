package uz.pdp.chocoshocoappserver.service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.chocoshocoappserver.entity.TwilioVerification;
import uz.pdp.chocoshocoappserver.entity.User;
import uz.pdp.chocoshocoappserver.payload.ApiResponse;
import uz.pdp.chocoshocoappserver.payload.TwilioDto;
import uz.pdp.chocoshocoappserver.payload.UserDto;
import uz.pdp.chocoshocoappserver.repository.TwilioVerificationRepository;
import uz.pdp.chocoshocoappserver.repository.UserRepository;
import uz.pdp.chocoshocoappserver.utills.CommonUtils;

import java.util.Optional;


@Service
public class TwilioVerificationService {
    @Autowired
    TwilioVerificationRepository twilioVerificationRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public ApiResponse

    getSmsCode(TwilioDto twilioDto) {
        int code = CommonUtils.getRandomNumber();
        User user = userRepository.findByPhoneNumber(twilioDto.getPhoneNumber());
        if (user == null) {
            return new ApiResponse("Bunday user mavjud emas", false);
        } else {
            try {
                TwilioVerification twilioVerification = new TwilioVerification();
                Optional<TwilioVerification> optionalTwilioVerification = twilioVerificationRepository.findByPhoneNumber(user.getPhoneNumber());
                if (optionalTwilioVerification.isPresent()) {
                    twilioVerification = optionalTwilioVerification.get();
                } else {
                    twilioVerification.setPhoneNumber(user.getPhoneNumber());
                }
                twilioVerification.setCode(code);
                twilioVerification.setVerified(false);
                twilioVerificationRepository.save(twilioVerification);
                String ACCOUNT_SID = "ACdf9ddedbc82bd3b3984543aaed3a915f";
                String AUTH_TOKEN = "1da1b08aa299791d05761b8b477d7c64";
                Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
                Message message = Message
                        .creator(new PhoneNumber(user.getPhoneNumber()), // to
                                new PhoneNumber("+12315387451"), // from
                                "Tasdiqlash kodi: " + code)
                        .create();
                return new ApiResponse("Sms kodni tasdiqlang", true);
            } catch (Exception e) {
                return new ApiResponse("Twilio bilan aloqa mavjud emas", false);
            }
        }
    }

    public ApiResponse checkPhoneAndCode(TwilioDto twilioDto) {
        try {
            TwilioVerification twilioVerification = twilioVerificationRepository.findByPhoneNumberAndCodeAndVerifiedFalse(twilioDto.getPhoneNumber(), twilioDto.getCode()).orElseThrow(() -> new IllegalStateException("Code error"));
            twilioVerification.setVerified(true);
            twilioVerificationRepository.save(twilioVerification);
            return new ApiResponse("Yangi password kiriting", true);
        } catch (Exception e) {
            return new ApiResponse("Kod xato kiritilgan, qaytadan urinib ko'ring", false);
        }
    }

    public ApiResponse editPassword(UserDto userDto) {
        try {
            User user = userRepository.findByPhoneNumber(userDto.getPhoneNumber());
            if (user != null) {
                if (userDto.getPassword().equals(userDto.getPrePassword())) {
                    user.setPassword(passwordEncoder.encode(userDto.getPassword()));
                    userRepository.save(user);
                    return new ApiResponse("Successfully edited", true);
                } else {
                    return new ApiResponse("Parollar mos emas", false);
                }
            }
            return new ApiResponse("Bunday user mavjud emas", false);
        } catch (Exception e) {
            return new ApiResponse("Bunday user mavjud emas", false);
        }
    }

    public ApiResponse getSmsCodeUserRegister(TwilioDto twilioDto) {
        int code = CommonUtils.getRandomNumber();
        User user = userRepository.findByPhoneNumber(twilioDto.getPhoneNumber());
        if (user != null) {
            return new ApiResponse("Bunday user mavjud", false);
        } else {
            try {
                String ACCOUNT_SID = "ACdf9ddedbc82bd3b3984543aaed3a915f";
                String AUTH_TOKEN = "1da1b08aa299791d05761b8b477d7c64";
                Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
                Message message = Message
                        .creator(new PhoneNumber(twilioDto.getPhoneNumber()), // to
                                new PhoneNumber("+12315387451"), // from
                                "Tasdiqlash kodi: " + code)
                        .create();
                TwilioVerification twilioVerification = new TwilioVerification();
                Optional<TwilioVerification> optionalTwilioVerification = twilioVerificationRepository.findByPhoneNumber(twilioDto.getPhoneNumber());
                if (optionalTwilioVerification.isPresent()) {
                    twilioVerification = optionalTwilioVerification.get();
                } else {
                    twilioVerification.setPhoneNumber(twilioDto.getPhoneNumber());
                }
                twilioVerification.setCode(code);
                twilioVerification.setVerified(false);
                twilioVerificationRepository.save(twilioVerification);
                return new ApiResponse("Sms kodni tasdiqlang", true);
            } catch (Exception e) {
                return new ApiResponse("Twilio bilan aloqa mavjud emas", false);
            }
        }
    }

    public ApiResponse checkPhoneAndCodeUserRegister(TwilioDto twilioDto) {
        try {
            TwilioVerification twilioVerification = twilioVerificationRepository.findByPhoneNumberAndCodeAndVerifiedFalse(twilioDto.getPhoneNumber(), twilioDto.getCode()).orElseThrow(() -> new IllegalStateException("Code error"));
            twilioVerification.setVerified(true);
            twilioVerificationRepository.save(twilioVerification);
            return new ApiResponse("Ismingiz va parolingizni kiriting", true);
        } catch (Exception e) {
            return new ApiResponse("Kod xato kiritilgan, qaytadan urinib ko'ring", false);
        }
    }

}
