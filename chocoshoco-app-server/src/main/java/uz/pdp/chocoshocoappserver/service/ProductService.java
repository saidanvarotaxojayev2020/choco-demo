package uz.pdp.chocoshocoappserver.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.chocoshocoappserver.entity.Attachment;
import uz.pdp.chocoshocoappserver.entity.Product;
import uz.pdp.chocoshocoappserver.payload.*;
import uz.pdp.chocoshocoappserver.repository.AttachmentRepository;
import uz.pdp.chocoshocoappserver.repository.CategoryRepository;
import uz.pdp.chocoshocoappserver.repository.ProductRepository;
import uz.pdp.chocoshocoappserver.repository.res.ColorRepository;
import uz.pdp.chocoshocoappserver.repository.res.MeasurementRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class  ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    MeasurementRepository measurementRepository;

    @Autowired
    ColorRepository colorRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    public ApiResponse saveOrEditProduct(ProductDto productDto) {
        try {
            Product product = new Product();
            if (productDto.getId() != null) {
                product = productRepository.findById(productDto.getId()).orElseThrow(() -> new IllegalStateException("Product not found"));
            }
            product.setCategory(categoryRepository.findById(productDto.getCategoryDto().getId()).orElseThrow(() -> new IllegalStateException("Category not found")));
            product.setColor(colorRepository.findById(productDto.getColorDto().getId()).orElseThrow(() -> new IllegalStateException("Color not found")));
            product.setMeasurement(measurementRepository.findById(productDto.getMeasurementDto().getId()).orElseThrow(() -> new IllegalStateException("Measurement not found")));
            product.setPrice(productDto.getPrice());
            product.setName(productDto.getName());
            product.setHasContent(productDto.isHasContent());

            Set<Attachment> attachmentSet = new HashSet<>();
            for (UUID attachmentId : productDto.getAttachmentIds()) {
                Attachment attachment_not_found = attachmentRepository.findById(attachmentId).orElseThrow(() -> new IllegalStateException("Attachment not found"));
                attachmentSet.add(attachment_not_found);
            }
            product.setPhotos(attachmentSet);
            product.setMeasureValue(productDto.getMeasureValue());
            productRepository.save(product);
            return new ApiResponse("Success", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse getAllProduct() {
        try {
            List<Product> allProduct = productRepository.findAll();
            return new ApiResponse("Success", true, allProduct.stream().map(this::getProductDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ProductDto getProductDto(Product product) {
        List<UUID> attachmentId = new ArrayList<>();
        for (Attachment photo : product.getPhotos()) {
            attachmentId.add(photo.getId());
        }
        return new ProductDto(
                product.getId(),
                product.getName(),
                product.getPrice(),
                product.getMeasureValue(),
                product.isHasContent(),
                new ColorDto(
                        product.getColor().getId(),
                        product.getColor().getName(),
                        product.getColor().getCode()
                ),
                new CategoryDto(
                        product.getCategory().getId(),
                        product.getCategory().getName()
                ),
                new MeasurementDto(
                        product.getMeasurement().getId(),
                        product.getMeasurement().getName()
                ),
                attachmentId
        );
    }

    public ApiResponse deleteProduct(UUID id) {
        try {
            productRepository.deleteById(id);
            return new ApiResponse("Success", true);
        }catch (Exception e){
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse getProductByCategory(UUID id) {
        try {
            List<Product> allProduct = productRepository.findAllByCategoryId(id);
            return new ApiResponse("Success", true, allProduct.stream().map(this::getProductDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }
}
