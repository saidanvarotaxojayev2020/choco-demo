package uz.pdp.chocoshocoappserver.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.chocoshocoappserver.entity.Attachment;
import uz.pdp.chocoshocoappserver.entity.ProductSet;
import uz.pdp.chocoshocoappserver.entity.ProductWithAmount;
import uz.pdp.chocoshocoappserver.payload.*;
import uz.pdp.chocoshocoappserver.repository.*;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductSetService {

    @Autowired
    ProductSetRepository productSetRepository;

    @Autowired
    OrderService orderService;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    BoxRepository boxRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductWithAmountRepository productWithAmountRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    public ApiResponse getAllProductSet() {
        try {
            List<ProductSet> allProductSet = productSetRepository.findAll();
            return new ApiResponse("Success", true, allProductSet.stream().map(this::getProductSetDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    private ProductSetDto getProductSetDto(ProductSet productSet) {
        Set<UUID> attachmentIds = new HashSet<>();
        for (Attachment attachment : productSet.getAttachments()) {
            attachmentIds.add(attachment.getId());
        }
        Set<UUID> boxAttachmentIds = new HashSet<>();
        for (Attachment attachment : productSet.getBox().getAttachments()) {
            boxAttachmentIds.add(attachment.getId());
        }
        ProductSetDto productSetDto = new ProductSetDto();
        productSetDto.setId(productSet.getId());
        productSetDto.setName(productSet.getName());
        productSetDto.setAttachmentIds(attachmentIds);
        productSetDto.setBoxDto(new BoxDto(
                productSet.getBox().getId(),
                productSet.getBox().getName(),
                new ColorDto(
                        productSet.getBox().getColor().getId(),
                        productSet.getBox().getColor().getName(),
                        productSet.getBox().getColor().getCode()
                ),
                new BoxCategoryDto(
                        productSet.getBox().getBoxCategory().getId(),
                        productSet.getBox().getBoxCategory().getName()
                ),
                boxAttachmentIds
        ));
        double price = 0;
        List<ProductWithAmountDto> productWithAmountDtoList = new ArrayList<>();
        for (ProductWithAmount productWithAmount : productSet.getProductWithAmounts()) {
            ProductWithAmountDto productWithAmountDto = orderService.getProductWithAmountDto(productWithAmount);
            productWithAmountDtoList.add(productWithAmountDto);
            price += productWithAmount.getAmount() * productWithAmount.getProduct().getPrice();
        }
        productSetDto.setProductWithAmountDtos(productWithAmountDtoList);
        productSetDto.setTotalPrice(price);
        return productSetDto;
    }


    public ApiResponse saveOrEditProductSet(ProductSetDto productSetDto) {
        try {
            ProductSet productSet = new ProductSet();
            if (productSetDto.getId() != null) {
                productSet = productSetRepository.findById(productSetDto.getId()).orElseThrow(() -> new IllegalStateException("ProductSet not found"));
                for (ProductWithAmount productWithAmount : productSet.getProductWithAmounts()) {
                    productWithAmountRepository.deleteProductWithAmountById(productWithAmount.getId());
                }
            }

            Set<Attachment> attachments = new HashSet<>();
            for (UUID attachmentId : productSetDto.getAttachmentIds()) {
                Attachment attachment = attachmentRepository.findById(attachmentId).orElseThrow(() -> new IllegalStateException("Attachment not found"));
                attachments.add(attachment);
            }

            productSet.setAttachments(attachments);
            productSet.setName(productSetDto.getName());
            productSet.setBox(boxRepository.findById(productSetDto.getBoxDto().getId()).orElseThrow(() -> new IllegalStateException("Box not found")));
            ProductSet saveProductSet = productSetRepository.save(productSet);
            for (ProductWithAmountDto productWithAmountDto : productSetDto.getProductWithAmountDtos()) {
                ProductWithAmount productWithAmount = new ProductWithAmount();
                productWithAmount.setProduct(productRepository.findById(productWithAmountDto.getProductDto().getId()).orElseThrow(() -> new IllegalStateException("Product not found")));
                productWithAmount.setAmount(productWithAmountDto.getAmount());
                if (productWithAmountDto.getContent() != null) {
                    productWithAmount.setContent(productWithAmountDto.getContent());
                }
                productWithAmount.setProductSet(saveProductSet);
                productWithAmountRepository.save(productWithAmount);
            }
            return new ApiResponse("Success", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }
}
