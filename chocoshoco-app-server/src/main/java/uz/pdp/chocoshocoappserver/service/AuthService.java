package uz.pdp.chocoshocoappserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.chocoshocoappserver.controller.AuthController;
import uz.pdp.chocoshocoappserver.entity.User;
import uz.pdp.chocoshocoappserver.entity.enums.RoleName;
import uz.pdp.chocoshocoappserver.payload.ApiResponse;
import uz.pdp.chocoshocoappserver.payload.ReqSignIn;
import uz.pdp.chocoshocoappserver.payload.UserDto;
import uz.pdp.chocoshocoappserver.repository.RoleRepository;
import uz.pdp.chocoshocoappserver.repository.UserRepository;


import java.util.Collections;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AuthController authController;

    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        return userRepository.findByPhoneNumber(phoneNumber);
    }

    public UserDetails loadUserById(UUID userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("User id not found: " + userId));
    }

    public ApiResponse registerUser(UserDto userDto) {
        try {
            User byPhoneNumber = userRepository.findByPhoneNumber(userDto.getPhoneNumber());
            if (byPhoneNumber != null)
                return new ApiResponse("This phone number is already registered", false);
            if (!userDto.getPassword().equals(userDto.getPrePassword()))
                return new ApiResponse("Parollar mos emas", false);
            User user = new User();
            user.setFirstName(userDto.getFirstName());
            user.setPhoneNumber(userDto.getPhoneNumber());
            user.setPassword(passwordEncoder.encode(userDto.getPassword()));
            user.setRoles(Collections.singletonList(roleRepository.findByRoleName(RoleName.ROLE_CLIENT)));
            userRepository.save(user);


            ReqSignIn reqSignIn = new ReqSignIn();
            reqSignIn.setPhoneNumber(userDto.getPhoneNumber());
            reqSignIn.setPassword(userDto.getPassword());
            String jwt = authController.getJwt(reqSignIn);
            return new ApiResponse("Success", true, jwt);
        }catch (Exception e) {
            return new ApiResponse("Error", false);
        }

    }

    public ApiResponse sendSms(UserDto userDto) {
        try {
            User byPhoneNumber = userRepository.findByPhoneNumber(userDto.getPhoneNumber());
            if (byPhoneNumber != null)
                return new ApiResponse("Bunday raqamli user mavjud", false);
            if (!userDto.getPassword().equals(userDto.getPrePassword()))
                return new ApiResponse("Parollar mos emas", false);
            return new ApiResponse("Success", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }

    }
}
