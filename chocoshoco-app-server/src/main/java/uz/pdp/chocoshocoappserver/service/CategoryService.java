package uz.pdp.chocoshocoappserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import uz.pdp.chocoshocoappserver.entity.Category;
import uz.pdp.chocoshocoappserver.exception.ResourceNotFoundException;
import uz.pdp.chocoshocoappserver.payload.ApiResponse;
import uz.pdp.chocoshocoappserver.payload.CategoryDto;
import uz.pdp.chocoshocoappserver.payload.PageableDto;
import uz.pdp.chocoshocoappserver.repository.CategoryRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CategoryService {
    @Autowired
    CategoryRepository categoryRepository;

    public ApiResponse saveOrEditCategory(CategoryDto categoryDto) {
        try {
            Category category = new Category();
            if (categoryDto.getId() != null)
                category = categoryRepository.findById(categoryDto.getId()).orElseThrow(() -> new IllegalStateException("Category"));
            category.setName(categoryDto.getName());
            if (categoryDto.getParentCategoryId() != null)
                category.setParentCategory(categoryRepository.findById(categoryDto.getParentCategoryId()).orElseThrow(() -> new IllegalStateException("Category")));
            categoryRepository.save(category);
            return new ApiResponse("Success", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
//        ApiResponse apiResponse = new ApiResponse();
//        try {
//            apiResponse.setSuccess(true);
//            apiResponse.setMessage("Saved");
//            Category category = new Category();
//            if (categoryDto.getId() != null) {
//                category = categoryRepository.findById(categoryDto.getId()).orElseThrow(() -> new ResourceNotFoundException("category", "id", categoryDto.getId()));
//                apiResponse.setMessage("Edited");
//            }
//            if (categoryDto.getParentCategoryId() != null) {
//                category.setParentCategory(categoryRepository.findById(categoryDto.getParentCategoryId()).orElseThrow(() -> new ResourceNotFoundException("parentCatId", "id", categoryDto.getParentCategoryId())));
//            }
//            category.setName(categoryDto.getName());
//            categoryRepository.save(category);
//        } catch (Exception e) {
//            apiResponse.setMessage("Error");
//            apiResponse.setSuccess(false);
//        }
//        return apiResponse;
    }

    public CategoryDto getCategoryDto(Category category) {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(category.getId());
        categoryDto.setName(category.getName());
        if (category.getParentCategory() != null) {
            categoryDto.setParentCategoryId(category.getParentCategory().getId());
            categoryDto.setParentCategoryName(category.getParentCategory().getName());
        }
        return categoryDto;
    }

    public ApiResponse getAll() {
        List<CategoryDto> categoryDtoList = new ArrayList<>();
        List<Category> all = categoryRepository.findAll();
        for (Category category : all) {
            categoryDtoList.add(getCategoryDto(category));
        }
        return new ApiResponse("OK", true, categoryDtoList);
    }

    public ApiResponse deleteCategory(UUID id) {
        try {
            categoryRepository.deleteById(id);
            return new ApiResponse("Deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }


    public ApiResponse getCategoryById(UUID id) {
        try {
            Category category = categoryRepository.findById(id).orElseThrow(() -> new IllegalStateException("CategoryId"));
            return new ApiResponse("Get", true, getCategoryDto(category));
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public PageableDto getAllByPageable(Integer page, Integer size) {
        List<CategoryDto> categoryDtoList = new ArrayList<>();
        Page<Category> categoryPage = categoryRepository.findAll(PageRequest.of(page, size, Sort.Direction.DESC));
        for (Category category : categoryPage) {
            categoryDtoList.add(getCategoryDto(category));
        }
        return new PageableDto(categoryDtoList, categoryPage.getTotalElements(), page);
    }

    public ApiResponse editCategory(Category category, UUID id) {
        Optional<Category> byId = categoryRepository.findById(id);
        if (!byId.isPresent())
            return new ApiResponse("not any ids", false);
        Category category1 = byId.get();
        category1.setName(category.getName());
        category1.setParentCategory(category.getParentCategory());
        categoryRepository.save(category1);
        return new ApiResponse("Edited", true, category1);
    }
}
