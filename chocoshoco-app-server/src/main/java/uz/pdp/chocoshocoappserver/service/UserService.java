package uz.pdp.chocoshocoappserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.chocoshocoappserver.entity.Role;
import uz.pdp.chocoshocoappserver.entity.User;
import uz.pdp.chocoshocoappserver.entity.enums.RoleName;
import uz.pdp.chocoshocoappserver.payload.ApiResponse;
import uz.pdp.chocoshocoappserver.payload.UserDto;
import uz.pdp.chocoshocoappserver.repository.RoleRepository;
import uz.pdp.chocoshocoappserver.repository.UserRepository;
import uz.pdp.chocoshocoappserver.utills.CommonUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;


    public ApiResponse adminAddUserOrEditOrUserRegister(UserDto userDto) {
        try {
            User user = new User();
            if (userDto.getId() != null) {
                user = userRepository.findById(userDto.getId()).orElseThrow(() -> new IllegalStateException("User Not Found"));
                if (userDto.getPassword() != null) {
                    if (userDto.getPassword().equals(userDto.getPrePassword()))
                        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
                    else
                        return new ApiResponse("Parollar mos emas", false);
                }
            } else {
                if (userDto.getRoleName() == null)
                    user.setRoles(Collections.singletonList(roleRepository.findByRoleName(RoleName.ROLE_CLIENT)));
                else
                    user.setRoles(Collections.singletonList(roleRepository.findByRoleName(userDto.getRoleName())));
                if (userDto.getPassword().equals(userDto.getPrePassword()))
                    user.setPassword(passwordEncoder.encode(userDto.getPassword()));
                else
                    return new ApiResponse("Parollar mos emas", false);
            }
            user.setFirstName(userDto.getFirstName());
            user.setPhoneNumber(userDto.getPhoneNumber());
            userRepository.save(user);
            return new ApiResponse(userDto.getId() != null ? "Edited" : "Saved", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }


    public ApiResponse getAllUsers(Integer page, Integer size) {
        Page<User> userPage;
        userPage = userRepository.findAll(CommonUtils.getPageableByCreatedAtDesc(page, size));
        return new ApiResponse("Ok", true, userPage.getContent().stream().map(this::getUserDto).collect(Collectors.toList()), userPage.getTotalElements(), page);
    }

    public UserDto getUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setFirstName(user.getFirstName());
        userDto.setPhoneNumber(user.getPhoneNumber());
        userDto.setRoles(user.getRoles());
        return userDto;
    }


    public ApiResponse getAllDriverOrUserByPageable(Integer page, Integer size, RoleName roleName) {
        try {
            List<User> userPage = userRepository.byUserRole(roleName.name(), page, size);
            return new ApiResponse("All driver", true, userPage.stream().map(this::getUserDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse searchUserByRoleName(String search, RoleName roleName) {
        try {
            List<User> userListBySearch = userRepository.findAllByPhoneNumberContainingIgnoreCaseOrFirstNameContainingIgnoreCase(search, search);

            if (roleName != null) {
                userListBySearch.removeIf(listBySearch -> !hasRole(listBySearch.getRoles(), roleName));
            }
            return new ApiResponse("Mana senga user list", true, userListBySearch);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public boolean hasRole(List<Role> roles, RoleName roleName) {
        for (Role role : roles) {
            if (role.getRoleName().equals(roleName))
                return true;
        }
        return false;
    }

    public ApiResponse editPassword(UserDto userDto) {
        try {
            User user = userRepository.findById(userDto.getId()).orElseThrow(() -> new IllegalStateException("User topilmadi"));
            if (passwordEncoder.matches(userDto.getPassword(), user.getPassword())) {
                if (userDto.getPassword().equals(userDto.getPrePassword()))
                    user.setPassword(passwordEncoder.encode(userDto.getPassword()));
                userRepository.save(user);
                return new ApiResponse("Successfully edited", true);
            }
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
        return new ApiResponse("Error", false);
    }

    public ApiResponse deleteUser(UUID id) {
        try {
            userRepository.deleteById(id);
            return new ApiResponse("Deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse blockUser(UUID id) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalStateException("user"));
        user.setEnabled(!user.isEnabled());
        userRepository.save(user);
        return new ApiResponse("Change enabled", true);
    }

    public ApiResponse editUserInfo(UUID id, UserDto userDto) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            user.setFirstName(userDto.getFirstName());
            user.setPhoneNumber(userDto.getPhoneNumber());
            userRepository.save(user);
            return new ApiResponse("Edited", true);
        } else {
            return new ApiResponse("Error", false);
        }

    }

    public ApiResponse registerUser(UserDto userDto) {
        return null;
    }
}
