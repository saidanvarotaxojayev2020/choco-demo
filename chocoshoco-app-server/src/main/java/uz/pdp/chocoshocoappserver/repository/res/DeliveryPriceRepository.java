package uz.pdp.chocoshocoappserver.repository.res;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.chocoshocoappserver.entity.DeliveryPrice;
import uz.pdp.chocoshocoappserver.projection.CustomDeliveryPrice;

import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "deliveryPrice", collectionResourceRel ="list",excerptProjection = CustomDeliveryPrice.class)
public interface DeliveryPriceRepository extends JpaRepository<DeliveryPrice, UUID> {
    DeliveryPrice findByDistrictId(Integer district_id);
}
