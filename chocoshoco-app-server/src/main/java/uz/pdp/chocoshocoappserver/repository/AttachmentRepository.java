package uz.pdp.chocoshocoappserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.chocoshocoappserver.entity.Attachment;

import java.util.UUID;
@CrossOrigin

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
    void deleteAllById(UUID id);
}
