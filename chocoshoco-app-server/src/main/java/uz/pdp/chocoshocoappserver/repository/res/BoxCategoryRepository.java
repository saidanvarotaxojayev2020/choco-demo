package uz.pdp.chocoshocoappserver.repository.res;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.chocoshocoappserver.entity.BoxCategory;
import uz.pdp.chocoshocoappserver.projection.CustomerBoxCategory;

import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "boxCategory", collectionResourceRel = "list", excerptProjection = CustomerBoxCategory.class)
public interface BoxCategoryRepository extends JpaRepository<BoxCategory, UUID> {

}
