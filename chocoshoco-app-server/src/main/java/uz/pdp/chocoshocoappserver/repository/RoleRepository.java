package uz.pdp.chocoshocoappserver.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.chocoshocoappserver.entity.Role;
import uz.pdp.chocoshocoappserver.entity.enums.RoleName;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByRoleName(RoleName roleName);
}
