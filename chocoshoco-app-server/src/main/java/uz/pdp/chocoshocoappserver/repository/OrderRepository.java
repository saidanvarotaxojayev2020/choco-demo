package uz.pdp.chocoshocoappserver.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.chocoshocoappserver.entity.Order;
import uz.pdp.chocoshocoappserver.entity.enums.OrderStatus;

import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {
    Page<Order> findAllByUserId(UUID user_id, Pageable pageable);
    Page<Order> findAllByUserIdAndOrderStatus(UUID user_id, OrderStatus orderStatus, Pageable pageable);
    Page<Order> findAllByOrderStatus(OrderStatus orderStatus, Pageable pageable);
}
