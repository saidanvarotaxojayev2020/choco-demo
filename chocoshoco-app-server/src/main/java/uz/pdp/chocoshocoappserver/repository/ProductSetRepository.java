package uz.pdp.chocoshocoappserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.chocoshocoappserver.entity.ProductSet;

import java.util.UUID;

public interface ProductSetRepository extends JpaRepository<ProductSet, UUID> {
}
