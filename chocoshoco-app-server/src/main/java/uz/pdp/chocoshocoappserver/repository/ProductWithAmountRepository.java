package uz.pdp.chocoshocoappserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.chocoshocoappserver.entity.ProductWithAmount;

import java.util.List;
import java.util.UUID;

public interface ProductWithAmountRepository extends JpaRepository<ProductWithAmount, UUID> {

    @Transactional
    @Modifying
    @Query(value = "DELETE from product_with_amount where id=:productWithAmountId", nativeQuery = true)
    void deleteProductWithAmountById(@Param(value = "productWithAmountId") UUID productWithAmountId);

    List<ProductWithAmount> findAllByOrderId(UUID order_id);
}
