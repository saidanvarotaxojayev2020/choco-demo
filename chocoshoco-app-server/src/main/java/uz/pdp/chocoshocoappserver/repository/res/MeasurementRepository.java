package uz.pdp.chocoshocoappserver.repository.res;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.chocoshocoappserver.entity.Measurement;
import uz.pdp.chocoshocoappserver.projection.CustomDistrict;
import uz.pdp.chocoshocoappserver.projection.CustomMeasurement;

import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "measurement",collectionResourceRel = "list", excerptProjection = CustomMeasurement.class)
public interface MeasurementRepository extends JpaRepository<Measurement, UUID> {
}
