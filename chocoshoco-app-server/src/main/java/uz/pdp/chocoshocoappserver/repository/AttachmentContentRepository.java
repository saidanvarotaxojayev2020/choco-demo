package uz.pdp.chocoshocoappserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.chocoshocoappserver.entity.AttachmentContent;

import java.util.Optional;
import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {
    Optional<AttachmentContent> findByAttachmentId(UUID attachment_id);
}
