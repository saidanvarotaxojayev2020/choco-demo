package uz.pdp.chocoshocoappserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.chocoshocoappserver.entity.Category;

import java.util.UUID;

public interface CategoryRepository extends JpaRepository<Category, UUID> {
}
