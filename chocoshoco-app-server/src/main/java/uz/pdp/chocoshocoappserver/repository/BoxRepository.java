package uz.pdp.chocoshocoappserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.chocoshocoappserver.entity.Box;

import java.util.List;
import java.util.UUID;

public interface BoxRepository extends JpaRepository<Box, UUID> {
    List<Box> findAllByBoxCategoryId(UUID boxCategory_id);
}
