package uz.pdp.chocoshocoappserver.repository.res;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.chocoshocoappserver.entity.Address;
import uz.pdp.chocoshocoappserver.projection.CustomAddress;


import java.util.UUID;


@RepositoryRestResource(path = "address",collectionResourceRel = "list", excerptProjection = CustomAddress.class)
public interface AddressRepository extends JpaRepository<Address, UUID> {

}
