package uz.pdp.chocoshocoappserver.repository.res;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.chocoshocoappserver.entity.Color;
import uz.pdp.chocoshocoappserver.projection.CustomColor;

import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "color",collectionResourceRel="list", excerptProjection = CustomColor.class)
public interface ColorRepository extends JpaRepository<Color, UUID> {

}
