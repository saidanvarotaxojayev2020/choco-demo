package uz.pdp.chocoshocoappserver.repository.res;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import uz.pdp.chocoshocoappserver.entity.District;
import uz.pdp.chocoshocoappserver.projection.CustomDistrict;

import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "district",collectionResourceRel = "list", excerptProjection = CustomDistrict.class)
public interface DistrictRepository extends JpaRepository<District,Integer> {

    @Query(nativeQuery = true,value = "select id,name from district where id=(select district_id from address where id=:addressId)")
    CustomDistrict districtByAddressId(@PathVariable(value = "addressId")UUID addressId);

    @Query(nativeQuery = true,value = "select name from district where id=:id")
    String getDistrictNameById(Integer id);

}
