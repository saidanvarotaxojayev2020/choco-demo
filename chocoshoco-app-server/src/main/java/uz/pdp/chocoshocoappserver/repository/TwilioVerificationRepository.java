package uz.pdp.chocoshocoappserver.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.chocoshocoappserver.entity.TwilioVerification;

import java.util.Optional;
import java.util.UUID;

public interface TwilioVerificationRepository extends JpaRepository<TwilioVerification, UUID> {
    Optional<TwilioVerification> findByPhoneNumber(String phoneNumber);
    Optional<TwilioVerification> findByPhoneNumberAndCodeAndVerifiedFalse(String phoneNumber, int code);
}
