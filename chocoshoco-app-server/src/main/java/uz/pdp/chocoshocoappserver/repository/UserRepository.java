package uz.pdp.chocoshocoappserver.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.pdp.chocoshocoappserver.entity.User;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    User findByPhoneNumber(String phoneNumber);

//    Optional<User> findByPhoneNumber(String phoneNumber);

    @Modifying
    @Query(value = "select users.* from users join users_roles ur on users.id = ur.users_id join role r on ur.roles_id = r.id and r.role_name = :ketmon limit :size offset (:page * :size)", nativeQuery = true)
    List<User> byUserRole(@Param(value = "ketmon") String roleName, @Param(value = "page") Integer page, @Param(value = "size") Integer size);

    List<User> findAllByPhoneNumberContainingIgnoreCaseOrFirstNameContainingIgnoreCase(String phoneNumber, String firstName);}
