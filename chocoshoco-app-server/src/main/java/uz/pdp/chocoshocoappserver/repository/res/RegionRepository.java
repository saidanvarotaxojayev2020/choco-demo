package uz.pdp.chocoshocoappserver.repository.res;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.chocoshocoappserver.entity.Region;
import uz.pdp.chocoshocoappserver.projection.CustomRegion;


@CrossOrigin
@RepositoryRestResource(path = "region", collectionResourceRel = "list", excerptProjection = CustomRegion.class)
public interface RegionRepository extends JpaRepository<Region, Integer> {

    @Query(value = "select id, name " +
            "from region where id = " +
            "(select region_id from district where id=:districtId)", nativeQuery = true)
    Region byId(@Param("districtId") Integer districtId);
}
