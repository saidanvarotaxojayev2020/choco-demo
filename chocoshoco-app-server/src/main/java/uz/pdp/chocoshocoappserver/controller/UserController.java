package uz.pdp.chocoshocoappserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.chocoshocoappserver.entity.User;
import uz.pdp.chocoshocoappserver.entity.enums.RoleName;
import uz.pdp.chocoshocoappserver.payload.ApiResponse;
import uz.pdp.chocoshocoappserver.payload.UserDto;
import uz.pdp.chocoshocoappserver.security.CurrentUser;
import uz.pdp.chocoshocoappserver.service.UserService;
import uz.pdp.chocoshocoappserver.utills.ApplicationConstance;


import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("api/user")
public class UserController {

    @Autowired
    UserService userService;


    @PostMapping("/adminAddUserOrEditOrUserRegister")
    public HttpEntity<?> adminAddUserOrEditOrUserRegister(@RequestBody UserDto userDto) {
        ApiResponse apiResponse = userService.adminAddUserOrEditOrUserRegister(userDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping("/allUserByPageable")
    public HttpEntity<?> getAllUsers(
            @RequestParam(value = "page", defaultValue = ApplicationConstance.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(value = "size", defaultValue = ApplicationConstance.DEFAULT_PAGE_SIZE) Integer size) {
        ApiResponse apiResponse = userService.getAllUsers(page, size);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping("/getAllDriverOrUserByPageable")
    public HttpEntity<?> getAllDriverOrUserByPageable(
            @RequestParam(value = "page", defaultValue = ApplicationConstance.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(value = "size", defaultValue = ApplicationConstance.DEFAULT_PAGE_SIZE) Integer size,
            @RequestParam(value = "roleName") RoleName roleName) {
        ApiResponse apiResponse = userService.getAllDriverOrUserByPageable(page, size, roleName);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/searchUserByRoleName")
    public HttpEntity<?> searchUserByRoleName(
            @RequestParam(value = "search") String search,
            @RequestParam(value = "roleName", defaultValue = "") RoleName roleName) {
        ApiResponse apiResponse = userService.searchUserByRoleName(search, roleName);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PutMapping("/editUserInfo/{id}")
    public HttpEntity<?> editUserInfo(@PathVariable UUID id, @RequestBody UserDto userDto) {
        ApiResponse apiResponse = userService.editUserInfo(id, userDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PutMapping("/editPassword")
    public HttpEntity<?> editPassword(@RequestBody UserDto userDto) {
        ApiResponse apiResponse = userService.editPassword(userDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteUser(@PathVariable UUID id) {
        ApiResponse apiResponse = userService.deleteUser(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PutMapping("/blockUser/{id}")
    public HttpEntity<?> blockUser(@PathVariable UUID id) {
        ApiResponse apiResponse = userService.blockUser(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping("/me")
    public HttpEntity<?> me(@CurrentUser User user) {
        return ResponseEntity.status(user != null ? 200 : 409).body(user);
    }
}

