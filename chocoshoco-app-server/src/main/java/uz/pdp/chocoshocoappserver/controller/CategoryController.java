package uz.pdp.chocoshocoappserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.chocoshocoappserver.entity.Category;
import uz.pdp.chocoshocoappserver.payload.ApiResponse;
import uz.pdp.chocoshocoappserver.payload.CategoryDto;
import uz.pdp.chocoshocoappserver.service.CategoryService;
import uz.pdp.chocoshocoappserver.utills.ApplicationConstance;

import java.util.UUID;

@RestController
@RequestMapping("/api/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @PostMapping
    public HttpEntity<?> saveOrEditCategory(@RequestBody CategoryDto categoryDto) {
        ApiResponse response = categoryService.saveOrEditCategory(categoryDto);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editCategory(@RequestBody Category category, @PathVariable UUID id) {
        ApiResponse response = categoryService.editCategory(category, id);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }

    @GetMapping("/all")
    public ApiResponse getAll() {
        return categoryService.getAll();
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteCategory(@PathVariable UUID id) {
        ApiResponse response = categoryService.deleteCategory(id);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getCategoryById(@PathVariable UUID id) {
        ApiResponse response = categoryService.getCategoryById(id);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/getAllCategoriesByPageable")
    public HttpEntity<?> getAllByPageable(@RequestParam(value = "page", defaultValue = ApplicationConstance.DEFAULT_PAGE_NUMBER) Integer page,
                                          @RequestParam(value = "size", defaultValue = ApplicationConstance.DEFAULT_PAGE_SIZE) Integer size) {
        return ResponseEntity.ok(categoryService.getAllByPageable(page, size));
    }
}
