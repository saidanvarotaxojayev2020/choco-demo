package uz.pdp.chocoshocoappserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.chocoshocoappserver.entity.enums.OrderStatus;
import uz.pdp.chocoshocoappserver.payload.ApiResponse;
import uz.pdp.chocoshocoappserver.payload.OrderDto;
import uz.pdp.chocoshocoappserver.service.OrderService;
import uz.pdp.chocoshocoappserver.utills.ApplicationConstance;

import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping
    public HttpEntity<?> saveOrEditOrder(@RequestBody OrderDto orderDto) {
        ApiResponse apiResponse = orderService.saveOrEditOrder(orderDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<?> getOrderList(@RequestParam(value = "page", defaultValue = ApplicationConstance.DEFAULT_PAGE_NUMBER) Integer page,
                                      @RequestParam(value = "size", defaultValue = ApplicationConstance.DEFAULT_PAGE_SIZE) Integer size,
                                      @RequestParam(value = "orderStatus", required = false) OrderStatus orderStatus,
                                      @RequestParam(value = "search", defaultValue = "all") String search,
                                      @RequestParam(value = "userId", required = false) UUID userId) {
        ApiResponse apiResponse = orderService.getAllOrderByPageable(page, size, orderStatus, search, userId);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteOrder(@PathVariable UUID id){
        ApiResponse apiResponse = orderService.deleteOrder(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PutMapping("/editStatus")
    public HttpEntity<?> editOrderStatus(
            @RequestParam(value = "orderId", required = false) UUID orderId,
            @RequestParam(value = "orderStatus", required = false) OrderStatus orderStatus) {
        ApiResponse apiResponse = orderService.editOrderStatus(orderId, orderStatus);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

}
