package uz.pdp.chocoshocoappserver.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.chocoshocoappserver.payload.ApiResponse;
import uz.pdp.chocoshocoappserver.payload.ProductSetDto;
import uz.pdp.chocoshocoappserver.repository.ProductSetRepository;
import uz.pdp.chocoshocoappserver.service.ProductSetService;

import java.util.UUID;

@RestController
@RequestMapping("/api/productSet")
public class ProductSetController {

    @Autowired
    ProductSetService productSetService;

    @Autowired
    ProductSetRepository productSetRepository;

    @GetMapping
    public HttpEntity<?> getAllProductSet() {
        ApiResponse allProductSet = productSetService.getAllProductSet();
        return ResponseEntity.status(allProductSet.isSuccess() ? 200 : 409).body(allProductSet);
    }

    @PostMapping
    public HttpEntity<?> saveOrEditProductSet(@RequestBody ProductSetDto productSetDto) {
        ApiResponse allProductSet = productSetService.saveOrEditProductSet(productSetDto);
        return ResponseEntity.status(allProductSet.isSuccess() ? 200 : 409).body(allProductSet);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteById(@PathVariable UUID id) {
        ApiResponse apiResponse = new ApiResponse();
        try {
            productSetRepository.deleteById(id);
            apiResponse.setMessage("Deleted");
            apiResponse.setSuccess(true);
        } catch (Exception e) {
            apiResponse.setMessage("Error");
            apiResponse.setSuccess(false);
        }
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
}
