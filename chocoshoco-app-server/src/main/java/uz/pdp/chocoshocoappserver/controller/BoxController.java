package uz.pdp.chocoshocoappserver.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.chocoshocoappserver.payload.ApiResponse;
import uz.pdp.chocoshocoappserver.payload.BoxDto;
import uz.pdp.chocoshocoappserver.service.BoxService;

import java.util.UUID;


@RestController
@RequestMapping("/api/box")
public class BoxController {

    @Autowired
    BoxService boxService;

    @PostMapping
    public HttpEntity<?> savOrEditBox(@RequestBody BoxDto boxDto){
        ApiResponse apiResponse = boxService.savOrEditBox( boxDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<?> getBox(){
        ApiResponse apiResponse = boxService.getAllBox();
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping("/getBoxByCategoryId/{id}")
    public HttpEntity<?> getBoxByCategoryId(@PathVariable UUID id){
        ApiResponse apiResponse = boxService.getBoxByCategoryId(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteBox(@PathVariable UUID id){
        ApiResponse apiResponse = boxService.deleteBox(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

}
