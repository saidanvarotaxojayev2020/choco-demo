package uz.pdp.chocoshocoappserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.chocoshocoappserver.payload.ApiResponse;
import uz.pdp.chocoshocoappserver.payload.TwilioDto;
import uz.pdp.chocoshocoappserver.payload.UserDto;
import uz.pdp.chocoshocoappserver.service.TwilioVerificationService;


@RestController
@RequestMapping("/api/twilio")
public class TwilioVerificationController {

    @Autowired
    TwilioVerificationService twilioVerificationService;

    @GetMapping
    public HttpEntity<?> getSmsCode(@RequestBody TwilioDto twilioDto) {
        ApiResponse apiResponse = twilioVerificationService.getSmsCode(twilioDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PostMapping
    public HttpEntity<?> checkPhoneAndCode(@RequestBody TwilioDto twilioDto){
        ApiResponse apiResponse = twilioVerificationService.checkPhoneAndCode(twilioDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PutMapping
    public HttpEntity<?> editPassword(@RequestBody UserDto userDto) {
        ApiResponse apiResponse = twilioVerificationService.editPassword(userDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }



}
