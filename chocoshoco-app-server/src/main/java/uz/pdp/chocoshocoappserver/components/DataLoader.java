package uz.pdp.chocoshocoappserver.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.chocoshocoappserver.entity.Role;
import uz.pdp.chocoshocoappserver.entity.User;
import uz.pdp.chocoshocoappserver.entity.enums.RoleName;
import uz.pdp.chocoshocoappserver.repository.RoleRepository;
import uz.pdp.chocoshocoappserver.repository.UserRepository;


import java.util.Collections;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")){
            Role admin = roleRepository.save(new Role(RoleName.ROLE_ADMIN));

            Role client = roleRepository.save(new Role(RoleName.ROLE_CLIENT));
//
//            Date date = new Date(1998-01-20);

            userRepository.save(
                    new User(
                            "Saidanvar",
                            "111",
                            passwordEncoder.encode("111"),
                            Collections.singletonList(admin)
                    )
            );

            userRepository.save(
                    new User(
                            "Ma'ruf",
                            "234",
                            passwordEncoder.encode("234"),
                            Collections.singletonList(client)
                    )
            );
        }
    }
}
