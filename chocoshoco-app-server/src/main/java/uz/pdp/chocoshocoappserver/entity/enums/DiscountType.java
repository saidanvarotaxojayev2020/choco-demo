package uz.pdp.chocoshocoappserver.entity.enums;

public enum DiscountType {
    SUM,
    PERCENT
}
