package uz.pdp.chocoshocoappserver.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.chocoshocoappserver.entity.enums.DiscountType;
import uz.pdp.chocoshocoappserver.entity.template.AbsEntity;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class OrderDiscount extends AbsEntity {

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    private Order order;

    private Double discountAmount;

    @Enumerated(EnumType.STRING)
    private DiscountType discountType;
}
