package uz.pdp.chocoshocoappserver.entity.enums;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_CLIENT,
}
