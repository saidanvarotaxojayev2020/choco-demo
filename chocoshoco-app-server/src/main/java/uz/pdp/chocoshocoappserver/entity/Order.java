package uz.pdp.chocoshocoappserver.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.chocoshocoappserver.entity.enums.OrderStatus;
import uz.pdp.chocoshocoappserver.entity.template.AbsEntity;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "orders")
public class Order extends AbsEntity {

    private Double totalPrice;

    @Column(nullable = false)
    private Date date;

    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private Address address;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Box box;

    @ManyToOne(fetch = FetchType.LAZY)
    private DeliveryPrice deliveryPrice;

    private String message;

    @Column(nullable = false)
    private String clientNumber;

    @JsonBackReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order", cascade = CascadeType.ALL)
    private List<ProductWithAmount> productWithAmounts;

    @JsonBackReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order", cascade = CascadeType.ALL)
    private List<OrderDiscount> orderDiscounts;

}
