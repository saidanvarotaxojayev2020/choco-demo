package uz.pdp.chocoshocoappserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.chocoshocoappserver.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Address extends AbsEntity {

    @Column(nullable = false)
    private String street;

    @Column(nullable = false)
    private String House;

    private Double lat;

    private Double lon;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private District district;



}
