package uz.pdp.chocoshocoappserver.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.chocoshocoappserver.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ProductSet extends AbsEntity {

    private String name;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Box box;

    @OneToMany
    private Set<Attachment> attachments;

    @JsonBackReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "productSet", cascade = CascadeType.ALL)
    private List<ProductWithAmount> productWithAmounts;
}

