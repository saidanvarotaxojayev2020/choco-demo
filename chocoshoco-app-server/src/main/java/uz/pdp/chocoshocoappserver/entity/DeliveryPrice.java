package uz.pdp.chocoshocoappserver.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.chocoshocoappserver.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class DeliveryPrice extends AbsEntity {

//    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private District district;

    @Column(nullable = false)
    private double price;
}
