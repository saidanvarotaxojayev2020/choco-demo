package uz.pdp.chocoshocoappserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.chocoshocoappserver.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Box extends AbsEntity {

    @Column(nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private Color color;

    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private BoxCategory boxCategory;

    @OneToMany
    private Set<Attachment> attachments;

}
