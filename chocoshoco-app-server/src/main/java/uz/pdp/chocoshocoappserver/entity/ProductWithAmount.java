package uz.pdp.chocoshocoappserver.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.chocoshocoappserver.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ProductWithAmount extends AbsEntity {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Product product;

    @Column(nullable = false)
    private Integer amount;

    private String content;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    private Order order;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductSet productSet;

}
