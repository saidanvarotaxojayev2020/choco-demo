package uz.pdp.chocoshocoappserver.entity.enums;


public enum OrderStatus {
    NEW,
    IN_PROGRESS,
    COMPLETED,
    FINISHED,
    CANCELED;
}
