package uz.pdp.chocoshocoappserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.chocoshocoappserver.entity.District;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressDto {

    private UUID id;

    private String street;

    private String house;

    private Double lat;

    private Double lon;

    private DistrictDto districtDto;
}
