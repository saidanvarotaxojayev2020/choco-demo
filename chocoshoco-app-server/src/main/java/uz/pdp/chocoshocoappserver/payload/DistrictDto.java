package uz.pdp.chocoshocoappserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class DistrictDto {

    private Integer id;

    private String name;

    private RegionDto regionDto;

    public DistrictDto(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
