package uz.pdp.chocoshocoappserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.chocoshocoappserver.entity.enums.DiscountType;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDiscountDto {

    private UUID id;

    private Double discountAmount;

    private DiscountType discountType;
}
