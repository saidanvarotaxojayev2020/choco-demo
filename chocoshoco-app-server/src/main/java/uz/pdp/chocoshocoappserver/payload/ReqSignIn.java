package uz.pdp.chocoshocoappserver.payload;

import lombok.Data;

@Data
public class ReqSignIn {
    private String phoneNumber;
    private String password;
}
