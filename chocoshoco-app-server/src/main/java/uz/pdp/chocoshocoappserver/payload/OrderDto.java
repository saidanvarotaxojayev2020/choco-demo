package uz.pdp.chocoshocoappserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.chocoshocoappserver.entity.enums.OrderStatus;

import java.sql.Date;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

    private UUID id;

    private Double totalPrice;

    private Date date;

    private OrderStatus orderStatus;

    private UserDto userDto;

    private AddressDto addressDto;

    private BoxDto boxDto;

    private String clientNumber;

    private DeliveryPriceDto deliveryPriceDto;

    List<ProductWithAmountDto> productWithAmountDtos;

    List<OrderDiscountDto> orderDiscountDtos;

    private String message;

}
