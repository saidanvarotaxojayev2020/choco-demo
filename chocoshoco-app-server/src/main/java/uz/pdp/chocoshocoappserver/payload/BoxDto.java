package uz.pdp.chocoshocoappserver.payload;


import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.Set;

import java.util.UUID;

@Data
@NoArgsConstructor
public class BoxDto {

    private UUID id;

    private String name;

    private ColorDto colorDto;

    private BoxCategoryDto boxCategoryDto;

    private Set<UUID> attachmentIds;

    public BoxDto(UUID id, String name, ColorDto colorDto, Set<UUID> attachmentIds) {
        this.id = id;
        this.name = name;
        this.colorDto = colorDto;
        this.attachmentIds = attachmentIds;
    }

    public BoxDto(UUID id, String name, ColorDto colorDto, BoxCategoryDto boxCategoryDto, Set<UUID> attachmentIds) {
        this.id = id;
        this.name = name;
        this.colorDto = colorDto;
        this.boxCategoryDto = boxCategoryDto;
        this.attachmentIds = attachmentIds;
    }
}
