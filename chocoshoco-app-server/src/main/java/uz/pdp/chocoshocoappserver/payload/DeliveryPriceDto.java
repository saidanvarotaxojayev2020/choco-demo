package uz.pdp.chocoshocoappserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryPriceDto {

    private UUID id;

    private DistrictDto districtDto;

    private double price;

    public DeliveryPriceDto(UUID id, double price) {
        this.id = id;
        this.price = price;
    }
}
