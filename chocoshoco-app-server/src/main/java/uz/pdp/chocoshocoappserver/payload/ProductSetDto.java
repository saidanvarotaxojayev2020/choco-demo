package uz.pdp.chocoshocoappserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.chocoshocoappserver.entity.Box;


import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductSetDto {

    private UUID id;

    private String name;

    private BoxDto boxDto;

    private Set<UUID> attachmentIds;

    private List<ProductWithAmountDto> productWithAmountDtos;

    private Double totalPrice;
}
