package uz.pdp.chocoshocoappserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductWithAmountDto {

    private UUID id;

    private ProductDto productDto;

    private Integer amount;

    private String content;

    private OrderDto orderDto;

    private ProductSetDto productSetDto;

    public ProductWithAmountDto(UUID id, ProductDto productDto, Integer amount, String content) {
        this.id = id;
        this.productDto = productDto;
        this.amount = amount;
        this.content = content;
    }
}
