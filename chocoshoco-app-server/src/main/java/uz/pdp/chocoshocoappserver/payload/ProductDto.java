package uz.pdp.chocoshocoappserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {

    private UUID id;

    private String name;

    private Double price;

    private double measureValue;

    private boolean hasContent;

    private ColorDto colorDto;

    private CategoryDto categoryDto;

    private MeasurementDto measurementDto;

    private List<UUID> attachmentIds;

}
