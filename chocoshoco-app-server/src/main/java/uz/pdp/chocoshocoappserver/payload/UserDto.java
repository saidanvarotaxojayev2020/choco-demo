package uz.pdp.chocoshocoappserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.chocoshocoappserver.entity.Role;
import uz.pdp.chocoshocoappserver.entity.enums.RoleName;


import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private UUID id;
    private String firstName;
    private String phoneNumber;
    private String password;
    private String prePassword;
    private RoleName roleName;
    private List<Role> roles;

    public UserDto(UUID id, String firstName, String phoneNumber, String password, String prePassword) {
        this.id = id;
        this.firstName = firstName;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.prePassword = prePassword;
    }

    public UserDto(UUID id, String firstName,  String phoneNumber) {
        this.id = id;
        this.firstName = firstName;
        this.phoneNumber = phoneNumber;
    }

    public UserDto(UUID id, String firstName, String phoneNumber, RoleName roleName) {
        this.id = id;
        this.firstName = firstName;
        this.phoneNumber = phoneNumber;
        this.roleName = roleName;
    }
}
