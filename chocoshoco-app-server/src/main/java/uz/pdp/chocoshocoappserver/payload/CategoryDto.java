package uz.pdp.chocoshocoappserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.UUID;

@Data
@NoArgsConstructor
public class CategoryDto {

    private UUID id;

    private String name;

    private UUID parentCategoryId;

    private String parentCategoryName;

    public CategoryDto(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public CategoryDto(UUID id, String name, UUID parentCategoryId) {
        this.id = id;
        this.name = name;
        this.parentCategoryId = parentCategoryId;
    }

}
