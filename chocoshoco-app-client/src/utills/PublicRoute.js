import React from 'react';
import {Route} from 'react-router-dom';
// import {ORDER,NEW_USER_FOR_ORDER,ATTACHMENT, URL_ORDER,REGISTER_AGENT} from "./constants";
import {useDispatch} from "react-redux";

const PublicRoute = ({component: Component, ...rest}) => {
    return (<Route
            {...rest}
            render={props =>
                <Component {...props}/>
            }
        />
    )
};
export default PublicRoute;
