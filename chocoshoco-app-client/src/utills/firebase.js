import firebase from 'firebase/app';
import 'firebase/auth';

const prodConfig = {
    // Your firebase project. CDN firebaseConfig
    apiKey: "AIzaSyCAfjxg_ybrr8wxzuPljfYLeAgbWaSHIvg",
    authDomain: "chocoshoco.firebaseapp.com",
    projectId: "chocoshoco",
    storageBucket: "chocoshoco.appspot.com",
    messagingSenderId: "597235195776",
    appId: "1:597235195776:web:a600413b874570d5b66b65",
    measurementId: "G-KZM5HYX4EW"
};

const devConfig = {
    // Your firebase project. CDN firebaseConfig
    apiKey: "AIzaSyCAfjxg_ybrr8wxzuPljfYLeAgbWaSHIvg",
    authDomain: "chocoshoco.firebaseapp.com",
    projectId: "chocoshoco",
    storageBucket: "chocoshoco.appspot.com",
    messagingSenderId: "597235195776",
    appId: "1:597235195776:web:a600413b874570d5b66b65",
    measurementId: "G-KZM5HYX4EW"
};

const config = process.env.NODE_ENV === 'production'
    ? prodConfig
    : devConfig;

if (!firebase.apps.length){
    firebase.initializeApp(config)
}

const firebaseAuth = firebase.auth();

export {
    firebaseAuth, firebase
}