import React from 'react'
import {useAlert} from "./alertContext"

export default function Alert() {
    const alert = useAlert()

    if (!alert) return null

    return (
        <div className={'alert alert-danger'}>
            Lorem ipsum dolor sit amet.
        </div>
    )

}



