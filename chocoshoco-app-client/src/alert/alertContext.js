import React, {useState} from 'react'

export const AlertContext = React.createContext()
export const AlertToggleContext = React.createContext()

export const useAlert = () => {
    return useAlert(AlertContext)
}
export const useAlertToggle = () => {

    return useAlert(AlertToggleContext)
}

export const AlertProvider = ({children}) => {

    const [alert, setAlert] = useState(false)

    const toggle = () => setAlert(prev => !prev)

    return (
        <AlertContext.Provider value={alert}>
            <AlertToggleContext.Provider value={toggle}>
                {children}
            </AlertToggleContext.Provider>
        </AlertContext.Provider>
    )
}