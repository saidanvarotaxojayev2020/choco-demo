import React from 'react'
import {Route, Switch} from "react-router-dom";
import Error404 from "./pages/errorPages/Error404";
import AdminMain from "./pages/admin/AdminMain";
import CustomerMain from "./pages/customer/CustomerMain";
import BoxCategory from "./pages/admin/BoxCategory";
import Category from "./pages/admin/Category";
import Color from "./pages/admin/Color";
import DeliveryPrice from "./pages/admin/DeliveryPrice";
import District from "./pages/admin/District";
import Measurement from "./pages/admin/Measurement";
import Region from "./pages/admin/Region";
import Box from "./pages/admin/Box";
import Order from "./pages/admin/Order";
import Product from "./pages/admin/Product";
import AdminClient from "./pages/admin/AdminClient";
import SignUpIn from "./pages/SignUpIn";
import CustomerBasket from "./pages/customer/CustomerBasket";
import "bootstrap/dist/css/bootstrap.min.css"
import CustomerOrder from "./pages/customer/CustomerOrder";
import ProductSet from "./pages/admin/ProductSet";

function App() {
    return (
        <div>
            <React.Fragment>
                <Switch>
                    <Route exact path="/" component={CustomerMain}/>
                    <Route path="/pages/signUpIn" component={SignUpIn}/>
                    <Route path="/admin/adminMain" component={AdminMain}/>
                    <Route path="/admin/adminClient" component={AdminClient}/>
                    <Route path="/admin/boxCategory" component={BoxCategory}/>
                    <Route path="/admin/category" component={Category}/>
                    <Route path="/admin/color" component={Color}/>
                    <Route path="/admin/deliveryPrice" component={DeliveryPrice}/>
                    <Route path="/admin/productSet" component={ProductSet}/>
                    <Route path="/admin/district" component={District}/>
                    <Route path="/admin/measurement" component={Measurement}/>
                    <Route path="/admin/region" component={Region}/>
                    <Route path="/admin/box" component={Box}/>
                    <Route path="/admin/order" component={Order}/>
                    <Route path="/admin/product" component={Product}/>
                    <Route path="/customer/customerBasket" component={CustomerBasket}/>
                    <Route path="/customer/customerOrder" component={CustomerOrder}/>
                    <Route path="/404" component={Error404}/>
                    <Route component={Error404}/>
                </Switch>
            </React.Fragment>
        </div>
    )
}

export default App;
