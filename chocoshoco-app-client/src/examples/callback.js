import React,{useState,useMemo,useEffect,useCallback} from 'react';
import ItemsList from "./itemsList";

function App() {
    const [count,setCount]=useState(1)
    const [colored,setColored]=useState(false)

    const styles=useMemo(()=>({
        color:colored ?'darkred':'black'
    }),[colored])

    const generateItemsFromAPI=useCallback((indexNumber)=> {
        return new Array(count).fill('').map((_, i) =>'Element $ {i + indexNumber}')
    },[count])

    return(
        <>
            <h1 style={styles}> son  :{count}</h1>
            <button className={'btn btn-success'}  onClick={()=>setCount(   prev=> prev+1)}>button +</button>
            <button className={'btn btn-warning'} onClick={()=>setColored(  prev=> !prev)}>button </button>

            <ItemsList getItems={generateItemsFromAPI}/>
        </>
    )


}

export default App
