import React, {useEffect, useState} from 'react';


function App() {

    const [type,setType]=useState('users')

    const [data,setData]=useState([])
    const [pos,setPos]=useState({
        x:0, y:0
    })
    // console.log('component render')
    //    useEffect(()=>{
    //     console.log('render')
    // })

    useEffect(()=>{
        console.log('Type,change',type)
        fetch(`https://jsonplaceholder.typicode.com/${type}`)
            .then(response => response.json())
            .then(json => setData(json))
    }, [type])

    const mouseMoveHandler  =event =>{
        setPos({
            x:event.clientX,
            y:event.clientY
        })
    }


    useEffect(()=>{
        console.log('ComponentDidMount')

        window.addEventListener('mousemove', mouseMoveHandler)
        return()=>{
            window.removeEventListener('mousemove', mouseMoveHandler    )

        }

    },[])

    return (
        <div>
            <h1>Pecypc: {type}</h1>

            <button onClick={()=>setType('users')}>Ma'ruf </button>
            <button onClick={()=>setType('todos')}>famelya </button>
            <button onClick={()=>setType('posts')}>post </button>

            <pre>{JSON.stringify(pos,null,2)}</pre>
        </div>
    );

}

export default App
