import React, {useEffect, useState} from 'react'
import {Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from 'reactstrap';
import axios from "axios";
import {TOKEN} from "../../utills/constants";
import {useHistory} from "react-router-dom"
import {api} from "../../api";


function CustomerMain() {
    let history = useHistory();

    const [isOpen, setIsOpen] = useState(false);
    const [productSetArray, setProductSetArray] = useState(false);
    const [me, setMe] = useState('')

    const toggle = () => setIsOpen(!isOpen);

    useEffect(() => {
        axios.get(api.userMe,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN)
                    }
            }).then(res => {
            setMe(res.data)
        })
    }, [])

    useEffect(() => {
        axios.get(api.productSet,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN)
                    }
            }).then(res => {
            setProductSetArray(res.data.object)
        })
    }, [])

    function handleClickProductSet(item) {
        history.push("/customer/customerOrder", item)
    }

    function logOut() {
        localStorage.removeItem(TOKEN)
        // history.push("/")
    }

    return (
        <div>
            <Navbar color="light" light expand="md">
                <NavbarBrand href="/">
                    Logotip
                </NavbarBrand>
                <NavbarToggler onClick={toggle}/>
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto" navbar>
                        <NavItem>
                            <NavLink href="/">Asosiy</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#">Biz haqimizda</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/customer/customerBasket">Mening zakazlarim</NavLink>
                        </NavItem>
                        {me ?
                            <NavItem>
                                <NavLink href="/" onClick={() => logOut()}>Chiqish</NavLink>
                                <p>Assalomu alaykum {me.firstName}</p>
                            </NavItem>
                            :
                            <NavItem>
                                <NavLink href="/pages/signUpIn">Kirish</NavLink>
                            </NavItem>
                        }
                    </Nav>
                </Collapse>
            </Navbar>
            <div className="container">
                <section id="blog" className="blog">
                    <div className="section-title">
                        Sotiladigan tovarlar
                    </div>
                    <div className="blog-items">
                        <div className="row">
                            {productSetArray && productSetArray.map((item, index) =>
                                <div className="col-md-4" key={index}>
                                    <div className="blog-item" onClick={() => handleClickProductSet(item)}>
                                        <div className="blog-item__image">
                                            <img src={api.attachment + item.attachmentIds[0]} alt="Blog 1"/>
                                        </div>
                                        <div className="blog-item__info">
                                            <h5 className="blog-item__title">Narxi: {item.totalPrice} so'm</h5>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                </section>
            </div>
        </div>


    )
}

export default CustomerMain