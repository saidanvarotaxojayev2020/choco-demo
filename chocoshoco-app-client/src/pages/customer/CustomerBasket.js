import React, {Component, useEffect, useState} from 'react';
import axios from "axios";
import {BASE_URL} from "../../utills/config";
import {TOKEN} from "../../utills/constants";
import {CardImg, Button, CardBody, ModalHeader, ModalFooter, Modal} from "reactstrap";
import {Card} from "antd";

import {AvForm} from "availity-reactstrap-validation";
import {api} from "../../api";

function CustomerBasket() {
    const [basket, setBasket] = useState([])
    const [orderArrayNew, setOrderArrayNew] = useState([]);
    const [orderArrayInProgress, setOrderArrayInProgress] = useState([]);
    const [orderArrayCompleted, setOrderArrayCompleted] = useState([]);
    const [currentOrder, setCurrentOrder] = useState('');
    const [currentOrderStatus, setCurrentOrderStatus] = useState('')
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [adminArray, setAdminArray] = useState([]);


    useEffect(() => {
        axios.get(api.userMe,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN)
                    }
            }).then(res => {
            setBasket(res.data.id)

            axios.get(api.order + '?search=client&page=0&size=10&orderStatus=NEW&userId=' + res.data.id,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                setOrderArrayNew(res.data.object)

            })
            axios.get(api.order + '?search=client&page=0&size=10&orderStatus=IN_PROGRESS&userId=' + res.data.id,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                setOrderArrayInProgress(res.data.object)
            })
            axios.get(api.order + '?search=client&page=0&size=10&orderStatus=COMPLETED&userId=' + res.data.id,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                setOrderArrayCompleted(res.data.object)

            })


            axios.get(api.getAllDriverOrUserByPageable + '?roleName=ROLE_ADMIN')
                .then(res => {
                    setAdminArray(res.data.object)
                })

        })


    }, [])

    function openModal(v) {
        setShowModal(!showModal)

    }


    function deleteOrder() {
        axios.delete(api.order + '/' + currentOrder.id,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            switch (currentOrderStatus) {
                case "NEW":
                    axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=NEW',
                        {
                            headers:
                                {
                                    "Authorization": localStorage.getItem(TOKEN),
                                }
                        }).then(res => {
                        setOrderArrayNew(res.data.object)
                    })
                    break
                case "IN_PROGRESS":
                    axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=IN_PROGRESS',
                        {
                            headers:
                                {
                                    "Authorization": localStorage.getItem(TOKEN),
                                }
                        }).then(res => {
                        setOrderArrayInProgress(res.data.object)
                    })
                    break
                case "COMPLETED":
                    axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=COMPLETED',
                        {
                            headers:
                                {
                                    "Authorization": localStorage.getItem(TOKEN),
                                }
                        }).then(res => {
                        setOrderArrayCompleted(res.data.object)
                    })
                    break
            }
            setShowModalDelete(!showModalDelete)
        })
    }

    const openModalDelete = (item) => {
        setCurrentOrder(item)
        setCurrentOrderStatus(item.orderStatus)
        setShowModalDelete(!showModalDelete)
    }


    return (
        <div className="row">
            <div className="col-xl-3 ml-4">
                <div className="row">
                    <div className="col-12">
                        <div className="card bg-primary text-white mb-4 mt-2">
                            <div className="card-body ">Yangi zakaz</div>
                        </div>
                    </div>
                </div>

                {orderArrayNew && orderArrayNew.map((item, index) =>
                    <div className="row" key={index}>
                        <div className="col-12">
                            <div className="card border-info  mb-4">
                                <Card>
                                    {item.productWithAmountDtos && item.productWithAmountDtos.map((item, index) =>
                                        <CardBody key={index}>
                                            <CardImg top width="100%"
                                                     src={api.attachment + item.productDto.attachmentIds[0]}
                                                     alt=""/>

                                            <p className="m-2">Product name: {item.productDto.name}</p>
                                            <p className="m-2">Product: {item.content ? item.content : item.amount}</p>

                                        </CardBody>
                                    )}
                                    <p className="m-2 mt-1 ml-4 ">Total price: <b>{item.totalPrice}</b></p>
                                    <p className="m-2 ml-4">Karobka nomi: <b>{item.boxDto.name}</b></p>

                                    <Button className="btn btn-danger w-100"
                                            onClick={() => openModalDelete(item)}>Delete</Button>

                                </Card>
                            </div>
                        </div>
                    </div>
                )}


            </div>
            <div className="col-xl-3 col-md-6">
                <div className="row">
                    <div className="col-12">
                        <div className="card bg-danger text-white mb-4 mt-2 mr-2">
                            <div className="card-body">Tayyorlanadigan zakaz</div>
                        </div>
                    </div>
                </div>
                {orderArrayInProgress && orderArrayInProgress.map((item, index) =>
                    <div className="row" key={index}>
                        <div className="col-12">
                            <div className="card border-info  mb-4">


                                <Card>

                                    {item.productWithAmountDtos && item.productWithAmountDtos.map((item, index) =>

                                        <p key={index}>

                                            <img src={api.attachment + item.productDto.attachmentIds[0]}
                                                 width="95%" alt=""/>

                                            <p className="m-2">Product: {item.content ? item.content : item.amount}</p>
                                            <p className="m-2">Product name: {item.productDto.name}</p>

                                        </p>
                                    )}

                                    <p className="m-2 mt-1 ">Total price: <b>{item.totalPrice}</b></p>
                                    <p className="m-2">Karobka nomi: <b>{item.boxDto.name}</b></p>

                                    <Button className="btn btn-danger w-100"
                                            onClick={() => openModal(item)}>Delete</Button>


                                </Card>
                            </div>
                        </div>
                    </div>
                )}
            </div>
            <div className="col-xl-3 col-md-6">
                <div className="row">
                    <div className="col-12">
                        <div className="card bg-dark text-white mb-4 mt-2">
                            <div className="card-body">Tayyor zakaz</div>
                        </div>
                    </div>
                </div>
                {orderArrayCompleted && orderArrayCompleted.map((item, index) =>
                    <div className="row" key={index}>
                        <div className="col-12">
                            <div className="card border-info  mb-4">
                                <Card>
                                    {item.productWithAmountDtos && item.productWithAmountDtos.map((item, index) =>
                                        <p key={index}>
                                            <img src={api.attachment + item.productDto.attachmentIds[0]}
                                                 width="100%" alt=""/>

                                            <p className="m-2">Product: {item.content ? item.content : item.amount}</p>
                                            <p className="m-2">Product name: {item.productDto.name}</p>

                                        </p>
                                    )}

                                    <p className="m-2 mt-1">Total price: <b>{item.totalPrice}</b></p>
                                    <p className="m-2">Karobka nomi: <b>{item.boxDto.name}</b></p>
                                </Card>
                            </div>
                        </div>
                    </div>
                )}
                <Modal style={{maxWidth: '600px', width: '400px'}} isOpen={showModalDelete}
                       toggle={() => openModalDelete('')}>
                    <ModalHeader>
                        Rostdan ham o'chirmoqchimisiz?
                    </ModalHeader>
                    <AvForm onValidSubmit={deleteOrder}>
                        <ModalFooter>
                            <Button onClick={() => openModalDelete('')}>Cancel</Button>
                            <Button color="danger" type="submit">Delete</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>


                <Modal style={{maxWidth: '600px', width: '400px'}} isOpen={showModal}
                       toggle={() => openModal('')}>
                    <ModalHeader>
                        Admin orqali O'chirasiz?
                    </ModalHeader>

                    <Card>
                        <b> Admin nomeri :</b>
                        {adminArray ? adminArray.map((itemm, index) =>
                            <Card>
                                <h5>{itemm.phoneNumber}</h5>
                            </Card>
                        ) : ''}

                        <ModalFooter>
                            <Button onClick={() => openModal('')}>Cancel</Button>
                        </ModalFooter>

                    </Card>
                </Modal>
            </div>
        </div>


    );
}

export default CustomerBasket;