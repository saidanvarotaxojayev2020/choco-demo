import React, {useEffect, useState} from 'react'
import {
    Button, Card, CardBody,
    Collapse,
    Nav,
    Navbar,
    NavbarBrand,
    NavbarText,
    NavbarToggler,
    NavItem,
    NavLink
} from "reactstrap";
import {BASE_URL} from "../../utills/config";
import axios from "axios";
import {TOKEN} from "../../utills/constants";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {useHistory} from "react-router-dom"
import {api} from "../../api";

function CustomerOrder(props) {
    let history = useHistory();
    const prodSet = props.location.state;
    const [me, setMe] = useState('')
    const [isOpen, setIsOpen] = useState(false);
    const [currentProductSet, setCurrentProductSet] = useState(prodSet)
    const [boxCategory, setBoxCategory] = useState([]);
    const [boxArray, setBoxArray] = useState([]);
    const [oneBox, setOneBox] = useState([]);
    const [indexBox, setIndexBox] = useState(0);
    const [allProduct, setAllProduct] = useState([])
    const [productWithAmountArray, setProductWithAmountArray] = useState([]);
    const [allCategoryArray, setAllCategoryArray] = useState([])
    const [indexProduct, setIndexProduct] = useState(0)
    const [districtId, setDistrictId] = useState([]);
    const [deliveryPriceArray, setDeliveryPriceArray] = useState([]);
    const [addressPrice, setAddressPrice] = useState(0);


    const toggle = () => setIsOpen(!isOpen);

    useEffect(() => {
        setOneBox([prodSet.boxDto])
        setProductWithAmountArray(prodSet.productWithAmountDtos)
    }, [])

    useEffect(() => {
        axios.get(api.userMe,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN)
                    }
            }).then(res => {
            setMe(res.data)
        })
    }, [])

    useEffect(() => {
        axios.get(api.boxCategory, {
            headers: {
                "Authorization": localStorage.getItem(TOKEN)
            }
        }).then(res => {
            setBoxCategory(res.data._embedded.list)
        })
    }, [])

    useEffect(() => {
        axios.get(api.box,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const boxArr = res.data.object
            if (boxArr.length) {
                setBoxArray(boxArr)
                setOneBox([prodSet.boxDto])
            }
        })
    }, [])

    useEffect(() => {
        axios.get(api.product, {
            headers: {
                "Authorization": localStorage.getItem(TOKEN)
            }
        }).then(res => {
            setAllProduct(res.data.object)
        })
    }, [])

    useEffect(() => {
        axios.get(api.deliveryPrice,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setDeliveryPriceArray(res.data._embedded.list)
        })
    }, [])

    function setBoxCategoryy(e) {
        axios.get(api.getBoxByCategoryId + e.target.value, {
            headers:
                {
                    "Authorization": localStorage.getItem(TOKEN),
                }
        }).then(res => {
            setBoxArray(res.data.object)
            setOneBox([res.data.object[0]])
        })
    }

    useEffect(() => {
        axios.get(api.categoryAll, {
            headers:
                {
                    "Authorization": localStorage.getItem(TOKEN),
                }
        }).then(res => {
            setAllCategoryArray(res.data.object)
        })
    }, [])

    function rightBox() {
        let index = indexBox + 1
        setIndexBox(index > boxArray.length - 1 ? index = 0 : index)
        setOneBox(index > boxArray.length - 1 ? [boxArray[0]] : [boxArray[index]])
    }

    function leftBox() {
        let index = indexBox - 1
        setIndexBox(index < 0 ? boxArray.length - 1 : index)
        setOneBox(index < 0 ? [boxArray[boxArray.length - 1]] : [boxArray[index]])
    }

    function setProductCategoryy(e, index) {
        axios.get(api.productByCategory + e.target.value, {
            headers:
                {
                    "Authorization": localStorage.getItem(TOKEN),
                }
        }).then(res => {
            setAllProduct(res.data.object)
            let newProductWithAmountArray = productWithAmountArray.map((item, i) => {
                if (i === index) {
                    return {...item, productDto: res.data.object[0]}
                } else {
                    return item
                }
                // return i === index ? {...item, productDto: allProducts[indexProd] : item}
            })
            setProductWithAmountArray(newProductWithAmountArray)
        })
    }

    function setInputProduct(e, index) {
        let value = e.target.value
        let newProductWithAmountArray = productWithAmountArray.map((item, i) => {
            if (i === index) {
                if (item.productDto.hasContent === true) {
                    return {...item, content: value, amount: value.length}
                } else {
                    return {...item, amount: value}
                }
            } else {
                return item
            }
        })
        setProductWithAmountArray(newProductWithAmountArray)
    }

    function leftProduct(index) {
        let indexProd = indexProduct - 1 < 0 ? allProduct.length - 1 : indexProduct - 1
        let allProducts = allProduct;  // state dan o'zgaruvchiga oldim
        let newProductWithAmountArray = productWithAmountArray.map((item, i) => {
            if (i === index) {
                return {...item, productDto: allProducts[indexProd]}
            } else {
                return item
            }
            // return i === index ? {...item, productDto: allProducts[indexProd] : item}
        })
        setProductWithAmountArray(newProductWithAmountArray)
        setIndexProduct(indexProd)
    }

    function rightProduct(index) {
        let indexProd = indexProduct + 1 > allProduct.length - 1 ? 0 : indexProduct + 1
        let allProducts = allProduct;  // state dan o'zgaruvchiga oldim
        let newProductWithAmountArray = productWithAmountArray.map((item, i) => {
            if (i === index) {
                return {...item, productDto: allProducts[indexProd]}
            } else {
                return item
            }
            // return i === index ? {...item, productDto: allProducts[indexProd] : item}
        })
        setProductWithAmountArray(newProductWithAmountArray)
        setIndexProduct(indexProd)
    }

    function deleteProductRow(index) {
        let newProdAmount = productWithAmountArray.filter(function (item, i) {
            if (i !== index) {
                return item
            }
        })
        setProductWithAmountArray(newProdAmount)
    }

    function plusProductRow() {
        if (allProduct.length) {
            setProductWithAmountArray([...productWithAmountArray, {
                id: '',
                productDto: {
                    id: allProduct[0].id,
                    name: allProduct[0].name,
                    price: allProduct[0].price,
                    attachmentIds: allProduct[0].attachmentIds,
                    categoryDto: {
                        id: allProduct[0].categoryDto.id,
                        name: allProduct[0].categoryDto.name
                    },
                    colorDto: {
                        id: allProduct[0].colorDto.id,
                        name: allProduct[0].colorDto.name,
                        code: allProduct[0].colorDto.code
                    },
                    measurementDto: {
                        id: '',
                        name: ''
                    },
                    measureValue: '',
                    hasContent: allProduct[0].hasContent
                },
                amount: '',
                content: '',
                orderDto: {},
                productSetDto: {
                    id: ''
                }
            }])
        } else {
            setProductWithAmountArray([...productWithAmountArray, {
                id: '',
                productDto: {
                    id: '',
                    name: '',
                    price: '',
                    attachmentIds: '',
                    categoryDto: {
                        id: '',
                        name: ''
                    },
                    colorDto: {
                        id: '',
                        name: '',
                        code: ''
                    },
                    measurementDto: {
                        id: '',
                        name: ''
                    },
                    measureValue: '',
                    hasContent: ''
                },
                amount: '',
                content: '',
                orderDto: {},
                productSetDto: {
                    id: ''
                }
            }])
        }
    }

    function setDistrict(e) {
        let disId = e.target.value
        setDistrictId(disId)
        let price = 0
        deliveryPriceArray.map(item => {
            if (item.districtId == disId)
                price = item.price
            // item.districtId === disId ? setAddressPrice(item.price) : setAddressPrice(0)
        })
        setAddressPrice(price)
    }

    function saveOrEditOrder(e, v) {
        if (me) {
            const order = {
                id: null,
                userDto: {
                    id: me.id
                },
                date: v.date,
                orderStatus: 'NEW',
                addressDto: isOpen ? {
                    street: v.street,
                    house: v.homeNumber,
                    districtDto: {
                        id: v.district
                    }
                } : null,
                clientNumber: v.clientNumber,
                boxDto: {
                    id: oneBox[0].id
                },
                productWithAmountDtos: productWithAmountArray
            }
            axios.post(api.order, order, {
                headers: {
                    "Authorization": localStorage.getItem(TOKEN)
                }
            }).then(res => {
                if (res.status === 201 || res.status === 200)
                    history.push("/")
            })
        } else {
            history.push("/pages/signUpIn")
        }
    }

    return (
        <div>
            <Navbar color="light" light expand="md">
                <NavbarBrand href="/">
                    Logotip
                </NavbarBrand>
                <NavbarToggler onClick={toggle}/>
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto" navbar>
                        <NavItem>
                            <NavLink href="/">Asosiy</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#">Biz haqimizda</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#">Chocoladlar</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>


            <div className="container">
                <AvForm onValidSubmit={saveOrEditOrder}>
                    {/*<div className="row">*/}
                    {/*    <div className="col-md-12">*/}
                    {/*        <Select*/}
                    {/*            showSearch*/}
                    {/*            style={{width: 200}}*/}
                    {/*            // placeholder={currentOrder ? currentOrder.userDto.firstName : "Select Client"}*/}
                    {/*            optionFilterProp="children"*/}
                    {/*            onChange={onChangeClient}*/}
                    {/*            required={true}*/}
                    {/*            // onFocus={true}*/}
                    {/*            // onBlur={true}*/}
                    {/*            onSearch={onSearchClient}*/}
                    {/*            allowClear={true}*/}
                    {/*            filterOption={false}*/}
                    {/*        >*/}
                    {/*            {clientArray && clientArray.map(item => {*/}
                    {/*                    return <Option key={item.id} value={item.id}>{item.firstName}</Option>*/}
                    {/*                }*/}
                    {/*            )}*/}
                    {/*        </Select>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    <div className="row">
                        <div className="col-md-2 mt-4">
                            <AvField
                                onChange={(e) => setBoxCategoryy(e)}
                                lable="NameUz"
                                name="category"
                                type="select"
                                placeholder="Category"
                                value={prodSet.boxDto.boxCategoryDto.id}
                            >
                                <option value="" disabled>Box Category</option>
                                {boxCategory && boxCategory.map((item, index) =>
                                    <option value={item.id} name="category"
                                            key={index}>{item.name}</option>
                                )}
                            </AvField>
                            {oneBox && oneBox.map((item, index) =>
                                <img key={index} src={api.attachment + item.attachmentIds[0]}
                                     alt=""
                                     width={'100%'}/>
                            )}
                            <button type="button" className="btn btn-success" onClick={leftBox}>Left
                            </button>
                            <button type="button" className="btn btn-success" onClick={rightBox}>Right
                            </button>
                        </div>

                        {productWithAmountArray && productWithAmountArray.map((item, index) =>
                            <div className="col-md-2 mt-4" key={index}>
                                <div>
                                    <AvField
                                        onChange={(e) => setProductCategoryy(e, index)}
                                        lable="NameUz"
                                        name="category"
                                        type="select"
                                        placeholder="Category"
                                        value={item.productDto.categoryDto ? item.productDto.categoryDto.id : ''}
                                    >
                                        <option value="" disabled>Product Category</option>
                                        {allCategoryArray && allCategoryArray.map((item, index) =>
                                            <option value={item.id} name="category"
                                                    key={index}>{item.name}</option>
                                        )}
                                    </AvField>
                                    <div>
                                        <img
                                            src={api.attachment + item.productDto.attachmentIds[0]}
                                            alt=""
                                            width={'100%'}/>

                                        <AvField onChange={(e) => setInputProduct(e, index)}
                                                 lable="Name"
                                                 name="content"
                                                 type={item.productDto.hasContent ? "text" : "number"}
                                                 required
                                                 value={item.productDto.hasContent ? item.content : item.amount}
                                                 placeholder={item.productDto.hasContent ? 'Enter text' : 'Enter number'}/>
                                        <button type="button" className="btn btn-success"
                                                onClick={() => leftProduct(index)}>Left
                                        </button>
                                        <button type="button" className="btn btn-success"
                                                onClick={() => rightProduct(index)}>Right
                                        </button>
                                        <button className="btn btn-danger"
                                                onClick={() => deleteProductRow(index)}>Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        )}
                        <Button className="btn btn-dark col-md-2"
                                onClick={() => plusProductRow()}>+</Button>
                    </div>
                    <div className="row mt-4">
                        <div className="col-md-4">
                            <AvField
                                lable="Date"
                                name="date"
                                type="date"
                                required
                                // defaultValue={currentOrder ? currentOrder.date : null}
                                placeholder="Tayyorlanish sanasi"
                            />
                        </div>
                        <div className="col-md-4">
                            <AvField
                                lable="ClientNumber"
                                name="clientNumber"
                                type="text"
                                required
                                value={me ? me.phoneNumber : ''}
                                placeholder="Nomer: +998 991234567"
                            />
                        </div>

                        <div className="col-md-4">
                            <Button color="primary" onClick={() => toggle()}
                                    style={{marginBottom: '1rem'}}>Yetkazib berish xizmatidan
                                foydalanasizmi?</Button>
                            <Collapse isOpen={isOpen}>
                                <Card>
                                    <CardBody>
                                        <AvField
                                            onChange={(e) => setDistrict(e)}
                                            lable="NameUz"
                                            name="district"
                                            type="select"
                                            placeholder="District"
                                            // value={currentOrder.addressDto ? currentOrder.addressDto.districtDto.id : ''}
                                        >
                                            <option value="" disabled>Tumanni tanlang</option>
                                            {deliveryPriceArray && deliveryPriceArray.map((item, index) =>
                                                <option value={item.districtId} name="district"
                                                        key={index}>{item.districtName}</option>
                                            )}
                                        </AvField>

                                        <AvField
                                            lable="Street"
                                            name="street"
                                            type="text"
                                            required={isOpen}
                                            value=""
                                            // defaultValue={currentOrder.addressDto ? currentOrder.addressDto.street : ''}
                                            placeholder="Ko'chani kiriting"
                                        />

                                        <AvField
                                            lable="HomeNumber"
                                            name="homeNumber"
                                            type="text"
                                            required={isOpen}
                                            value=""
                                            // defaultValue={currentOrder.addressDto ? currentOrder.addressDto.house : ''}
                                            placeholder="Uy raqamini kiriting"
                                        />
                                    </CardBody>
                                </Card>
                            </Collapse>
                        </div>
                    </div>
                    <p>Total sum: <b> {isOpen
                        ?
                        (productWithAmountArray.reduce(function (prev, cur) {
                            return prev + (cur.amount * cur.productDto.price)
                        }, 0) + addressPrice).toFixed().toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")
                        :
                        (productWithAmountArray.reduce(function (prev, cur) {
                            return prev + (cur.amount * cur.productDto.price)
                        }, 0)).toFixed().toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")
                    }
                    </b>
                    </p>
                    <Button color="success" type="submit">Save</Button>
                </AvForm>
            </div>
        </div>
    );
}

export default CustomerOrder;