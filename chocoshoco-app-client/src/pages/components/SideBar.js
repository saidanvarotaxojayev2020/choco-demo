import React, {useEffect} from 'react';
import {Link, useHistory} from "react-router-dom";
import {TOKEN} from "../../utills/constants";
import axios from "axios";
import {BASE_URL} from "../../utills/config";
import {api} from "../../api";

function SideBar() {
    let history = useHistory();

    useEffect(() => {
        if (localStorage.getItem(TOKEN) != null) {
            axios.get(api.userMe,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN)
                        }
                }).then(res => {
                console.log(res.data, "meeeeeeeeeeeeeeeeeeee")
                if (res.data.roles[0].roleName !== "ROLE_ADMIN") {
                    history.push('/404')
                }
            }).catch(err => {
                history.push("/pages/signUpIn")
            })
        } else {
            history.push('/')
        }
    }, [])

    function setPage(path) {
        console.log(path)
        history.push(path)
    }

    function logOut() {
        localStorage.removeItem(TOKEN)
        history.push("/")
    }

    return (
        <div>
            <div id="layoutSidenav">
                <div id="layoutSidenav_nav">
                    <nav className="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                        <div className="sb-sidenav-menu">
                            <div className="nav">
                                <div className="sb-sidenav-menu-heading">
                                    Category
                                </div>
                                <Link className="nav-link" to="/admin/order">
                                    <div className="sb-nav-link-icon">
                                    </div>
                                    Orders
                                </Link>
                                <Link className="nav-link" to="/admin/adminClient">
                                    <div className="sb-nav-link-icon">
                                    </div>
                                    Clients
                                </Link>
                                <Link className="nav-link" to="/admin/product">
                                    <div className="sb-nav-link-icon">
                                    </div>
                                    Product
                                </Link>
                                <Link className="nav-link" to="/admin/boxCategory">
                                    <div className="sb-nav-link-icon">
                                    </div>
                                    Box category
                                </Link>
                                <Link className="nav-link" to="/admin/category">
                                    <div className="sb-nav-link-icon">
                                    </div>
                                    Category
                                </Link>
                                <Link className="nav-link" to="/admin/color">
                                    <div className="sb-nav-link-icon">
                                    </div>
                                    Color
                                </Link>
                                <Link className="nav-link" to="/admin/deliveryPrice">
                                    <div className="sb-nav-link-icon">
                                    </div>
                                    DeliveryPrice
                                </Link>
                                <Link className="nav-link" to="/admin/productSet">
                                    <div className="sb-nav-link-icon">
                                    </div>
                                    ProductSet
                                </Link>
                                <Link className="nav-link" to="/admin/district">
                                    <div className="sb-nav-link-icon">
                                    </div>
                                    District
                                </Link>
                                <Link className="nav-link" to="/admin/measurement">
                                    <div className="sb-nav-link-icon">
                                    </div>
                                    Measurement
                                </Link>
                                <Link className="nav-link" to="/admin/region">
                                    <div className="sb-nav-link-icon">
                                    </div>
                                    Region
                                </Link>
                                <Link className="nav-link" to="/admin/box">
                                    <div className="sb-nav-link-icon">
                                    </div>
                                    Box
                                </Link>
                            </div>
                        </div>
                        <button className="btn btn-danger" onClick={logOut}>Log Out</button>
                        <div className="sb-sidenav-footer">
                            <div className="small">Logged in as:</div>
                            Start Bootstrap
                        </div>
                    </nav>
                </div>
            </div>

        </div>
    )
}

export default SideBar;