import React, {useEffect, useState} from 'react'
import {AvField, AvForm} from "availity-reactstrap-validation";
import axios from 'axios';
import {TOKEN} from "../utills/constants";
import {BASE_URL} from "../utills/config";
import jwt from "jwt-decode";
import {useHistory} from "react-router-dom"
import {firebaseAuth} from "../utills/firebase";
import firebase from "firebase/app";
import {api} from "../api";

function SingUpIn() {
    let history = useHistory();

    const [reCaptcha, setReCaptcha] = useState('')
    const [active, setActive] = useState(true)
    const [bla, setBla] = useState('')
    const [section, setSection] = useState('')
    const [activ, setactiv] = useState(false)
    const [user, setUser] = useState({})
    const [smsSent, setSmsSent] = useState(false)
    const [confirmationResult, setConfirmationResult] = useState('')


    useEffect(() => {
        let cont = document.getElementById('reCaptcha');
        let reCaptchaVerifier;
        reCaptchaVerifier = new firebase.auth.RecaptchaVerifier(cont, {
            'size': 'invisible',
            'callback': (res) => {
            },
            'expired-callback': () => {
            },
            'error-callback': () => {
                alert("Xatolik!!! Iltimos birozdan keyin urinib ko'ring")
            }
        });
        reCaptchaVerifier.render();
        setReCaptcha(reCaptchaVerifier);
    }, [])

    function login(e, v) {
        axios.post(api.login, {
            phoneNumber: v.phoneNumber,
            password: v.password
        })
            .then(res => {
                localStorage.setItem(TOKEN, 'Bearer ' + ' ' + res.data);
                let parsedToken = jwt(res.data);
                // setTimeout(() => {
                if (parsedToken.roles[0].roleName === 'ROLE_ADMIN') {
                    history.push('/admin/order')

                } else if (parsedToken.roles[0].roleName === 'ROLE_CLIENT') {
                    history.push('/')
                } else {
                    history.push('/')
                }
                // }, 0);
            }).catch(err => {
            alert('Nomer yoki parol xato terildi!!!')
        })
    }

    function toggleForm() {
        setActive(!active)
        setBla(active ? 'active' : '')
        setactiv(!activ)
        setSection(activ ? 'active' : '')
    }

    function checkRegister(e, v) {
        // alert("sendSms FUNCTION")
        setUser(v)
        axios.post(BASE_URL + 'auth/sendSms', v)
            .then(res => {
                //firebase ga Nomer jo'nataman

                sendFirebase(v.phoneNumber)
            }).catch(err => {
            alert(err.response.data.message)
        })
    }

    function sendFirebase(phoneNumber) {
        // alert("sendFirebase FUNCTION")

        firebaseAuth.signInWithPhoneNumber(phoneNumber, reCaptcha)
            .then(res => {
                setSmsSent(true)
                setConfirmationResult(res)
            }).catch(err => {
                alert(err.message)
        })
    }

    function sendSmsToFirebase(e, v) {
        // alert("sendSmsToFirebase FUNCTION")

        confirmationResult.confirm(v.code)
            .then(res => {
                registerUser(user)
            }).catch(err => {
            alert('Kod xato')
        })
    }

    function registerUser(userData) {
        // alert("registerUser FUNCTION")
        // firebasega userning phoneNumber bilan code ni jo'natamiz
        axios.post(api.register, userData)
            .then(res => {
                localStorage.setItem(TOKEN, 'Bearer ' + res.data.object)
                history.push('/')
            }).catch(err => {
            alert(err.response.data.message)
        })
    }

    return (
        <section className={section}>
            <div className={"containers " + bla}>
                <div className="user signinBx">
                    <div className="imgBx"><img src="/6.jpg" alt=""/></div>
                    <div className="formBx">
                        <AvForm onValidSubmit={login}>
                            {/*<form action="">*/}
                            <h2>Sing In</h2>


                            <div className="form-group">
                                <AvField
                                    className="form-control py-4"
                                    type="text"
                                    name="phoneNumber"
                                    label="Enter phone number"
                                    required
                                    placeholder="Enter phone number"
                                />
                            </div>

                            <div className="form-group">
                                <AvField
                                    className="form-control py-4"
                                    type="password"
                                    name="password"
                                    label="Enter password"
                                    required
                                    placeholder="Enter password"
                                />
                            </div>
                            <div
                                className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                <input type="submit" value="Login"/>
                            </div>

                            <p className="signup">accountingiz yo'qmi? <a href="#" onClick={toggleForm}>ro'yxatdan
                                o'tish.</a>
                            </p>
                            {/*</form>*/}
                        </AvForm>
                    </div>
                </div>


                <div className="user signupBx">
                    <div className="formBx">
                        {!smsSent ?
                            <AvForm onValidSubmit={checkRegister}>
                                <h2>Create an account</h2>
                                <div className="form-group">
                                    <AvField
                                        className="form-control py-4"
                                        required
                                        lable="firstName"
                                        name="firstName"
                                        type="text"
                                        placeholder="First name"
                                    />
                                </div>
                                <AvField
                                    className="form-control py-4"
                                    required
                                    lable="PhoneNumber"
                                    name="phoneNumber"
                                    type="text"
                                    placeholder="PhoneNumber"
                                />
                                <AvField
                                    className="form-control py-4"
                                    required
                                    lable="Password"
                                    name="password"
                                    type="password"
                                    placeholder="Password"
                                />
                                <AvField
                                    className="form-control py-4"
                                    required
                                    lable="PrePassword"
                                    name="prePassword"
                                    type="password"
                                    placeholder="PrePassword"
                                />
                                <input type="submit" value="Sign Up"/>
                                <p className="signup">accountingiz bormi? <a href="#" onClick={toggleForm}>kirish.</a>
                                </p>
                                {/*<form onValidSubmit={register}>*/}
                                {/*    <h2>Create an account</h2>*/}
                                {/*    <input type="text" placeholder="Name"/>*/}
                                {/*    <input type="text" placeholder="Phone Number"/>*/}
                                {/*    <input type="password" placeholder="Creat Password"/>*/}
                                {/*    <input type="password" placeholder="Config Password"/>*/}
                                {/*    <input type="submit" value="Sign Up"/>*/}
                                {/*    <p className="signup">Already have an account? <a href="#" onClick={toggleForm}>Sing in.</a>*/}
                                {/*    </p>*/}
                                {/*</form>*/}
                            </AvForm>
                            :
                            <AvForm onValidSubmit={sendSmsToFirebase}>
                                <h2>Raqamga kod yuborildi</h2>
                                <div className="form-group">
                                    <AvField
                                        className="form-control py-4"
                                        required
                                        lable="code"
                                        name="code"
                                        type="text"
                                        placeholder="Kodni kiriting"
                                    />
                                    {/*<input type="submit" value="Yuborish"/>*/}
                                    <button type="button" className="btn btn-danger" onClick={() => setSmsSent(!smsSent)}>Orqaga</button>
                                    <button type="submit" className="btn btn-success">Yuborish</button>
                                </div>
                            </AvForm>
                        }
                        <div id="reCaptcha" render="explicit" style={{display: "none"}}/>

                    </div>
                    <div className="imgBx"><img src="/7.jpg" alt=""/></div>
                </div>
            </div>
        </section>


    )
}

export default SingUpIn