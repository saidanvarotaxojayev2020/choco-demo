import React, {useEffect, useState} from "react";
import SideBar from "../components/SideBar";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {DeleteOutlined, EditOutlined, PlusOutlined} from "@ant-design/icons";
import {AvField, AvForm} from "availity-reactstrap-validation";
import axios from "axios";
import {TOKEN} from "../../utills/constants";
import {useHistory} from "react-router-dom";
import {api} from "../../api";

function District() {
    let history = useHistory();
    const [showModal, setShowModal] = useState(false);
    const [districtArray, setDistrictArray] = useState([]);
    const [currentDistrict, setCurrentDistrict] = useState('');
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [regionArray, setRegionArray] = useState([]);

    function openModalDelete(v) {
        setCurrentDistrict(v)
        setShowModalDelete(!showModalDelete)
    }

    useEffect(() => {
        axios.get(api.region,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const regionArray1 = res.data._embedded.list
            regionArray1.reverse()
            setRegionArray(regionArray1)
        })
    }, [])

    useEffect(() => {
        axios.get(api.district,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const districtArray1 = res.data._embedded.list
            districtArray1.reverse()
            setDistrictArray(districtArray1)
        })
    }, [])

    function deleteDistrict() {
        axios.delete(api.district + currentDistrict.id).then(res => {
            axios.get(api.district,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                const districtArray1 = res.data._embedded.list
                districtArray1.reverse()
                setDistrictArray(districtArray1)
            })
        })
        setShowModalDelete(!showModalDelete)
    }

    function openModal(v) {
        setCurrentDistrict(v)
        setShowModal(!showModal)
    }

    function saveOrEditDistrict(e, v) {
        if (currentDistrict) {
            axios.patch(api.district + currentDistrict.id, v,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                axios.get(api.district,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const districtArray1 = res.data._embedded.list
                    districtArray1.reverse()
                    setDistrictArray(districtArray1)
                })
            })
        } else {
            axios.post(api.district, v,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                axios.get(api.district,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const districtArray1 = res.data._embedded.list
                    districtArray1.reverse()
                    setDistrictArray(districtArray1)
                })
            })
        }
        setShowModal(!showModal)
    }


    return (
        <div>
            <div className="row">
                <div className="col-md-12">
                    <div>
                        <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
                        </nav>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">
                    <SideBar/>
                </div>
                <div className="col-md-10">
                    <div className="row mt-3">
                        <div className="col-md-2">
                            <Button className="btn btn-success" onClick={() => openModal('')}><PlusOutlined/></Button>
                        </div>
                        <div className="col-md-4 offset-6">
                            <h1>District</h1>
                        </div>
                    </div>
                    <Table>
                        <thead>
                        <tr>
                            <th>T/R</th>
                            <th>Name</th>
                            <th>Region name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {districtArray && districtArray.map((item, index) =>
                            <tr key={index}>
                                <th>{index + 1}</th>
                                <td>{item.name}</td>
                                <td>{item.region.name}</td>
                                <td>
                                    <Button className="btn btn-success" onClick={() => openModal(item)}><EditOutlined/></Button>
                                    <Button className="btn btn-danger ml-2"
                                            onClick={() => openModalDelete(item)}><DeleteOutlined/></Button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </Table>

                    <Modal isOpen={showModal} toggle={() => openModal('')}>
                        <ModalHeader toggle={() => openModal('')}>
                            {currentDistrict ? "Edit District" : "Add District"}
                        </ModalHeader>
                        <AvForm onValidSubmit={saveOrEditDistrict}>
                            <ModalBody>
                                <AvField lable="Name"
                                         name="name"
                                         type="text"
                                         required
                                         defaultValue={currentDistrict ? currentDistrict.name : ''}
                                         placeholder="Name"/>
                                <AvField lable="Region"
                                         name="region"
                                         type="select"
                                    // defaultValue={currentDistrict ? currentDistrict?.region.id : ''}
                                         placeholder="Region"
                                         value={currentDistrict ? "/" +  currentDistrict.region.id : ''}
                                >
                                    <option value="" disabled>Region tanlang</option>
                                    {regionArray && regionArray.map((item, index) =>
                                        <option key={index} value={"/" + item.id} name="region">{item.name}</option>
                                    )}

                                </AvField>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="danger" onClick={() => openModal('')}>Cancel</Button>
                                <Button color="success" type="submit">Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    <Modal isOpen={showModalDelete} toggle={() => openModalDelete('')}>
                        <ModalHeader>Rostdan ham o'chirmoqchimisiz?</ModalHeader>
                        <AvForm onValidSubmit={deleteDistrict}>
                            <ModalFooter>
                                <Button className="btn btn-danger"
                                        onClick={() => openModalDelete('')}>Cancel</Button>
                                <Button className="btn btn-success" type="submit">Delete</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>
                </div>
            </div>
        </div>
    )
}

export default District;