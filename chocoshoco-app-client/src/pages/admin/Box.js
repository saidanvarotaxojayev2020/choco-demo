import React, {useEffect, useState} from "react";
import SideBar from "../components/SideBar";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {DeleteOutlined, EditOutlined, PlusOutlined} from "@ant-design/icons";
import axios from "axios";
import {TOKEN} from "../../utills/constants";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {useHistory} from "react-router-dom";
import {api} from "../../api";


function Box() {
    let history = useHistory();
    const [showModal, setShowModal] = useState(false);
    const [currentBox, setCurrentBox] = useState('');
    const [colorArray, setColorArray] = useState([])
    const [boxCategoryArray, setBoxCategoryArray] = useState([])
    const [boxArray, setBoxArray] = useState([])
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [selectedFileId, setSelectedFileId] = useState([]);




    function deleteBox() {
        axios.delete(api.box + currentBox.id)
            .then(res => {
                axios.get(api.box,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    setBoxArray(res.data.object)
                })
            })
        setShowModalDelete(!showModalDelete)
    }

    function openModal(v) {
        setCurrentBox(v)

        setSelectedFileId(v ? v.attachmentIds : [])
        setShowModal(!showModal)
    }

    function openModalDelete(v) {
        setCurrentBox(v)
        setShowModalDelete(!showModalDelete)
    }

    useEffect(() => {
        axios.get(api.color,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const colorArray1 = res.data._embedded.list
            colorArray1.reverse()
            setColorArray(colorArray1)

        })

    }, [])

    useEffect(() => {
        axios.get(api.boxCategory,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const boxCategoryArray1 = res.data._embedded.list
            boxCategoryArray1.reverse()
            setBoxCategoryArray(boxCategoryArray1)
        })
    }, [])


    useEffect(() => {
        axios.get(api.box,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const boxArray1 = res.data.object
            boxArray1.reverse()
            setBoxArray(boxArray1)
        })
    }, [])


    function saveOrEditBox(e, v) {
        const fayl = {
            id: currentBox ? currentBox.id : null,
            name: v.name,
            colorDto: {
                id: v.color
            },
            boxCategoryDto: {
                id: v.boxCategory
            },
            attachmentIds: selectedFileId
        }
        if (currentBox) {
            axios.post(api.box, fayl,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                axios.get(api.box,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const boxArray1 = res.data.object
                    boxArray1.reverse()
                    setBoxArray(boxArray1)
                })
            })
        } else {
            axios.post(api.box, fayl,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                axios.get(api.box,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN)
                            }
                    }).then(res => {
                    const boxArray1 = res.data.object
                    boxArray1.reverse()
                    setBoxArray(boxArray1)

                })
            })
        }
        setShowModal(!showModal)
    }

    function setAttachment(e) {
        const formData = new FormData();

        formData.append('file', e.target.files[0]);
        formData.append("type", e.target.name);

        axios.post(api.attachment, formData,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                        "Content-Type": "multipart/form-data"
                    }
            }).then(res => {
            setSelectedFileId([res.data])
        })
    }

    return (
        <div>
            <div>
                <div className="row">
                    <div className="col-md-12">
                        <div>
                            <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
                            </nav>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-2">
                        <SideBar/>
                    </div>
                    <div className="col-md-10">
                        <div className="row mt-3">
                            <div className="col-md-2">
                                <Button className="btn btn-success"
                                        onClick={() => openModal('')}><PlusOutlined/></Button>
                            </div>
                            <div className="col-md-4 offset-6">
                                <h1>Box</h1>
                            </div>
                        </div>

                        <Table>
                            <thead>
                            <tr>
                                <th>T/R</th>
                                <th>Name</th>
                                <th>Color</th>
                                <th>BoxCategory</th>
                                <th>Photo</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {boxArray && boxArray.map((item, index) =>
                                <tr key={index}>
                                    <th>{index + 1}</th>
                                    <td>{item.name}</td>
                                    <td>
                                        <div style={{width: 60, height: 20, backgroundColor: item.colorDto.code}}></div>
                                    </td>
                                    <td>{item.boxCategoryDto.name}</td>
                                    <td>
                                        {item.attachmentIds ?
                                            <div><img style={{width: 30}}
                                                      src={api.attachment + item.attachmentIds[0]} alt=""/>
                                            </div> : ''}
                                    </td>
                                    <td>
                                        <Button className="btn btn-success"
                                                onClick={() => openModal(item)}><EditOutlined/></Button>
                                        <Button className="btn btn-danger ml-2"
                                                onClick={() => openModalDelete(item)}><DeleteOutlined/></Button>
                                    </td>
                                </tr>
                            )}
                            </tbody>
                        </Table>

                        <Modal isOpen={showModal} toggle={() => openModal('')}>
                            <ModalHeader toggle={() => openModal('')}>
                                {currentBox ? "Edit Box" : "Add Box"}
                            </ModalHeader>
                            <AvForm onValidSubmit={saveOrEditBox}>
                                <ModalBody>
                                    <AvField lable="Attachment"
                                             name="file"
                                             type="file"

                                        // defaultValue={currentBox ? currentBox.name : ''}
                                             placeholder="Attachment"
                                             onChange={setAttachment}/>

                                    <div style={{width: 30, height: 30}}>
                                            <img className="img-fluid"
                                                 src={api.attachment + selectedFileId[0]}
                                                 alt=""/>
                                    </div>
                                    <AvField lable="Name"
                                             name="name"
                                             type="text"
                                             required
                                             defaultValue={currentBox ? currentBox.name : ''}
                                             placeholder="Name"/>
                                    <AvField lable="Color"
                                             name="color"
                                             type="select"
                                        // defaultValue={currentDistrict ? currentDistrict?.region.id : ''}
                                             placeholder="Color"
                                             value={currentBox ? currentBox.colorDto.id : ''}
                                    >
                                        <option value="" disabled>Color talang</option>
                                        {colorArray && colorArray.map((item, index) =>
                                            <option key={index} value={item.id} name="color">{item.name}</option>
                                        )}

                                    </AvField>
                                    <AvField lable="BoxCategory"
                                             name="boxCategory"
                                             type="select"
                                        // defaultValue={currentDistrict ? currentDistrict?.region.id : ''}
                                             placeholder="BoxCategory"
                                             value={currentBox ? currentBox.boxCategoryDto.id : ''}
                                    >
                                        <option value="" disabled>Box category tanlang</option>
                                        {boxCategoryArray && boxCategoryArray.map((item, index) =>
                                            <option key={index} value={item.id} name="boxCategory">{item.name}</option>
                                        )}

                                    </AvField>
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="danger" onClick={() => openModal('')}>Cancel</Button>
                                    <Button color="success" type="submit">Save</Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>

                        <Modal isOpen={showModalDelete} toggle={() => openModalDelete('')}>
                            <ModalHeader>Rostdan ham o'chirmoqchimisiz?</ModalHeader>
                            <AvForm onValidSubmit={deleteBox}>
                                <ModalFooter>
                                    <Button className="btn btn-danger"
                                            onClick={() => openModalDelete('')}>Cancel</Button>
                                    <Button className="btn btn-success" type="submit">Delete</Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>


                    </div>
                </div>
            </div>
        </div>
    )
}

export default Box;