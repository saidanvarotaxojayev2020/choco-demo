import React, {useEffect, useState} from "react";
import SideBar from "../components/SideBar";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {DeleteOutlined, EditOutlined, PlusOutlined} from '@ant-design/icons';
import axios from "axios";
import {TOKEN} from "../../utills/constants";
import {api} from "../../api";


function Color() {

    const [showModal, setShowModal] = useState(false);
    const [colorArray, setColorArray] = useState([]);
    const [currentColor, setCurrentColor] = useState('');
    const [showModalDelete, setShowModalDelete] = useState(false);

    function openModalDelete(v) {
        setCurrentColor(v)
        setShowModalDelete(!showModalDelete)

    }
        function deleteProduct() {
            axios.delete(api.color + currentColor.id)
                .then(res => {
                axios.get(api.color,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const colorArray1 = res.data._embedded.list
                    colorArray1.reverse()
                    setColorArray(colorArray1)
                })
            })
            setShowModalDelete(!showModalDelete)
        }

        function openModal(v) {
            setCurrentColor(v)
            setShowModal(!showModal)
        }

        function saveOrEditColor(e, v) {
            if (currentColor) {
                axios.put(api.color+ currentColor.id,
                    v,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    axios.get(api.color,
                        {
                            headers:
                                {
                                    "Authorization": localStorage.getItem(TOKEN),
                                }
                        }).then(res => {
                        const colorArray1 = res.data._embedded.list
                        colorArray1.reverse()
                        setColorArray(colorArray1)
                    })
                })
            } else {
                axios.post(api.color, v,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    axios.get(api.color,
                        {
                            headers:
                                {
                                    "Authorization": localStorage.getItem(TOKEN),
                                }
                        }).then(res => {
                        const colorArray1 = res.data._embedded.list
                        colorArray1.reverse()
                        setColorArray(colorArray1)
                    })
                })
            }
            setShowModal(!showModal)
        }

        useEffect(() => {
            axios.get(api.color,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                const colorArray1 = res.data._embedded.list
                colorArray1.reverse()
                setColorArray(colorArray1)
            })
        }, [])


        return (
            <div>
                <div className="row">
                    <div className="col-md-12">
                        <div>
                            <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
                            </nav>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-2">
                        <SideBar/>
                    </div>
                    <div className="col-md-10">
                        <div className="row mt-3">
                            <div className="col-md-2">
                                <Button className="btn btn-success"
                                        onClick={() => openModal('')}><PlusOutlined/></Button>
                            </div>
                            <div className="col-md-4 offset-6">
                                <h1>Color</h1>
                            </div>
                        </div>
                        <Table>
                            <thead>
                            <tr>
                                <th>T/R</th>
                                <th>Name</th>
                                <th>code</th>
                            </tr>
                            </thead>
                            <tbody>
                            {colorArray && colorArray.map((item, index) =>
                                <tr>
                                    <th>{index + 1}</th>
                                    <td>{item.name}</td>
                                    <td><div style={{width: 60, height: 20, backgroundColor: item.code}}></div></td>
                                    <td>
                                        <Button className="btn btn-success"
                                                onClick={() => openModal(item)}><EditOutlined/></Button>
                                        <Button className="btn btn-danger ml-2"
                                                onClick={() => openModalDelete(item)}><DeleteOutlined/></Button>
                                    </td>
                                </tr>
                            )}
                            </tbody>
                        </Table>

                        <Modal isOpen={showModal} toggle={() => openModal('')}>
                            <ModalHeader toggle={() => openModal('')}>
                                {currentColor ? "Edit Color" : "Add Color"}
                            </ModalHeader>
                            <AvForm onValidSubmit={saveOrEditColor}>
                                <ModalBody>
                                    <AvField lable="Name"
                                             name="name"
                                             type="text"
                                             required
                                             defaultValue={currentColor ? currentColor.name : ''}
                                             placeholder="Name"/>
                                    <AvField lable="Code"
                                             name="code"
                                             type="color"
                                             defaultValue={currentColor ? currentColor.code : ''}
                                             placeholder="rang codini kiriting"/>
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="danger" onClick={() => openModal('')}>Cancel</Button>
                                    <Button color="success" type="submit">Save</Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>

                        <Modal isOpen={showModalDelete} toggle={() => openModalDelete('')}>
                            <ModalHeader>Rostdan ham o'chirmoqchimisiz?</ModalHeader>
                            <AvForm onValidSubmit={deleteProduct}>
                                <ModalFooter>
                                    <Button className="btn btn-danger"
                                            onClick={() => openModalDelete('')}>Cancel</Button>
                                    <Button className="btn btn-success" type="submit">Delete</Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>
                    </div>
                </div>
            </div>
        )
}

export default Color