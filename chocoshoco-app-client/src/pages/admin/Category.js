import React, {useEffect, useState} from "react";
import SideBar from "../components/SideBar";
import {useHistory} from "react-router-dom";
import {TOKEN} from "../../utills/constants";
import axios from "axios";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {DeleteOutlined, EditOutlined, PlusOutlined} from "@ant-design/icons";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {api} from "../../api";


function Category() {
    let history = useHistory();

    const [categoryArray, setCategoryArray] = useState([])
    const [currentCategory, setCurrentCategory] = useState('')
    const [showModal, setShowModal] = useState(false)
    const [showModalDelete, setShowModalDelete] = useState(false)

    useEffect(() => {
        axios.get(api.categoryAll,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setCategoryArray(res.data.object)
        })
    }, [])

    function openModal(item) {
        setCurrentCategory(item)
        setShowModal(!showModal)
    }

    function openModalDelete(item) {
        setCurrentCategory(item)
        setShowModalDelete(!showModalDelete)
    }

    function deleteCategory() {
        axios.delete(api.category + currentCategory.id,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            axios.get(api.categoryAll,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                setCategoryArray(res.data.object)
            })
        })
        setShowModalDelete(!showModalDelete)
    }

    function saveOrEditCategory(e, v) {
        let test = {
            ...v,
            id: currentCategory ? currentCategory.id : null
        }
        axios.post(api.category,
            test,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN)
                    }
            }).then(res => {
            axios.get(api.categoryAll,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                setCategoryArray(res.data.object)
            })
        })
    }

    return (
        <div>
            <div className="row">
                <div className="col-md-12">
                    <div>

                        <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
                        </nav>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">
                    <SideBar/>
                </div>
                <div className="col-md-10">
                    <div className="row mt-3">
                        <div className="col-md-2">
                            <Button className="btn btn-success"
                                    onClick={() => openModal('')}><PlusOutlined/></Button>
                        </div>
                        <div className="col-md-4 offset-6">
                            <h1>Category</h1>
                        </div>
                    </div>
                    <Table>
                        <thead>
                        <tr>
                            <th>T/R</th>
                            <th>Name</th>
                            <th>ParentCategory</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {categoryArray && categoryArray.map((item, index) =>
                            <tr key={index}>
                                <th>{index + 1}</th>
                                <td>{item.name}</td>
                                <td>{item.parentCategoryName}</td>
                                <td>
                                    <Button className="btn btn-success"
                                            onClick={() => openModal(item)}><EditOutlined/></Button>
                                    <Button className="btn btn-danger ml-2"
                                            onClick={() => openModalDelete(item)}><DeleteOutlined/></Button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </Table>

                    <Modal isOpen={showModal} toggle={() => openModal('')}>
                        <ModalHeader toggle={() => openModal('')}>
                            {currentCategory ? "Edit category" : "Add category"}
                        </ModalHeader>
                        <AvForm onValidSubmit={saveOrEditCategory}>
                            <ModalBody>
                                <AvField lable="Name"
                                         name="name"
                                         type="text"
                                         required
                                         defaultValue={currentCategory ? currentCategory.name : ''}
                                         placeholder="Name"/>
                                <AvField lable="Category"
                                         name="parentCategoryId"
                                         type="select"
                                         placeholder="Category"
                                         value={currentCategory.parentCategoryId ? currentCategory.parentCategoryId : ''}
                                >
                                    <option value="" disabled>Categoryni tanlang</option>
                                    {categoryArray && categoryArray.map((item, index) =>
                                        <option key={index} value={item.id}
                                                name="parentCategoryId">{item.name}</option>
                                    )}
                                </AvField>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="danger" onClick={() => openModal('')}>Cancel</Button>
                                <Button color="success" type="submit">Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    <Modal isOpen={showModalDelete} toggle={() => openModalDelete('')}>
                        <ModalHeader>Rostdan ham o'chirmoqchimisiz?</ModalHeader>
                        <AvForm onValidSubmit={deleteCategory}>
                            <ModalFooter>
                                <Button className="btn btn-danger"
                                        onClick={() => openModalDelete('')}>Cancel</Button>
                                <Button className="btn btn-success" type="submit">Delete</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>
                </div>
            </div>
        </div>
    )
}

export default Category;