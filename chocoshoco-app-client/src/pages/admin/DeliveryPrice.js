import React, {useEffect, useState} from "react";
import SideBar from "../components/SideBar";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {DeleteOutlined, EditOutlined, PlusOutlined} from "@ant-design/icons";
import axios from "axios";
import {TOKEN} from "../../utills/constants";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {useHistory} from "react-router-dom";
import {api} from "../../api";

function DeliveryPrice() {
    let history = useHistory();
    const [deliveryPriceArray, setDeliveryPriceArray] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [currentDeliveryPrice, setCurrentDeliveryPrice] = useState('');
    const [districtArray, setDistrictArray] = useState('');

    useEffect(() => {
        axios.get(api.deliveryPrice,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setDeliveryPriceArray(res.data._embedded.list)
        })
    }, [])

    useEffect(() => {
        axios.get(api.district,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setDistrictArray(res.data._embedded.list)
        })
    }, [])

    function openModal(item) {
        setCurrentDeliveryPrice(item)
        setShowModal(!showModal)
    }

    function openModalDelete(item) {
        setCurrentDeliveryPrice(item)
        setShowModalDelete(!showModalDelete)
    }

    function saveOrEditDeliveryPrice(e, v) {
        let test;
        if (currentDeliveryPrice) {
            test = {
                price: v.price,
                districtId: '/' + v.district
            }
            axios.put(api.deliveryPrice + currentDeliveryPrice.id, test, {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
                axios.get(api.deliveryPrice,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    setDeliveryPriceArray(res.data._embedded.list)
                })
            })
        } else {
            test = {
                ...v,
                district: '/' + v.district
            }
            axios.post(api.deliveryPrice, test, {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
                axios.get(api.deliveryPrice,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    setDeliveryPriceArray(res.data._embedded.list)
                })
            })
        }
        setShowModal(!showModal)
    }

    function deleteDeliveryPrice() {
        axios.delete(api.deliveryPrice + currentDeliveryPrice.id,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            axios.get(api.deliveryPrice,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                setDeliveryPriceArray(res.data._embedded.list)
            })
        })
        setShowModalDelete(!showModalDelete)
    }

    return (
        <div>
            <div className="row">
                <div className="col-md-12">
                    <div>
                        <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
                        </nav>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">
                    <SideBar/>
                </div>
                <div className="col-md-10">
                    <div className="row mt-3">
                        <div className="col-md-2">
                            <Button className="btn btn-success"
                                    onClick={() => openModal('')}><PlusOutlined/></Button>
                        </div>
                        <div className="col-md-4 offset-6">
                            <h1>Delivery Price</h1>
                        </div>
                    </div>
                    <Table>
                        <thead>
                        <tr>
                            <th>T/R</th>
                            <th>Tuman nomi</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        {deliveryPriceArray && deliveryPriceArray.map((item, index) =>
                            <tr key={index}>
                                <th>{index + 1}</th>
                                <td>{item.districtName}</td>
                                <td>{item.price}</td>
                                <td>
                                    <Button className="btn btn-success"
                                            onClick={() => openModal(item)}><EditOutlined/></Button>
                                    <Button className="btn btn-danger ml-2"
                                            onClick={() => openModalDelete(item)}><DeleteOutlined/></Button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </Table>

                    <Modal isOpen={showModal} toggle={() => openModal('')}>
                        <ModalHeader toggle={() => openModal('')}>
                            {currentDeliveryPrice ? "Edit DeliveryPrice" : "Add DeliveryPrice"}
                        </ModalHeader>
                        <AvForm onValidSubmit={saveOrEditDeliveryPrice}>
                            <ModalBody>
                                <div className="row">
                                    <div className="col-md-12">
                                        <AvField lable="Category"
                                                 name="district"
                                                 type="select"
                                            // defaultValue={currentDeliveryPrice ? currentDeliveryPrice.districtName : ''}
                                                 placeholder="Category"
                                                 value={currentDeliveryPrice ? currentDeliveryPrice.districtId : ''}
                                        >
                                            <option value="" disabled>Categoryni tanlang</option>
                                            {districtArray && districtArray.map((item, index) =>
                                                <option key={index} value={item.id}
                                                        name="district">{item.name}</option>
                                            )}
                                        </AvField>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <AvField lable="Price"
                                                 name="price"
                                                 type="number"
                                                 value={currentDeliveryPrice ? currentDeliveryPrice.price : ''}
                                                 placeholder="Price"/>
                                    </div>
                                </div>
                            </ModalBody>
                            <ModalFooter>
                                <Button product="danger" onClick={() => openModal('')}>Cancel</Button>
                                <Button product="success" type="submit">Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    <Modal isOpen={showModalDelete} toggle={() => openModalDelete('')}>
                        <ModalHeader>Rostdan ham o'chirmoqchimisiz?</ModalHeader>
                        <AvForm onValidSubmit={deleteDeliveryPrice}>
                            <ModalFooter>
                                <Button className="btn btn-danger"
                                        onClick={() => openModalDelete('')}>Cancel</Button>
                                <Button className="btn btn-success" type="submit">Delete</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>
                </div>
            </div>
        </div>
    )
}

export default DeliveryPrice;