import React, {useEffect, useState} from "react";
import SideBar from "../components/SideBar";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {DeleteOutlined, EditOutlined, PlusOutlined} from '@ant-design/icons';
import axios from "axios";
import {TOKEN} from "../../utills/constants";
import {useHistory} from "react-router-dom";
import {api} from "../../api";


function BoxCategory() {
    let history = useHistory();
    const [showModal, setShowModal] = useState(false);
    const [boxCategoryArray, setBoxCategoryArray] = useState([]);
    const [currentBoxCategory, setCurrentBoxCategory] = useState('');
    const [showModalDelete, setShowModalDelete] = useState(false);



    function openModalDelete(v) {
        setCurrentBoxCategory(v)
        setShowModalDelete(!showModalDelete)
    }

    function deleteBoxCategory() {
        axios.delete(api.boxCategory + currentBoxCategory.id)
            .then(res => {
                axios.get(api.boxCategory,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const boxCategoryArray1 = res.data._embedded.list
                    boxCategoryArray1.reverse()
                    setBoxCategoryArray(boxCategoryArray1)
                })
            })
        setShowModalDelete(!showModalDelete)
    }

    function openModal(v) {
        setCurrentBoxCategory(v)
        setShowModal(!showModal)
    }

    function saveOrEditBoxCategory(e, v) {
        if (currentBoxCategory) {
            axios.put(api.boxCategory + currentBoxCategory.id,
                v,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                axios.get(api.boxCategory,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const boxCategoryArray1 = res.data._embedded.list
                    boxCategoryArray1.reverse()
                    setBoxCategoryArray(boxCategoryArray1)
                })
            })
        } else {
            axios.post(api.boxCategory, v,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                axios.get(api.boxCategory,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const boxCategoryArray1 = res.data._embedded.list
                    boxCategoryArray1.reverse()
                    setBoxCategoryArray(boxCategoryArray1)
                })
            })
        }
        setShowModal(!showModal)
    }

    useEffect(() => {
        axios.get(api.boxCategory,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const boxCategoryArray1 = res.data._embedded.list
            boxCategoryArray1.reverse()
            setBoxCategoryArray(boxCategoryArray1)
        })
    }, [])

    return (
        <div>
            <div className="row">
                <div className="col-md-12">
                    <div>
                        <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
                        </nav>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">
                    <SideBar/>
                </div>
                <div className="col-md-10">
                    <div className="row mt-3">
                        <div className="col-md-2">
                            <Button className="btn btn-success" onClick={() => openModal('')}><PlusOutlined/></Button>
                        </div>
                        <div className="col-md-4 offset-6">
                            <h1>BoxCategory</h1>
                        </div>
                    </div>
                    <Table>
                        <thead>
                        <tr>
                            <th>T/R</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {boxCategoryArray && boxCategoryArray.map((item, index) =>
                            <tr>
                                <th>{index + 1}</th>
                                <td>{item.name}</td>
                                <td>
                                    <Button className="btn btn-success" onClick={() => openModal(item)}><EditOutlined/></Button>
                                    <Button className="btn btn-danger ml-2"
                                            onClick={() => openModalDelete(item)}><DeleteOutlined/></Button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </Table>

                    <Modal isOpen={showModal} toggle={() => openModal('')}>
                        <ModalHeader toggle={() => openModal('')}>
                            {currentBoxCategory ? "Edit box category" : "Add box category"}
                        </ModalHeader>
                        <AvForm onValidSubmit={saveOrEditBoxCategory}>
                            <ModalBody>
                                <AvField lable="Name"
                                         name="name"
                                         type="text"
                                         required
                                         defaultValue={currentBoxCategory ? currentBoxCategory.name : ''}
                                         placeholder="Name"/>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="danger" onClick={() => openModal('')}>Cancel</Button>
                                <Button color="success" type="submit">Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    <Modal isOpen={showModalDelete} toggle={() => openModalDelete('')}>
                        <ModalHeader>Rostdan ham o'chirmoqchimisiz?</ModalHeader>
                        <AvForm onValidSubmit={deleteBoxCategory}>
                            <ModalFooter>
                                <Button className="btn btn-danger"
                                        onClick={() => openModalDelete('')}>Cancel</Button>
                                <Button className="btn btn-success" type="submit">Delete</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>
                </div>
            </div>
        </div>
    )
}

export default BoxCategory;