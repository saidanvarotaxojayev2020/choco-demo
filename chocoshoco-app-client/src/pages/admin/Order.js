import React, {useEffect, useState} from "react";
import SideBar from "../components/SideBar";
import {Button, Card, CardBody, Collapse, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {PlusOutlined} from "@ant-design/icons";
import {AvField, AvForm} from "availity-reactstrap-validation";
import axios from "axios";
import {TOKEN} from "../../utills/constants";
import 'pure-react-carousel/dist/react-carousel.es.css';
import {Select} from 'antd';
import {useHistory} from "react-router-dom";
import {api} from "../../api";

const {Option} = Select;

function Order() {
    let history = useHistory();
    const [showModalStatus, setShowModalStatus] = useState(false);
    const [showModalSaveOrEdit, setShowModalSaveOrEdit] = useState(false);
    const [currentOrder, setCurrentOrder] = useState('');
    const [orderArrayNew, setOrderArrayNew] = useState([]);
    const [orderArrayInProgress, setOrderArrayInProgress] = useState([]);
    const [orderArrayCompleted, setOrderArrayCompleted] = useState([]);
    const [orderArrayFinished, setOrderArrayFinished] = useState([]);
    const [orderArrayCanceled, setOrderArrayCanceled] = useState([]);
    const [boxArray, setBoxArray] = useState([]);
    const [productWithAmountArray, setProductWithAmountArray] = useState([]);
    const [allCategoryArray, setAllCategoryArray] = useState([])
    const [allProduct, setAllProduct] = useState([])
    const [indexProduct, setIndexProduct] = useState(0)
    const [districtArray, setDistrictArray] = useState([]);
    const [clientArray, setClientArray] = useState([]);
    const [clientId, setClientId] = useState('');
    const [boxCategory, setBoxCategory] = useState([]);
    const [oneBox, setOneBox] = useState([]);
    const [indexBox, setIndexBox] = useState(0);
    const [isOpen, setIsOpen] = useState(false);
    const [currentOrderStatus, setCurrentOrderStatus] = useState('')
    const [showModalDelete, setShowModalDelete] = useState(false);



    function toggle() {
        setIsOpen(!isOpen)
    }


    function openModalSaveOrEdit(item) {
        setShowModalSaveOrEdit(!showModalSaveOrEdit)
        if (item) {
            setOneBox([item.boxDto])
            setClientId(item.userDto.id)
            setProductWithAmountArray(item.productWithAmountDtos)
            setIsOpen(item.addressDto !== null ? !isOpen : isOpen)
        } else {
            setIsOpen(false)
        }
        setCurrentOrder(item);

    }

    function openModalStatus(item) {
        setCurrentOrder(item)
        setCurrentOrderStatus(item.orderStatus)
        setShowModalStatus(!showModalStatus)
    }

    function saveOrEditOrder(e, v) {
        const order = {
            id: currentOrder ? currentOrder.id : null,
            userDto: {
                id: clientId
            },
            date: v.date,
            orderStatus: 'NEW',
            addressDto: isOpen ? {
                street: v.street,
                house: v.homeNumber,
                districtDto: {
                    id: v.district
                }
            } : null,
            clientNumber: v.clientNumber,
            boxDto: {
                id: oneBox[0].id
            },
            productWithAmountDtos: productWithAmountArray
        }

        axios.post(api.order, order, {
            headers: {
                "Authorization": localStorage.getItem(TOKEN)
            }
        }).then(res => {
            axios.get(api.order + '?page=0&size=10&orderStatus=NEW&search=client',
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                setOrderArrayNew(res.data.object)
            })
        })
        setShowModalSaveOrEdit(!showModalSaveOrEdit)
    }

    useEffect(() => {
        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=NEW',
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setOrderArrayNew(res.data.object)
        })
    }, [])

    useEffect(() => {
        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=IN_PROGRESS',
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setOrderArrayInProgress(res.data.object)
        })
    }, [])

    useEffect(() => {
        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=COMPLETED',
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setOrderArrayCompleted(res.data.object)
        })
    }, [])

    useEffect(() => {
        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=FINISHED',
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setOrderArrayFinished(res.data.object)
        })
    }, [])

    useEffect(() => {
        axios.get(api.boxCategory, {
            headers: {
                "Authorization": localStorage.getItem(TOKEN)
            }
        }).then(res => {
            setBoxCategory(res.data._embedded.list)
        })
    }, [])

    useEffect(() => {
        axios.get(api.box,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const boxArr = res.data.object
            if (boxArr.length) {
                setBoxArray(boxArr)
                setOneBox([boxArr[0]])
            }
        })
    }, [])

    function setBoxCategoryy(e) {
        axios.get(api.getBoxByCategoryId + e.target.value, {
            headers:
                {
                    "Authorization": localStorage.getItem(TOKEN),
                }
        }).then(res => {
            setBoxArray(res.data.object)
            setOneBox([res.data.object[0]])
        })
    }

    function setProductCategoryy(e, index) {
        axios.get(api.productByCategory + e.target.value, {
            headers:
                {
                    "Authorization": localStorage.getItem(TOKEN),
                }
        }).then(res => {
            setAllProduct(res.data.object)
            let newProductWithAmountArray = productWithAmountArray.map((item, i) => {
                if (i === index) {
                    return {...item, productDto: res.data.object[0]}
                } else {
                    return item
                }
                // return i === index ? {...item, productDto: allProducts[indexProd] : item}
            })
            setProductWithAmountArray(newProductWithAmountArray)
        })
    }

    useEffect(() => {
        axios.get(api.categoryAll, {
            headers:
                {
                    "Authorization": localStorage.getItem(TOKEN),
                }
        }).then(res => {
            setAllCategoryArray(res.data.object)
        })
    }, [])

    useEffect(() => {
        axios.get(api.product, {
            headers: {
                "Authorization": localStorage.getItem(TOKEN)
            }
        }).then(res => {
            setAllProduct(res.data.object)
        })
    }, [])

    function rightBox() {
        let index = indexBox + 1
        setIndexBox(index > boxArray.length - 1 ? index = 0 : index)
        setOneBox(index > boxArray.length - 1 ? [boxArray[0]] : [boxArray[index]])
    }

    function leftBox() {
        let index = indexBox - 1
        setIndexBox(index < 0 ? boxArray.length - 1 : index)
        setOneBox(index < 0 ? [boxArray[boxArray.length - 1]] : [boxArray[index]])
    }

    function leftProduct(index) {
        let indexProd = indexProduct - 1 < 0 ? allProduct.length - 1 : indexProduct - 1
        let allProducts = allProduct;  // state dan o'zgaruvchiga oldim
        let newProductWithAmountArray = productWithAmountArray.map((item, i) => {
            if (i === index) {
                return {...item, productDto: allProducts[indexProd]}
            } else {
                return item
            }
            // return i === index ? {...item, productDto: allProducts[indexProd] : item}
        })
        setProductWithAmountArray(newProductWithAmountArray)
        setIndexProduct(indexProd)
    }

    function rightProduct(index) {
        let indexProd = indexProduct + 1 > allProduct.length - 1 ? 0 : indexProduct + 1
        let allProducts = allProduct;  // state dan o'zgaruvchiga oldim
        let newProductWithAmountArray = productWithAmountArray.map((item, i) => {
            if (i === index) {
                return {...item, productDto: allProducts[indexProd]}
            } else {
                return item
            }
            // return i === index ? {...item, productDto: allProducts[indexProd] : item}
        })
        setProductWithAmountArray(newProductWithAmountArray)
        setIndexProduct(indexProd)
    }

    function plusProductRow() {
        if (allProduct.length) {
            setProductWithAmountArray([...productWithAmountArray, {
                id: '',
                productDto: {
                    id: allProduct[0].id,
                    name: allProduct[0].name,
                    price: allProduct[0].price,
                    attachmentIds: allProduct[0].attachmentIds,
                    categoryDto: {
                        id: allProduct[0].categoryDto.id,
                        name: allProduct[0].categoryDto.name
                    },
                    colorDto: {
                        id: allProduct[0].colorDto.id,
                        name: allProduct[0].colorDto.name,
                        code: allProduct[0].colorDto.code
                    },
                    measurementDto: {
                        id: '',
                        name: ''
                    },
                    measureValue: '',
                    hasContent: allProduct[0].hasContent
                },
                amount: '',
                content: '',
                orderDto: {},
                productSetDto: {
                    id: ''
                }
            }])
        } else {
            setProductWithAmountArray([...productWithAmountArray, {
                id: '',
                productDto: {
                    id: '',
                    name: '',
                    price: '',
                    attachmentIds: '',
                    categoryDto: {
                        id: '',
                        name: ''
                    },
                    colorDto: {
                        id: '',
                        name: '',
                        code: ''
                    },
                    measurementDto: {
                        id: '',
                        name: ''
                    },
                    measureValue: '',
                    hasContent: ''
                },
                amount: '',
                content: '',
                orderDto: {},
                productSetDto: {
                    id: ''
                }
            }])
        }

    }

    function setInputProduct(e, index) {
        let value = e.target.value
        let newProductWithAmountArray = productWithAmountArray.map((item, i) => {
            if (i === index) {
                if (item.productDto.hasContent === true) {
                    return {...item, content: value, amount: value.length}
                } else {
                    return {...item, amount: value}
                }
            } else {
                return item
            }
        })
        setProductWithAmountArray(newProductWithAmountArray)
    }

    function deleteProductRow(index) {
        let newProdAmount = productWithAmountArray.filter(function (item, i) {
            if (i !== index) {
                return item
            }
        })
        setProductWithAmountArray(newProdAmount)
    }

    useEffect(() => {
        axios.get(api.district,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const districtArray1 = res.data._embedded.list
            districtArray1.reverse()
            setDistrictArray(districtArray1)
        })
    }, [])

    function onSearchClient(val) {
        axios.get(api.searchUserByRoleName + '?search=' + val + '&roleName=ROLE_CLIENT')
            .then(res => {
                setClientArray(res.data.object)
            })
    }

    function onChangeClient(value) {
        setClientId(value)
    }

    function searchOrderByClientId(value) {
        if (!value) {
            value = ''
        }
        axios.get(api.order + '?search=client&page=0&size=10&orderStatus=NEW&userId=' + value,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setOrderArrayNew(res.data.object)
        })
        axios.get(api.order + '?search=client&page=0&size=10&orderStatus=IN_PROGRESS&userId=' + value,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setOrderArrayInProgress(res.data.object)
        })
        axios.get(api.order + '?search=client&page=0&size=10&orderStatus=COMPLETED&userId=' + value,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setOrderArrayCompleted(res.data.object)
        })
    }

    function deleteOrder() {
        axios.delete(api.order + '/' + currentOrder.id,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            switch (currentOrderStatus) {
                case "NEW":
                    axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=NEW',
                        {
                            headers:
                                {
                                    "Authorization": localStorage.getItem(TOKEN),
                                }
                        }).then(res => {
                        setOrderArrayNew(res.data.object)
                    })
                    break
                case "IN_PROGRESS":
                    axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=IN_PROGRESS',
                        {
                            headers:
                                {
                                    "Authorization": localStorage.getItem(TOKEN),
                                }
                        }).then(res => {
                        setOrderArrayInProgress(res.data.object)
                    })
                    break
                case "COMPLETED":
                    axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=COMPLETED',
                        {
                            headers:
                                {
                                    "Authorization": localStorage.getItem(TOKEN),
                                }
                        }).then(res => {
                        setOrderArrayCompleted(res.data.object)
                    })
                    break
            }
            setShowModalDelete(!showModalDelete)
        })
    }

    const openModalDelete = (item) => {
        setCurrentOrder(item)
        setCurrentOrderStatus(item.orderStatus)
        setShowModalDelete(!showModalDelete)
    }

    function selectNew() {
        axios.put(api.editStatus + '?orderId=' + currentOrder.id + '&orderStatus=NEW',
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
                switch (currentOrderStatus) {
                    case "NEW":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=NEW',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayNew(res.data.object)
                        })
                        break
                    case "IN_PROGRESS":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=IN_PROGRESS',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayInProgress(res.data.object)
                        })
                        break
                    case "COMPLETED":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=COMPLETED',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayCompleted(res.data.object)
                        })
                        break
                    case "FINISHED":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=FINISHED',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayFinished(res.data.object)
                        })
                        break
                }
                axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=NEW',
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    setOrderArrayNew(res.data.object)
                })
            }
        )
        setShowModalStatus(!showModalStatus)
    }

    function selectInProgress() {
        axios.put(api.editStatus + '?orderId=' + currentOrder.id + '&orderStatus=IN_PROGRESS',
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
                switch (currentOrderStatus) {
                    case "NEW":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=NEW',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayNew(res.data.object)
                        })
                        break
                    case "IN_PROGRESS":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=IN_PROGRESS',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayInProgress(res.data.object)
                        })
                        break
                    case "COMPLETED":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=COMPLETED',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayCompleted(res.data.object)
                        })
                        break
                    case "FINISHED":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=FINISHED',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayFinished(res.data.object)
                        })
                        break
                }
                axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=IN_PROGRESS',
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    setOrderArrayInProgress(res.data.object)
                })
            }
        )
        setShowModalStatus(!showModalStatus)
    }

    function selectCompleted() {
        axios.put(api.editStatus + '?orderId=' + currentOrder.id + '&orderStatus=COMPLETED',
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
                switch (currentOrderStatus) {
                    case "NEW":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=NEW',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayNew(res.data.object)
                        })
                        break
                    case "IN_PROGRESS":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=IN_PROGRESS',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayInProgress(res.data.object)
                        })
                        break
                    case "COMPLETED":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=COMPLETED',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayCompleted(res.data.object)
                        })
                        break
                    case "FINISHED":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=FINISHED',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayFinished(res.data.object)
                        })
                        break
                }
                axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=COMPLETED',
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    setOrderArrayCompleted(res.data.object)
                })
            }
        )
        setShowModalStatus(!showModalStatus)
    }

    function selectFinished() {
        axios.put(api.editStatus + '?orderId=' + currentOrder.id + '&orderStatus=FINISHED',
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
                switch (currentOrderStatus) {
                    case "NEW":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=NEW',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayNew(res.data.object)
                        })
                        break
                    case "IN_PROGRESS":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=IN_PROGRESS',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayInProgress(res.data.object)
                        })
                        break
                    case "COMPLETED":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=COMPLETED',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayCompleted(res.data.object)
                        })
                        break
                    case "FINISHED":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=FINISHED',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayFinished(res.data.object)
                        })
                        break
                }
                axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=FINISHED',
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    setOrderArrayFinished(res.data.object)
                })
            }
        )
        setShowModalStatus(!showModalStatus)
    }

    function selectCanceled() {
        axios.put(api.editStatus + '?orderId=' + currentOrder.id + '&orderStatus=CANCELED',
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
                switch (currentOrderStatus) {
                    case "NEW":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=NEW',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayNew(res.data.object)
                        })
                        break
                    case "IN_PROGRESS":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=IN_PROGRESS',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayInProgress(res.data.object)
                        })
                        break
                    case "COMPLETED":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=COMPLETED',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayCompleted(res.data.object)
                        })
                        break
                    case "FINISHED":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=FINISHED',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayFinished(res.data.object)
                        })
                        break
                    case "CANCELED":
                        axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=CANCELED',
                            {
                                headers:
                                    {
                                        "Authorization": localStorage.getItem(TOKEN),
                                    }
                            }).then(res => {
                            setOrderArrayCanceled(res.data.object)
                        })
                        break
                }
                axios.get(api.order + '?search=bla&page=0&size=10&orderStatus=CANCELED',
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    setOrderArrayCanceled(res.data.object)
                })
            }
        )
        setShowModalStatus(!showModalStatus)
    }

    return (
        <div>
            <div className="row">
                <div className="col-md-12">
                    <div>
                        <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">

                        </nav>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">
                    <SideBar/>
                </div>
                <div className="col-md-10">
                    <div className="row mt-3">
                        <div className="col-md-1">
                            <Button className="btn btn-success" onClick={() => openModalSaveOrEdit('')}>
                                <PlusOutlined/></Button>
                        </div>
                        <div className="col-md-2">
                            <Select
                                showSearch
                                style={{width: 250}}
                                placeholder={"Search order by client number"}
                                optionFilterProp="children"
                                onChange={searchOrderByClientId}
                                required={true}
                                // onFocus={true}
                                // onBlur={true}
                                onSearch={onSearchClient}
                                allowClear={true}
                                filterOption={false}
                            >
                                {clientArray && clientArray.map(item => {
                                        return <Option key={item.id} value={item.id}>{item.firstName}</Option>
                                    }
                                )}
                            </Select>
                        </div>
                        <div className="col-md-4 offset-5">
                            <h1>Order</h1>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-3 col-md-6">
                            <div className="row">
                                <div className="col-12">
                                    <div className="card bg-primary text-white mb-4">
                                        <div className="card-body">Yangi zakaz</div>
                                    </div>
                                </div>
                            </div>

                            {orderArrayNew && orderArrayNew.map((item, index) =>
                                <div className="row" key={index}>
                                    <div className="col-12">
                                        <div className="card border-info  mb-4">
                                            <div>
                                                <p className="m-2">Total price: <b>{item.totalPrice}</b></p>
                                                <p className="m-2">Karobka nomi: <b>{item.boxDto.name}</b></p>
                                                <p className="m-2">User name: <b>{item.userDto.firstName}</b></p>
                                                <p className="m-2">User phone number: <b>{item.userDto.phoneNumber}</b>
                                                </p>
                                                {item.productWithAmountDtos && item.productWithAmountDtos.map((item, index) =>
                                                    <div key={index} className="card border-info">
                                                        <p className="m-2">Product: {item.content ? item.content : item.amount}</p>
                                                        <p className="m-2">Product name: {item.productDto.name}</p>
                                                    </div>
                                                )}
                                                <div className="justify-content-center col-md-12">
                                                    <Button className="btn btn-danger"
                                                            onClick={() => openModalDelete(item)}>Delete</Button>
                                                    <Button className="btn btn-success"
                                                            onClick={() => openModalSaveOrEdit(item)}>Edit</Button>
                                                    <Button className="btn btn-dark"
                                                            onClick={() => openModalStatus(item)}>Status</Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )}


                        </div>
                        <div className="col-xl-3 col-md-6">
                            <div className="row">
                                <div className="col-12">
                                    <div className="card bg-danger text-white mb-4">
                                        <div className="card-body">Tayyorlanadigan zakaz</div>
                                    </div>
                                </div>
                            </div>
                            {orderArrayInProgress && orderArrayInProgress.map((item, index) =>
                                <div className="row" key={index}>
                                    <div className="col-12">
                                        <div className="card border-info  mb-4">
                                            <div>
                                                <p className="m-2">Total price: <b>{item.totalPrice}</b></p>
                                                <p className="m-2">Karobka nomi: <b>{item.boxDto.name}</b></p>
                                                <p className="m-2">User name: <b>{item.userDto.firstName}</b></p>
                                                <p className="m-2">User phone number: <b>{item.userDto.phoneNumber}</b>
                                                </p>
                                                {item.productWithAmountDtos && item.productWithAmountDtos.map((item, index) =>
                                                    <div key={index} className="card border-info">
                                                        <p className="m-2">Product: {item.content ? item.content : item.amount}</p>
                                                        <p className="m-2">Product name: {item.productDto.name}</p>
                                                    </div>
                                                )}
                                                <div className="justify-content-center col-md-12">
                                                    <Button className="btn btn-danger"
                                                            onClick={() => openModalDelete(item)}>Delete</Button>
                                                    <Button className="btn btn-success"
                                                            onClick={() => openModalSaveOrEdit(item)}>Edit</Button>
                                                    <Button className="btn btn-dark"
                                                            onClick={() => openModalStatus(item)}>Status</Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                        <div className="col-xl-3 col-md-6">
                            <div className="row">
                                <div className="col-12">
                                    <div className="card bg-dark text-white mb-4">
                                        <div className="card-body">Tayyor zakaz</div>
                                    </div>
                                </div>
                            </div>
                            {orderArrayCompleted && orderArrayCompleted.map((item, index) =>
                                <div className="row" key={index}>
                                    <div className="col-12">
                                        <div className="card border-info  mb-4">
                                            <div>
                                                <p className="m-2">Total price: <b>{item.totalPrice}</b></p>
                                                <p className="m-2">Karobka nomi: <b>{item.boxDto.name}</b></p>
                                                <p className="m-2">User name: <b>{item.userDto.firstName}</b></p>
                                                <p className="m-2">User phone number: <b>{item.userDto.phoneNumber}</b>
                                                </p>
                                                {item.productWithAmountDtos && item.productWithAmountDtos.map((item, index) =>
                                                    <div key={index} className="card border-info">
                                                        <p className="m-2">Product: {item.content ? item.content : item.amount}</p>
                                                        <p className="m-2">Product name: {item.productDto.name}</p>
                                                    </div>
                                                )}
                                                <div className="justify-content-center col-md-12">
                                                    <Button className="btn btn-danger"
                                                            onClick={() => openModalDelete(item)}>Delete</Button>
                                                    <Button className="btn btn-success"
                                                            onClick={() => openModalSaveOrEdit(item)}>Edit</Button>
                                                    <Button className="btn btn-dark"
                                                            onClick={() => openModalStatus(item)}>Status</Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                        <div className="col-xl-3 col-md-6">
                            <div className="row">
                                <div className="col-12">
                                    <div className="card bg-success text-white mb-4">
                                        <div className="card-body">Sotilgan zakaz</div>
                                    </div>
                                </div>
                            </div>
                            {orderArrayFinished && orderArrayFinished.map((item, index) =>
                                <div className="row" key={index}>
                                    <div className="col-12">
                                        <div className="card border-info  mb-4">
                                            <div>
                                                <p className="m-2">Total price: <b>{item.totalPrice}</b></p>
                                                <p className="m-2">Karobka nomi: <b>{item.boxDto.name}</b></p>
                                                <p className="m-2">User name: <b>{item.userDto.firstName}</b></p>
                                                <p className="m-2">User phone number: <b>{item.userDto.phoneNumber}</b>
                                                </p>
                                                {item.productWithAmountDtos && item.productWithAmountDtos.map((item, index) =>
                                                    <div key={index} className="card border-info">
                                                        <p className="m-2">Product: {item.content ? item.content : item.amount}</p>
                                                        <p className="m-2">Product name: {item.productDto.name}</p>
                                                    </div>
                                                )}
                                                <div className="justify-content-center col-md-12">
                                                    <Button className="btn btn-danger"
                                                            onClick={() => openModalDelete(item)}>Delete</Button>
                                                    <Button className="btn btn-success"
                                                            onClick={() => openModalSaveOrEdit(item)}>Edit</Button>
                                                    <Button className="btn btn-dark"
                                                            onClick={() => openModalStatus(item)}>Status</Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>

                    <Modal style={{maxWidth: '1200px'}} isOpen={showModalSaveOrEdit}
                           toggle={() => openModalSaveOrEdit('')}>
                        <ModalHeader toggle={() => openModalSaveOrEdit('')}>
                            Order info
                        </ModalHeader>
                        <AvForm onValidSubmit={saveOrEditOrder}>
                            <ModalBody>
                                <div className="row">
                                    <div className="col-md-12">
                                        <Select
                                            showSearch
                                            style={{width: 200}}
                                            placeholder={currentOrder ? currentOrder.userDto.firstName : "Select Client"}
                                            optionFilterProp="children"
                                            onChange={onChangeClient}
                                            required={true}
                                            // onFocus={true}
                                            // onBlur={true}
                                            onSearch={onSearchClient}
                                            allowClear={true}
                                            filterOption={false}
                                        >
                                            {clientArray && clientArray.map(item => {
                                                    return <Option key={item.id} value={item.id}>{item.firstName}</Option>
                                                }
                                            )}
                                        </Select>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-2">
                                        <AvField
                                            onChange={(e) => setBoxCategoryy(e)}
                                            lable="NameUz"
                                            name="category"
                                            type="select"
                                            placeholder="Category"
                                            value={currentOrder ? currentOrder.boxDto.boxCategoryDto.id : ''}
                                        >
                                            <option value="" disabled>Box Category</option>
                                            {boxCategory && boxCategory.map((item, index) =>
                                                <option value={item.id} name="category"
                                                        key={index}>{item.name}</option>
                                            )}
                                        </AvField>
                                        {oneBox && oneBox.map((item, index) =>
                                            <img key={index} src={api.attachment + item.attachmentIds[0]}
                                                 alt=""
                                                 width={'100%'}/>
                                        )}
                                        <button type="button" className="btn btn-success" onClick={leftBox}>Left
                                        </button>
                                        <button type="button" className="btn btn-success" onClick={rightBox}>Right
                                        </button>
                                    </div>

                                    {productWithAmountArray && productWithAmountArray.map((item, index) =>
                                        <div className="col-md-2" key={index}>
                                            <div>
                                                <AvField
                                                    onChange={(e) => setProductCategoryy(e, index)}
                                                    lable="NameUz"
                                                    name="category"
                                                    type="select"
                                                    placeholder="Category"
                                                    value={item.productDto.categoryDto ? item.productDto.categoryDto.id : ''}
                                                >
                                                    <option value="" disabled>Product Category</option>
                                                    {allCategoryArray && allCategoryArray.map((item, index) =>
                                                        <option value={item.id} name="category"
                                                                key={index}>{item.name}</option>
                                                    )}
                                                </AvField>
                                                <div>
                                                    <img
                                                        src={api.attachment + item.productDto.attachmentIds[0]}
                                                        alt=""
                                                        width={'100%'}/>

                                                    <AvField onChange={(e) => setInputProduct(e, index)}
                                                             lable="Name"
                                                             name="content"
                                                             type={item.productDto.hasContent ? "text" : "number"}
                                                             required
                                                             value={item.productDto.hasContent ? item.content : item.amount}
                                                             placeholder={item.productDto.hasContent ? 'Enter text' : 'Enter number'}/>
                                                    <button type="button" className="btn btn-success"
                                                            onClick={() => leftProduct(index)}>Left
                                                    </button>
                                                    <button type="button" className="btn btn-success"
                                                            onClick={() => rightProduct(index)}>Right
                                                    </button>
                                                    <button className="btn btn-danger"
                                                            onClick={() => deleteProductRow(index)}>Delete
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    )}
                                    <Button className="btn btn-dark col-md-2"
                                            onClick={() => plusProductRow()}>+</Button>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-md-4">
                                        <AvField
                                            lable="Date"
                                            name="date"
                                            type="date"
                                            required
                                            defaultValue={currentOrder ? currentOrder.date : null}
                                            placeholder="Tayyorlanish sanasi"
                                        />
                                    </div>
                                    <div className="col-md-4">
                                        <AvField
                                            lable="ClientNumber"
                                            name="clientNumber"
                                            type="text"
                                            required
                                            defaultValue={currentOrder ? currentOrder.clientNumber : ''}
                                            placeholder="Nomer: +998 991234567"
                                        />
                                    </div>

                                    <div className="col-md-4">
                                        <Button color="primary" onClick={() => toggle()}
                                                style={{marginBottom: '1rem'}}>Yetkazib berish xizmatidan
                                            foydalanasizmi?</Button>
                                        <Collapse isOpen={isOpen}>
                                            <Card>
                                                <CardBody>
                                                    <AvField
                                                        // onChange={(e) => setProductCategoryy(e, index)}
                                                        lable="NameUz"
                                                        name="district"
                                                        type="select"
                                                        placeholder="District"
                                                        value={currentOrder.addressDto ? currentOrder.addressDto.districtDto.id : ''}
                                                    >
                                                        <option value="" disabled>Tumanni tanlang</option>
                                                        {districtArray && districtArray.map((item, index) =>
                                                            <option value={item.id} name="district"
                                                                    key={index}>{item.name}</option>
                                                        )}
                                                    </AvField>

                                                    <AvField
                                                        lable="Street"
                                                        name="street"
                                                        type="text"
                                                        required={isOpen}
                                                        value=""
                                                        defaultValue={currentOrder.addressDto ? currentOrder.addressDto.street : ''}
                                                        placeholder="Ko'chani kiriting"
                                                    />

                                                    <AvField
                                                        lable="HomeNumber"
                                                        name="homeNumber"
                                                        type="text"
                                                        required={isOpen}
                                                        value=""
                                                        defaultValue={currentOrder.addressDto ? currentOrder.addressDto.house : ''}
                                                        placeholder="Uy raqamini kiriting"
                                                    />
                                                </CardBody>
                                            </Card>
                                        </Collapse>
                                    </div>
                                </div>

                            </ModalBody>
                            <ModalFooter>
                                <Button color="danger" onClick={() => openModalSaveOrEdit('')}>Close</Button>
                                <Button color="success" type="submit">Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    <Modal style={{maxWidth: '600px', width: '400px'}} isOpen={showModalDelete}
                           toggle={() => openModalDelete('')}>
                        <ModalHeader>
                            Rostdan ham o'chirmoqchimisiz?
                        </ModalHeader>
                        <AvForm onValidSubmit={deleteOrder}>
                            <ModalFooter>
                                <Button onClick={() => openModalDelete('')}>Cancel</Button>
                                <Button color="danger" type="submit">Delete</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    <Modal style={{maxWidth: '600px', width: '400px'}} isOpen={showModalStatus}
                           toggle={() => openModalStatus('')}>
                        <ModalHeader>
                            Edit Status
                        </ModalHeader>
                        <ModalBody>
                            <div className="row m-1">
                                <Button className="btn btn-dark" onClick={selectNew}>NEW</Button>
                            </div>
                            <div className="row m-1">
                                <Button className="btn btn-dark" onClick={selectInProgress}>IN_PROGRESS</Button>
                            </div>
                            <div className="row m-1">
                                <Button className="btn btn-dark" onClick={selectCompleted}>COMPLETED</Button>
                            </div>
                            <div className="row m-1">
                                <Button className="btn btn-dark" onClick={selectFinished}>FINISHED</Button>
                            </div>
                            <div className="row m-1">
                                <Button className="btn btn-danger" onClick={selectCanceled}>CANCELED</Button>
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="danger" onClick={() => openModalStatus('')}>Cancel</Button>
                        </ModalFooter>
                    </Modal>

                </div>
            </div>
        </div>
    )
}

export default Order;