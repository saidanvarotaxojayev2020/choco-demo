import React, {useEffect, useState} from "react";
import SideBar from "../components/SideBar";
import axios from "axios";
import {TOKEN} from "../../utills/constants";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {DeleteOutlined, EditOutlined, PlusOutlined} from "@ant-design/icons";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {useHistory} from "react-router-dom"
import {api} from "../../api";

function ProductSet() {
    let history = useHistory();
    const [currentProductSet, setCurrentProductSet] = useState('');
    const [productSetArray, setProductSetArray] = useState(false);
    const [showModalSaveOrEdit, setShowModalSaveOrEdit] = useState(false);
    const [boxArray, setBoxArray] = useState([]);
    const [oneBox, setOneBox] = useState([]);
    const [boxCategory, setBoxCategory] = useState([]);
    const [indexBox, setIndexBox] = useState(0);
    const [productWithAmountArray, setProductWithAmountArray] = useState([]);
    const [allProduct, setAllProduct] = useState([])
    const [allCategoryArray, setAllCategoryArray] = useState([])
    const [indexProduct, setIndexProduct] = useState(0)
    const [selectedFileId, setSelectedFileId] = useState([]);
    const [showModalDelete, setShowModalDelete] = useState(false);



    useEffect(() => {
        axios.get(api.productSet,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN)
                    }
            }).then(res => {
            setProductSetArray(res.data.object)
        })
    }, [])

    useEffect(() => {
        axios.get(api.boxCategory, {
            headers: {
                "Authorization": localStorage.getItem(TOKEN)
            }
        }).then(res => {
            setBoxCategory(res.data._embedded.list)
        })
    }, [])

    useEffect(() => {
        axios.get(api.box,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const boxArr = res.data.object
            if (boxArr.length) {
                setBoxArray(boxArr)
                setOneBox([boxArr[0]])
            }
        })
    }, [])

    useEffect(() => {
        axios.get(api.product, {
            headers: {
                "Authorization": localStorage.getItem(TOKEN)
            }
        }).then(res => {
            setAllProduct(res.data.object)
        })
    }, [])

    useEffect(() => {
        axios.get(api.categoryAll, {
            headers:
                {
                    "Authorization": localStorage.getItem(TOKEN),
                }
        }).then(res => {
            setAllCategoryArray(res.data.object)
        })
    }, [])

    function openModalDelete(item) {
        setCurrentProductSet(item)
        setShowModalDelete(!showModalDelete)
    }

    function openModalSaveOrEdit(item) {
        setCurrentProductSet(item)
        setShowModalSaveOrEdit(!showModalSaveOrEdit)
        if (item) {
            setOneBox([item.boxDto])
            setSelectedFileId(item.attachmentIds)
            setProductWithAmountArray(item.productWithAmountDtos)
        } else {
            setSelectedFileId([])
            setProductWithAmountArray([])
        }
    }

    function setBoxCategoryy(e) {
        axios.get(api.getBoxByCategoryId + e.target.value, {
            headers:
                {
                    "Authorization": localStorage.getItem(TOKEN),
                }
        }).then(res => {
            setBoxArray(res.data.object)
            setOneBox([res.data.object[0]])
        })
    }

    function rightBox() {
        let index = indexBox + 1
        setIndexBox(index > boxArray.length - 1 ? index = 0 : index)
        setOneBox(index > boxArray.length - 1 ? [boxArray[0]] : [boxArray[index]])
    }

    function leftBox() {
        let index = indexBox - 1
        setIndexBox(index < 0 ? boxArray.length - 1 : index)
        setOneBox(index < 0 ? [boxArray[boxArray.length - 1]] : [boxArray[index]])
    }

    function setProductCategoryy(e, index) {
        axios.get(api.productByCategory + e.target.value, {
            headers:
                {
                    "Authorization": localStorage.getItem(TOKEN),
                }
        }).then(res => {
            setAllProduct(res.data.object)
            let newProductWithAmountArray = productWithAmountArray.map((item, i) => {
                if (i === index) {
                    return {...item, productDto: res.data.object[0]}
                } else {
                    return item
                }
                // return i === index ? {...item, productDto: allProducts[indexProd] : item}
            })
            setProductWithAmountArray(newProductWithAmountArray)
        })
    }

    function setInputProduct(e, index) {
        let value = e.target.value
        let newProductWithAmountArray = productWithAmountArray.map((item, i) => {
            if (i === index) {
                if (item.productDto.hasContent === true) {
                    return {...item, content: value, amount: value.length}
                } else {
                    return {...item, amount: value}
                }
            } else {
                return item
            }
        })
        setProductWithAmountArray(newProductWithAmountArray)
    }

    function leftProduct(index) {
        let indexProd = indexProduct - 1 < 0 ? allProduct.length - 1 : indexProduct - 1
        let allProducts = allProduct;  // state dan o'zgaruvchiga oldim
        let newProductWithAmountArray = productWithAmountArray.map((item, i) => {
            if (i === index) {
                return {...item, productDto: allProducts[indexProd]}
            } else {
                return item
            }
            // return i === index ? {...item, productDto: allProducts[indexProd] : item}
        })
        setProductWithAmountArray(newProductWithAmountArray)
        setIndexProduct(indexProd)
    }

    function rightProduct(index) {
        let indexProd = indexProduct + 1 > allProduct.length - 1 ? 0 : indexProduct + 1
        let allProducts = allProduct;  // state dan o'zgaruvchiga oldim
        let newProductWithAmountArray = productWithAmountArray.map((item, i) => {
            if (i === index) {
                return {...item, productDto: allProducts[indexProd]}
            } else {
                return item
            }
            // return i === index ? {...item, productDto: allProducts[indexProd] : item}
        })
        setProductWithAmountArray(newProductWithAmountArray)
        setIndexProduct(indexProd)
    }

    function deleteProductRow(index) {
        let newProdAmount = productWithAmountArray.filter(function (item, i) {
            if (i !== index) {
                return item
            }
        })
        setProductWithAmountArray(newProdAmount)
    }

    function plusProductRow() {
        if (allProduct.length) {
            setProductWithAmountArray([...productWithAmountArray, {
                id: '',
                productDto: {
                    id: allProduct[0].id,
                    name: allProduct[0].name,
                    price: allProduct[0].price,
                    attachmentIds: allProduct[0].attachmentIds,
                    categoryDto: {
                        id: allProduct[0].categoryDto.id,
                        name: allProduct[0].categoryDto.name
                    },
                    colorDto: {
                        id: allProduct[0].colorDto.id,
                        name: allProduct[0].colorDto.name,
                        code: allProduct[0].colorDto.code
                    },
                    measurementDto: {
                        id: '',
                        name: ''
                    },
                    measureValue: '',
                    hasContent: allProduct[0].hasContent
                },
                amount: '',
                content: '',
                orderDto: {},
                productSetDto: {
                    id: ''
                }
            }])
        } else {
            setProductWithAmountArray([...productWithAmountArray, {
                id: '',
                productDto: {
                    id: '',
                    name: '',
                    price: '',
                    attachmentIds: '',
                    categoryDto: {
                        id: '',
                        name: ''
                    },
                    colorDto: {
                        id: '',
                        name: '',
                        code: ''
                    },
                    measurementDto: {
                        id: '',
                        name: ''
                    },
                    measureValue: '',
                    hasContent: ''
                },
                amount: '',
                content: '',
                orderDto: {},
                productSetDto: {
                    id: ''
                }
            }])
        }
    }

    function setAttachment(e) {
        const formData = new FormData();

        formData.append('file', e.target.files[0]);
        formData.append("type", e.target.name);

        axios.post(api.attachment, formData,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                        "Content-Type": "multipart/form-data"
                    }
            }).then(res => {
            setSelectedFileId([res.data])
        })
    }

    function deleteProductSet() {
        axios.delete(api.productSet + currentProductSet.id, {
            headers: {
                "Authorization": localStorage.getItem(TOKEN)
            }
        }).then(res => {
            axios.get(api.productSet,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN)
                        }
                }).then(res => {
                setProductSetArray(res.data.object)
            })
        })
        setShowModalDelete(!showModalDelete)
    }

    function saveOrEditProductSet(e, v) {
        const productSet = {
            id: currentProductSet ? currentProductSet.id : null,
            name: v.productSetName,
            attachmentIds: selectedFileId,
            boxDto: {
                id: oneBox[0].id
            },
            productWithAmountDtos: productWithAmountArray
        }
        axios.post(api.productSet, productSet, {
            headers: {
                "Authorization": localStorage.getItem(TOKEN)
            }
        }).then(res => {
            axios.get(api.productSet,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN)
                        }
                }).then(res => {
                setProductSetArray(res.data.object)
            })
        })
        setShowModalSaveOrEdit(!showModalSaveOrEdit)
    }

    return (
        <div>
            <div className="row">
                <div className="col-md-12">
                    <div>
                        <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
                        </nav>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">
                    <SideBar/>
                </div>
                <div className="col-md-10">
                    <div className="row mt-3">
                        <div className="col-md-2">
                            <Button className="btn btn-success"
                                    onClick={() => openModalSaveOrEdit('')}><PlusOutlined/></Button>
                        </div>
                        <div className="col-md-4 offset-6">
                            <h1>ProductSet</h1>
                        </div>
                    </div>
                    <Table>
                        <thead>
                        <tr>
                            <th>T/R</th>
                            <th>Name</th>
                            <th>Box</th>
                            <th>Price</th>
                            <th>Photo</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        {productSetArray && productSetArray.map((item, index) =>
                            <tr key={index}>
                                <th>{index + 1}</th>
                                <td>{item.name}</td>
                                <td>{item.boxDto.name}</td>
                                <td>{item.totalPrice}</td>
                                <td>{item.attachmentIds.map(photo =>
                                    <img style={{width: 40, height: 40}}
                                         src={api.attachment + photo} alt=""/>
                                )}</td>
                                <td>
                                    <Button className="btn btn-success"
                                            onClick={() => openModalSaveOrEdit(item)}><EditOutlined/></Button>
                                    <Button className="btn btn-danger ml-2"
                                            onClick={() => openModalDelete(item)}><DeleteOutlined/></Button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </Table>
                </div>

                <Modal style={{maxWidth: '1200px'}} isOpen={showModalSaveOrEdit}
                       toggle={() => openModalSaveOrEdit('')}>
                    <ModalHeader toggle={() => openModalSaveOrEdit('')}>
                        {currentProductSet ? "Edit productSet" : "Add productSet"}
                    </ModalHeader>
                    <AvForm onValidSubmit={saveOrEditProductSet}>
                        <ModalBody>
                            <div className="row">
                                <div className="col-md-5">
                                    <AvField
                                        // onChange={handleChange}
                                        lable="productSetName"
                                        name="productSetName"
                                        type="text"
                                        placeholder="Nomini kiriting"
                                        value={currentProductSet ? currentProductSet.name : ''}
                                    />
                                </div>
                                <div className="col-md-3">
                                    {/*<input required type="file" onChange={setAttachment}/>*/}
                                    <AvField
                                        onChange={setAttachment}
                                        lable="attachment"
                                        name="attachment"
                                        type="file"
                                        placeholder="attachment"
                                        // value={currentProductSet ? currentProductSet.name : ''}
                                    />
                                </div>
                                <div className="col-md-2">
                                    <div style={{width: 300, height: 300}}>
                                        {selectedFileId ?
                                            <img className="img-fluid"
                                                 src={api.attachment + selectedFileId}
                                                 alt=""/> : ''
                                        }
                                    </div>
                                </div>

                            </div>
                            <div className="row">
                                <div className="col-md-2 mt-4">
                                    <AvField
                                        onChange={(e) => setBoxCategoryy(e)}
                                        lable="NameUz"
                                        name="category"
                                        type="select"
                                        placeholder="Category"
                                        value={currentProductSet ? currentProductSet.boxDto.boxCategoryDto.id : ''}
                                    >
                                        <option value="" disabled>Box Category</option>
                                        {boxCategory && boxCategory.map((item, index) =>
                                            <option value={item.id} name="category"
                                                    key={index}>{item.name}</option>
                                        )}
                                    </AvField>
                                    {oneBox && oneBox.map((item, index) =>
                                        <img key={index} src={api.attachment + item.attachmentIds[0]}
                                             alt=""
                                             width={'100%'}/>
                                    )}
                                    <button type="button" className="btn btn-success" onClick={leftBox}>Left
                                    </button>
                                    <button type="button" className="btn btn-success" onClick={rightBox}>Right
                                    </button>
                                </div>

                                {productWithAmountArray && productWithAmountArray.map((item, index) =>
                                    <div className="col-md-2 mt-4" key={index}>
                                        <div>
                                            <AvField
                                                onChange={(e) => setProductCategoryy(e, index)}
                                                lable="NameUz"
                                                name="category"
                                                type="select"
                                                placeholder="Category"
                                                value={item.productDto.categoryDto ? item.productDto.categoryDto.id : ''}
                                            >
                                                <option value="" disabled>Product Category</option>
                                                {allCategoryArray && allCategoryArray.map((item, index) =>
                                                    <option value={item.id} name="category"
                                                            key={index}>{item.name}</option>
                                                )}
                                            </AvField>
                                            <div>
                                                <img
                                                    src={api.attachment + item.productDto.attachmentIds[0]}
                                                    alt=""
                                                    width={'100%'}/>

                                                <AvField onChange={(e) => setInputProduct(e, index)}
                                                         lable="Name"
                                                         name="content"
                                                         type={item.productDto.hasContent ? "text" : "number"}
                                                         required
                                                         value={item.productDto.hasContent ? item.content : item.amount}
                                                         placeholder={item.productDto.hasContent ? 'Enter text' : 'Enter number'}/>
                                                <button type="button" className="btn btn-success"
                                                        onClick={() => leftProduct(index)}>Left
                                                </button>
                                                <button type="button" className="btn btn-success"
                                                        onClick={() => rightProduct(index)}>Right
                                                </button>
                                                <button className="btn btn-danger"
                                                        onClick={() => deleteProductRow(index)}>Delete
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                )}
                                <Button className="btn btn-dark col-md-2"
                                        onClick={() => plusProductRow()}>+</Button>
                            </div>

                            <p>Total sum: <b> {productWithAmountArray.reduce(function (prev, cur) {
                                return prev + (cur.amount * cur.productDto.price)
                            }, 0)
                                .toFixed().toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")
                            }
                            </b>
                            </p>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="danger" onClick={() => openModalSaveOrEdit('')}>Close</Button>
                            <Button color="success" type="submit">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                <Modal style={{maxWidth: '600px', width: '400px'}} isOpen={showModalDelete}
                       toggle={() => openModalDelete('')}>
                    <ModalHeader>
                        Rostdan ham o'chirmoqchimisiz?
                    </ModalHeader>
                    <AvForm onValidSubmit={deleteProductSet}>
                        <ModalFooter>
                            <Button onClick={() => openModalDelete('')}>Cancel</Button>
                            <Button color="danger" type="submit">Delete</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>
            </div>
        </div>
    )
}

export default ProductSet;