import React, {useEffect, useState} from "react";
import SideBar from "../components/SideBar";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {DeleteOutlined, EditOutlined, PlusOutlined} from '@ant-design/icons';
import axios from "axios";
import {TOKEN} from "../../utills/constants";
import {useHistory} from "react-router-dom";
import {api} from "../../api";

function Product() {
    let history = useHistory();
    const [showModal, setShowModal] = useState(false);
    const [productArray, setProductArray] = useState([]);
    const [currentProduct, setCurrentProduct] = useState('');
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [categoryArray, setCategoryArray] = useState([])
    const [colorArray, setColorArray] = useState([])
    const [measurementArray, setMeasurementArray] = useState([])
    const [attachmentArray, setAttachmentArray] = useState([])
    const [selectedFileId, setSelectedFileId] = useState([]);


    function openModalDelete(v) {
        setCurrentProduct(v)
        setShowModalDelete(!showModalDelete)

    }

    function deleteProduct() {
        axios.delete(api.product + currentProduct.id)
            .then(res => {
                axios.get(api.product,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const productArray1 = res.data.object
                    productArray1.reverse()
                    setProductArray(productArray1)
                })
            })
        setShowModalDelete(!showModalDelete)
    }

    function openModal(v) {
        setCurrentProduct(v)
        setSelectedFileId(v.attachmentIds)
        setShowModal(!showModal)
    }

    function saveOrEditProduct(e, v) {
        const newProduct = {
            id: currentProduct ? currentProduct.id : null,
            name: v.name,
            price: v.price,
            measureValue: v.measureValue,
            hasContent: v.hasContent,
            colorDto: {
                id: v.colorId,
            },
            categoryDto: {
                id: v.categoryId
            },
            measurementDto: {
                id: v.measurementId
            },
            attachmentIds: selectedFileId
        }

        if (currentProduct) {
            axios.post(api.product,
                newProduct,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                axios.get(api.product,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const productArray1 = res.data.object
                    productArray1.reverse()
                    setProductArray(productArray1)
                })
            })
        } else {
            axios.post(api.product, newProduct,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                axios.get(api.product,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const productArray1 = res.data.object
                    productArray1.reverse()
                    setProductArray(productArray1)
                })
            })
        }
        setShowModal(!showModal)
    }

    useEffect(() => {
        axios.get(api.product,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const productArray1 = res.data.object
            productArray1.reverse()
            setProductArray(productArray1)
        })
    }, [])

    useEffect(() => {
        axios.get(api.categoryAll,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setCategoryArray(res.data.object)
        })
    }, [])

    useEffect(() => {
        axios.get(api.color,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setColorArray(res.data._embedded.list)
        })
    }, [])

    useEffect(() => {
        axios.get(api.measurement,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            setMeasurementArray(res.data._embedded.list)
        })
    }, [])


    function setAttachment(e) {
        const formData = new FormData();

        formData.append('file', e.target.files[0]);
        formData.append("type", e.target.name);

        axios.post(api.attachment, formData,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                        "Content-Type": "multipart/form-data"
                    }
            }).then(res => {
            setSelectedFileId([res.data])
        })
    }

    return (
        <div>
            <div className="row">
                <div className="col-md-12">
                    <div>
                        <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
                        </nav>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">
                    <SideBar/>
                </div>
                <div className="col-md-10">
                    <div className="row mt-3">
                        <div className="col-md-2">
                            <Button className="btn btn-success"
                                    onClick={() => openModal('')}><PlusOutlined/></Button>
                        </div>
                        <div className="col-md-4 offset-6">
                            <h1>Product</h1>
                        </div>
                    </div>
                    <Table>
                        <thead>
                        <tr>
                            <th>T/R</th>
                            <th>Name</th>
                            <th>measurement</th>
                            <th>color</th>
                            <th>price</th>
                            <th>category</th>
                            <th>Photo</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        {productArray && productArray.map((item, index) =>
                            <tr>
                                <th>{index + 1}</th>
                                <td>{item.name}</td>
                                <td>{item.measurementDto.name}</td>
                                <td>{<div style={{width: 60, height: 20, backgroundColor: item.colorDto.code}}></div>
                                }</td>
                                <td>{item.price}</td>
                                <td>{item.categoryDto.name}</td>
                                <td>{item.attachmentIds.map(photo =>
                                    <img style={{width: 40, height: 40}}
                                         src={api.attachment + photo} alt=""/>
                                )}</td>
                                <td>
                                    <Button className="btn btn-success"
                                            onClick={() => openModal(item)}><EditOutlined/></Button>
                                    <Button className="btn btn-danger ml-2"
                                            onClick={() => openModalDelete(item)}><DeleteOutlined/></Button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </Table>

                    <Modal isOpen={showModal} toggle={() => openModal('')}>
                        <ModalHeader toggle={() => openModal('')}>
                            {currentProduct ? "Edit Product" : "Add Product"}
                        </ModalHeader>
                        <AvForm onValidSubmit={saveOrEditProduct}>
                            <ModalBody>

                                <div className="row mt-2">
                                    <input type="file" onChange={setAttachment}/>
                                </div>
                                <div style={{width: 30, height: 30}}>
                                    {selectedFileId ?
                                        <img className="img-fluid"
                                             src={api.attachment + selectedFileId}
                                             alt=""/> : ''
                                    }
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <AvField lable="Name"
                                                 name="name"
                                                 type="text"
                                                 required
                                                 defaultValue={currentProduct ? currentProduct.name : ''}
                                                 placeholder="Name"/>
                                    </div>
                                    <div className="col-md-4">
                                        <AvField lable="Price"
                                                 name="price"
                                                 type="number"
                                                 defaultValue={currentProduct ? currentProduct.price : ''}
                                                 placeholder="Price"/>
                                    </div>

                                    <div className="col-md-2">
                                        <AvField lable="hasContent"
                                                 name="hasContent"
                                                 type="checkbox"
                                                 defaultValue={currentProduct ? currentProduct.hasContent : ''}
                                        />
                                        <p>.. Harfmi?</p>
                                    </div>
                                </div>


                                <div className="row">
                                    <div className="col-md-6">
                                        <AvField lable="Category"
                                                 name="categoryId"
                                                 type="select"
                                                 defaultValue={currentProduct ? currentProduct.categoryDto.name : ''}
                                                 placeholder="Category"
                                                 value={currentProduct ? currentProduct.categoryDto.id : ''}>
                                            <option value="" disabled>Categoryni tanlang</option>
                                            {categoryArray && categoryArray.map((category, index) =>
                                                <option value={category.id}
                                                        name="category">{category.name}</option>
                                            )}
                                        </AvField>
                                    </div>
                                    <div className="col-md-6">
                                        <AvField lable="Color"
                                                 name="colorId"
                                                 type="select"
                                                 defaultValue={currentProduct ? currentProduct.colorDto.name : ''}
                                                 placeholder="Color"
                                                 value={currentProduct ? currentProduct.colorDto.id : ''}>
                                            <option value="" disabled>Colorni tanlang</option>
                                            {colorArray && colorArray.map((color, index) =>
                                                <option value={color.id}
                                                        name="color">{color.name}</option>
                                            )}
                                        </AvField>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-6">
                                        <AvField lable="Measurement"
                                                 name="measurementId"
                                                 type="select"
                                                 defaultValue={currentProduct ? currentProduct.measurementDto.name : ''}
                                                 placeholder="Category"
                                                 value={currentProduct ? currentProduct.measurementDto.id : ''}>
                                            <option value="" disabled>measurementni tanlang</option>
                                            {measurementArray && measurementArray.map((measurement, index) =>
                                                <option value={measurement.id}
                                                        name="measurement">{measurement.name}</option>
                                            )}
                                        </AvField>
                                    </div>

                                    <div className="col-md-6">
                                        <AvField lable="MeasureValue"
                                                 name="measureValue"
                                                 type="text"
                                                 defaultValue={currentProduct ? currentProduct.measureValue : ''}
                                                 placeholder="measureValue"
                                        />
                                    </div>
                                </div>


                            </ModalBody>
                            <ModalFooter>
                                <Button product="danger" onClick={() => openModal('')}>Cancel</Button>
                                <Button product="success" type="submit">Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    <Modal isOpen={showModalDelete} toggle={() => openModalDelete('')}>
                        <ModalHeader>Rostdan ham o'chirmoqchimisiz?</ModalHeader>
                        <AvForm onValidSubmit={deleteProduct}>
                            <ModalFooter>
                                <Button className="btn btn-danger"
                                        onClick={() => openModalDelete('')}>Cancel</Button>
                                <Button className="btn btn-success" type="submit">Delete</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>
                </div>
            </div>
        </div>
    )
}

export default Product