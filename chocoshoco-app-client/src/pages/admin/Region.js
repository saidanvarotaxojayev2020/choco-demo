import React, {useEffect, useState} from "react";
import SideBar from "../components/SideBar";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {DeleteOutlined, EditOutlined, PlusOutlined} from '@ant-design/icons';
import axios from "axios";
import {TOKEN} from "../../utills/constants";
import {useHistory} from "react-router-dom";
import {api} from "../../api";

function Region() {
    let history = useHistory();
    const [showModal, setShowModal] = useState(false);
    const [regionArray, setRegionArray] = useState([]);
    const [currentRegion, setCurrentRegion] = useState('');
    const [showModalDelete, setShowModalDelete] = useState(false);

    function openModalDelete(v) {
        setCurrentRegion(v)
        setShowModalDelete(!showModalDelete)

    }

    function deleteProduct() {
        axios.delete(api.region + currentRegion.id)
            .then(res => {
                axios.get(api.region,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const regionArray1 = res.data._embedded.list
                    regionArray1.reverse()
                    setRegionArray(regionArray1)
                })
            })
        setShowModalDelete(!showModalDelete)
    }

    function openModal(v) {
        setCurrentRegion(v)
        setShowModal(!showModal)
    }

    function saveOrEditRegion(e, v) {
        if (currentRegion) {
            axios.patch(api.region + currentRegion.id,
                v,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                axios.get(api.region,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const regionArray1 = res.data._embedded.list
                    regionArray1.reverse()
                    setRegionArray(regionArray1)
                })
            })
        } else {
            axios.post(api.region, v,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                axios.get(api.region,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const regionArray1 = res.data._embedded.list
                    regionArray1.reverse()
                    setRegionArray(regionArray1)
                })
            })
        }
        setShowModal(!showModal)
    }

    useEffect(() => {
        axios.get(api.region,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const regionArray1 = res.data._embedded.list
            regionArray1.reverse()
            setRegionArray(regionArray1)
        })
    }, [])


    return (
        <div>
            <div className="row">
                <div className="col-md-12">
                    <div>
                        <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
                        </nav>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">
                    <SideBar/>
                </div>
                <div className="col-md-10">
                    <div className="row mt-3">
                        <div className="col-md-2">
                            <Button className="btn btn-success"
                                    onClick={() => openModal('')}><PlusOutlined/></Button>
                        </div>
                        <div className="col-md-4 offset-6">
                            <h1>Region</h1>
                        </div>
                    </div>
                    <Table>
                        <thead>
                        <tr>
                            <th>T/R</th>
                            <th>Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        {regionArray && regionArray.map((item, index) =>
                            <tr>
                                <th>{index + 1}</th>
                                <td>{item.name}</td>
                                <td>
                                    <Button className="btn btn-success"
                                            onClick={() => openModal(item)}><EditOutlined/></Button>
                                    <Button className="btn btn-danger ml-2"
                                            onClick={() => openModalDelete(item)}><DeleteOutlined/></Button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </Table>

                    <Modal isOpen={showModal} toggle={() => openModal('')}>
                        <ModalHeader toggle={() => openModal('')}>
                            {currentRegion ? "Edit Region" : "Add Region"}
                        </ModalHeader>
                        <AvForm onValidSubmit={saveOrEditRegion}>
                            <ModalBody>
                                <AvField lable="Name"
                                         name="name"
                                         type="text"
                                         required
                                         defaultValue={currentRegion ? currentRegion.name : ''}
                                         placeholder="Name"/>

                            </ModalBody>
                            <ModalFooter>
                                <Button region="danger" onClick={() => openModal('')}>Cancel</Button>
                                <Button region="success" type="submit">Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    <Modal isOpen={showModalDelete} toggle={() => openModalDelete('')}>
                        <ModalHeader>Rostdan ham o'chirmoqchimisiz?</ModalHeader>
                        <AvForm onValidSubmit={deleteProduct}>
                            <ModalFooter>
                                <Button className="btn btn-danger"
                                        onClick={() => openModalDelete('')}>Cancel</Button>
                                <Button className="btn btn-success" type="submit">Delete</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>
                </div>
            </div>
        </div>
    )
}

export default Region