import React, {useEffect, useState} from "react";
import SideBar from "../components/SideBar";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {DeleteOutlined, EditOutlined, PlusOutlined} from '@ant-design/icons';
import axios from "axios";
import {TOKEN} from "../../utills/constants";
import {useHistory} from "react-router-dom";
import {api} from "../../api";

function Measurement() {
    let history = useHistory();
    const [showModal, setShowModal] = useState(false);
    const [measurementArray, setMeasurementArray] = useState([])
    const [currentMeasurement, setCurrentMeasurement] = useState('');
    const [showModalDelete, setShowModalDelete] = useState(false);

    useEffect(() => {
        axios.get(api.measurement,
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const measurementArray1 = res.data._embedded.list
            measurementArray1.reverse()
            setMeasurementArray(measurementArray1)
        })
    }, [])

    function openModal(v) {
        setCurrentMeasurement(v)
        setShowModal(!showModal)

    }

    function openModalDelete(v) {
        setCurrentMeasurement(v)
        setShowModalDelete(!showModalDelete)
    }

    function deleteProduct() {
        axios.delete(api.measurement + currentMeasurement.id).then(res => {
            axios.get(api.measurement,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                const measurementArray1 = res.data._embedded.list
                measurementArray1.reverse()
                setMeasurementArray(measurementArray1)
            })
        })
        setShowModalDelete(!showModalDelete)
    }

    function saveOrEditMeasurement(e, v) {
        if (currentMeasurement) {
            axios.put(api.measurement + currentMeasurement.id,
                v,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }

                }).then(res => {
                axios.get(api.measurement,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const measurementArray1 = res.data._embedded.list
                    measurementArray1.reverse()
                    setMeasurementArray(res.data._embedded.list.reverse())
                })

            })
        } else {
            axios.post(api.measurement, v,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),

                        }
                }).then(res => {
                axios.get(api.measurement,
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN)
                            }
                    }).then(res => {
                    const newMeasurementArray = res.data._embedded.list
                    newMeasurementArray.reverse()
                    setMeasurementArray(newMeasurementArray)
                })
            })
        }
        setShowModal(!showModal)
    }


    return (
        <div>
            <div className="row">
                <div className="col-md-12">
                    <div>
                        <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
                        </nav>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">
                    <SideBar/>
                </div>
                <div className="col-md-10">
                    <div className="row mt-3">
                        <div className="col-md-2">
                            <Button className="btn btn-success" onClick={() => openModal('')}><PlusOutlined/></Button>
                        </div>
                        <div className="col-md-4 offset-6">
                            <h1>Measurment</h1>
                        </div>
                    </div>

                    <Table>
                        <thead>
                        <tr>
                            <th>T/R</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {measurementArray && measurementArray.map((item, index) =>
                            <tr>
                                <th>{index + 1}</th>
                                <td>{item.name}</td>
                                <td>
                                    <Button className="btn btn-success" onClick={() => openModal(item)}><EditOutlined/></Button>
                                    <Button className="btn btn-danger ml-2"
                                            onClick={() => openModalDelete(item)}><DeleteOutlined/></Button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </Table>

                    <Modal isOpen={showModal} toggle={() => openModal('')}>
                        <ModalHeader toggle={() => openModal('')}>
                            {currentMeasurement ? "Edit measurement category" : "Add measurement category"}
                        </ModalHeader>
                        <AvForm onValidSubmit={saveOrEditMeasurement}>
                            <ModalBody>
                                <AvField lable="Name"
                                         name="name"
                                         type="text"
                                         required
                                         defaultValue={currentMeasurement ? currentMeasurement.name : ''}
                                         placeholder="Name"/>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="danger" onClick={() => openModal('')}>Cancel</Button>
                                <Button color="success" type="submit">Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    <Modal isOpen={showModalDelete} toggle={() => openModalDelete('')}>
                        <ModalHeader>Rostdan ham o'chirmoqchimisiz?</ModalHeader>
                        <AvForm onValidSubmit={deleteProduct}>
                            <ModalFooter>
                                <Button className="btn btn-danger"
                                        onClick={() => openModalDelete('')}>Cancel</Button>
                                <Button className="btn btn-success" type="submit">Delete</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>
                </div>

            </div>
        </div>
    )
}

export default Measurement;