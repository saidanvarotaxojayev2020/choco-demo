import React, {useEffect, useState} from 'react'
import SideBar from "../components/SideBar";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {DeleteOutlined, EditOutlined, PlusOutlined} from '@ant-design/icons';
import axios from "axios";
import {TOKEN} from "../../utills/constants";
import {Input} from "antd";
import {useHistory} from "react-router-dom";
import {api} from "../../api";

function AdminClient() {
    let history = useHistory();
    const [showModal, setShowModal] = useState(false);
    const [adminClientArray, setAdminClientArray] = useState([]);
    const [currentAdminClient, setCurrentAdminClient] = useState('');
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [editPassword, setEditPassword] = useState(true);


    function searchUser(e, v) {
        axios.get(api.searchUserByRoleName + '?search=' + e.target.value + '&roleName=ROLE_CLIENT')
            .then(res => {
                setAdminClientArray(res.data.object)
            })

    }


    function openModalDelete(v) {
        setCurrentAdminClient(v)
        setShowModalDelete(!showModalDelete)

    }


    function deleteProduct() {
        axios.delete(api.user + currentAdminClient.id)
            .then(res => {
                axios.get(api.getAllDriverOrUserByPageable + '?roleName=ROLE_CLIENT',
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const adminClientArray1 = res.data.object

                    setAdminClientArray(adminClientArray1)
                })
            })
        setShowModalDelete(!showModalDelete)
    }

    function openModal(v) {
        setCurrentAdminClient(v)
        setShowModal(!showModal)
    }

    function saveOrEditAdminClient(e, v) {
        let blaa = {
            ...v,
            phoneNumber: v.phoneNumber.replaceAll(' ', '')
        }
        if (currentAdminClient) {
            let client = {
                id: currentAdminClient.id,
                ...blaa
            }
            axios.post(api.adminAddUserOrEditOrUserRegister, client,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                axios.get(api.getAllDriverOrUserByPageable + '?roleName=ROLE_CLIENT',
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),

                            }
                    }).then(res => {
                    const adminClientArray1 = res.data.object
                    setAdminClientArray(adminClientArray1)
                })
            })
        } else {
            axios.post(api.adminAddUserOrEditOrUserRegister, blaa,
                {
                    headers:
                        {
                            "Authorization": localStorage.getItem(TOKEN),
                        }
                }).then(res => {
                axios.get(api.getAllDriverOrUserByPageable + '?roleName=ROLE_CLIENT',
                    {
                        headers:
                            {
                                "Authorization": localStorage.getItem(TOKEN),
                            }
                    }).then(res => {
                    const adminClientArray1 = res.data.object
                    setAdminClientArray(adminClientArray1)
                })
            })
        }
        setShowModal(!showModal)
    }

    useEffect(() => {
        axios.get(api.getAllDriverOrUserByPageable + '?roleName=ROLE_CLIENT',
            {
                headers:
                    {
                        "Authorization": localStorage.getItem(TOKEN),
                    }
            }).then(res => {
            const adminClientArray1 = res.data.object
            setAdminClientArray(adminClientArray1)
        })
    }, [])


    function handleEditPassword() {
        setEditPassword(!editPassword);
    }


    return (
        <div>
            <div className="row">
                <div className="col-md-12">
                    <div>
                        <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
                        </nav>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">
                    <SideBar/>
                </div>
                <div className="col-md-10">
                    <div className="row mt-3">
                        <div className="col-md-2">
                            <Button className="btn btn-success"
                                    onClick={() => openModal('')}><PlusOutlined/></Button>
                        </div>
                        <div className="col-md-4 offset-6">
                            <h1>Clients</h1>

                        </div>
                    </div>
                    <Input.Group compact>
                        <Input.Search allowClear style={{
                            marginLeft: 12,
                            marginBottom: 20,
                            width: 200,
                            height: 30,
                            borderRadius: 5
                        }}
                                      type="text"
                                      placeholder="Search"
                                      onChange={(e) => searchUser(e)}
                        />

                    </Input.Group>


                    <Table>
                        <thead>
                        <tr>
                            <th>T/R</th>
                            <th>Name</th>
                            <th>Nomeri</th>

                        </tr>
                        </thead>
                        <tbody>

                        {adminClientArray && adminClientArray.map((item, index) =>
                            <tr>
                                <th>{index + 1}</th>
                                <td>{item.firstName}</td>
                                <td>{item.phoneNumber}</td>


                                <td>
                                    <Button className="btn btn-success"
                                            onClick={() => openModal(item)}><EditOutlined/></Button>
                                    <Button className="btn btn-danger ml-2"
                                            onClick={() => openModalDelete(item)}><DeleteOutlined/></Button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </Table>


                    <Modal isOpen={showModal} toggle={() => openModal('')}>
                        <ModalHeader toggle={() => openModal('')}>
                            {currentAdminClient ? "Edit Client" : "Add Client"}
                        </ModalHeader>
                        <AvForm onValidSubmit={saveOrEditAdminClient}>
                            <ModalBody>
                                <AvField lable="Name"
                                         name="firstName"
                                         type="text"
                                         required
                                         defaultValue={currentAdminClient ? currentAdminClient.firstName : ''}
                                         placeholder="Name"/>
                                <AvField lable="Number"
                                         name="phoneNumber"
                                         type="text"
                                         defaultValue={currentAdminClient ? currentAdminClient.phoneNumber : ''}
                                         placeholder="Nomerizni kiriting"/>
                                {
                                    currentAdminClient ?
                                        <AvField lable="Change Password"
                                                 name="editPassword"
                                                 type="checkbox"
                                                 onClick={handleEditPassword}/> : ''
                                }
                                <AvField lable="Password"
                                         name="password"
                                         type="password"
                                         disabled={editPassword}
                                    // defaultValue={currentAdminClient ? currentAdminClient.password : ''}
                                         placeholder="Parolingizni kiriting"/>
                                <AvField lable="prePassword"
                                         name="prePassword"
                                         type="password"
                                         disabled={editPassword}
                                    // defaultValue={currentAdminClient ? currentAdminClient.prePassword : ''}
                                         placeholder="Qaytadan parolingizni kiriting"/>


                            </ModalBody>
                            <ModalFooter>
                                <Button adminClient="danger" onClick={() => openModal('')}>Cancel</Button>
                                <Button adminClient="success" type="submit">Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    <Modal isOpen={showModalDelete} toggle={() => openModalDelete('')}>
                        <ModalHeader>Rostdan ham o'chirmoqchimisiz?</ModalHeader>
                        <AvForm onValidSubmit={deleteProduct}>
                            <ModalFooter>
                                <Button className="btn btn-danger"
                                        onClick={() => openModalDelete('')}>Cancel</Button>
                                <Button className="btn btn-success" type="submit">Delete</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>
                </div>
            </div>
        </div>
    )
}


export default AdminClient