import React from 'react'
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Button} from "reactstrap";
import axios from 'axios';
import {TOKEN} from "../utills/constants";
import {BASE_URL} from "../utills/config";
import jwt from "jwt-decode";
import {useHistory} from "react-router-dom"

function SignIn() {
    let history = useHistory();

    function login(e, v) {
        console.log(v, "value")
        axios.post(BASE_URL + 'auth/login', {
            phoneNumber: v.phoneNumber,
            password: v.password
        })
            .then(res => {
                console.log(res.data, "RESPONSE");
                localStorage.setItem(TOKEN, res.data.tokenType + ' ' + res.data.accessToken);
                let parsedToken = jwt(res.data.accessToken);
                console.log(parsedToken, "ParsedToken")
                if (parsedToken.roles[0].roleName === 'ROLE_ADMIN') {
                    history.push('/admin/order')
                } else if (parsedToken.roles[0].roleName === 'ROLE_CLIENT') {
                    history.push('/customer/customerMain')
                } else {
                    history.push('/')
                }
            }).catch(err => {
            alert('Nomer yoki parol xato terildi!!!')
        })
    }

    return (
        <div className="App">
            <div id="layoutAuthentication_content">
                <main>
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-lg-5">
                                <div className="card shadow-lg border-0 rounded-lg mt-5">
                                    <div className="card-header"><h3
                                        className="text-center font-weight-light my-4">Login</h3></div>
                                    <div className="card-body">
                                        <AvForm onValidSubmit={login}>
                                            <div className="form-group">
                                                <AvField
                                                    className="form-control py-4"
                                                    type="text"
                                                    name="phoneNumber"
                                                    label="Enter phone number"
                                                    required
                                                    placeholder="Enter phone number"
                                                />
                                            </div>
                                            <div className="form-group">
                                                <AvField
                                                    className="form-control py-4"
                                                    type="password"
                                                    name="password"
                                                    label="Enter password"
                                                    required
                                                    placeholder="Enter password"
                                                />
                                            </div>
                                            <div
                                                className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                                <a className="small" href="password.html">Forgot Password?</a>
                                                <Button className="btn btn-success" type="submit">Login</Button>
                                            </div>
                                        </AvForm>
                                    </div>
                                    <div className="card-footer text-center">
                                        <div className="small"><a href="register.html">Need an account? Sign up!</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    )
}

export default SignIn