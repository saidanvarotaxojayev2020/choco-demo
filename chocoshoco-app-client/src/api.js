import {BASE_URL} from "./utills/config";

export const api = {
    // USER controller //
    user: BASE_URL + 'user/',
    adminAddUserOrEditOrUserRegister: BASE_URL + 'user/adminAddUserOrEditOrUserRegister',
    allUserByPageable: BASE_URL + 'user/allUserByPageable',
    getAllDriverOrUserByPageable: BASE_URL + 'user/getAllDriverOrUserByPageable',
    searchUserByRoleName: BASE_URL + 'user/searchUserByRoleName',
    editUserInfo: BASE_URL + 'user/editUserInfo/',
    editPassword: BASE_URL + 'user/editPassword',
    blockUser: BASE_URL + 'user/blockUser/',
    userMe: BASE_URL + 'user/me',

    // ORDER controller //
    order: BASE_URL + 'order',
    orderSlash: BASE_URL + 'order/',
    editStatus: BASE_URL + 'order/editStatus',

    // ATTACHMENT //
    attachment: BASE_URL + 'attachment/',

    // AUTH controller //
    auth: BASE_URL + 'auth/',
    login: BASE_URL + 'auth/login',
    register: BASE_URL + 'auth/register',
    sendSms: BASE_URL + 'auth/sendSms',

    // BOX controller //
    box: BASE_URL + 'box/',
    getBoxByCategoryId: BASE_URL + 'box/getBoxByCategoryId/',

    // CATEGORY controller //
    category: BASE_URL + 'category/',
    categoryAll: BASE_URL + 'category/all/',
    getAllCategoriesByPageable: BASE_URL + 'category/getAllCategoriesByPageable',

    // PRODUCT //
    product: BASE_URL + 'product/',
    productByCategory: BASE_URL + 'product/productByCategory/',

    // PRODUCTSET controller //
    productSet: BASE_URL + 'productSet/',

    // RepositoryRestResource //
    boxCategory: BASE_URL + 'boxCategory/',
    color: BASE_URL + 'color/',
    deliveryPrice: BASE_URL + 'deliveryPrice/',
    district: BASE_URL + 'district/',
    measurement: BASE_URL + 'measurement/',
    region: BASE_URL + 'region/'
}